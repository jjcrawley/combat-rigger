The project folder is broken up into five primary folders:

1.	Combat Framework
2.	Interop Tests
3.	Unity Combat Rigger
4.	Unity Combat Rigger Build 
5.	Unity Combat Rigger Package.

The Combat Framework folder contains all the code for the C++ runtime portion of the project. 

The Interop tests folder contains files and code that was created as part of the Research phase, 
for testing and establishing an interop workflow. This consists of the C++ DLL code and C# console application. 
It also has the final Unity test project that was used as the final test bed for the C++ DLL.

The Unity Combat Rigger is the Unity project that contains all the C# runtime and editor code, as well as a 
constructed test scene that was used to test and debug the project. The demo scene was constructed using some 
third-party assets downloaded from the Unity Asset Store.

The Unity Combat Rigger Build is a built version of the test scene using the Combo box. The build is a 
Standalone 64-bit development build for the Windows platform. This build should run on most modern-day machines.

The Unity Combat Rigger Package is a stripped-down version of the Unity Combat Rigger Unity project. This package
contains the necessary scripts and files to add the Combo Box to a Unity project of your choice. The package does 
not contain any of the third-party assets or the test scene in the Unity Combat Rigger project.
