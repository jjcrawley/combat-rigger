﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//
// アニメーション簡易プレビュースクリプト
// 2015/4/25 quad arrow
//

// Require these components when using this script
[RequireComponent(typeof(Animator))]

public class IdleChanger : InputHandler
{

	private Animator anim;						// Animatorへの参照
	private AnimatorStateInfo currentState;		// 現在のステート状態を保存する参照
	private AnimatorStateInfo previousState;	// ひとつ前のステート状態を保存する参照

    private HitboxController m_testRig;

    private Dictionary<string, bool> m_active = new Dictionary<string, bool>();

    private IInputSettings m_settings;

    public override IInputSettings Settings
    {
        set
        {
            m_settings = value;
        }
        get
        {
            return m_settings;
        }
    }

    // Use this for initialization
    void Start ()
	{
		// 各参照の初期化
		anim = GetComponent<Animator> ();
		currentState = anim.GetCurrentAnimatorStateInfo (0);
		previousState = currentState;

        m_testRig = GetComponent<HitboxController>();

        m_active.Add("Jab", false);
        m_active.Add("HiKick", false);
        m_active.Add("Spin kick", false);
        m_active.Add("Rising_P", false);
        m_active.Add("HeadSpring", false);
        m_active.Add("Land", false);
        m_active.Add("ScrewKick", false);
        m_active.Add("DamageDown", false);
        m_active.Add("SamK", false);             
	}

	void OnGUI()
	{	
		GUI.Box(new Rect(Screen.width - 200 , 45 ,120 , 350), "");
        if (GUI.Button(new Rect(Screen.width - 190, 60, 100, 20), "Jab"))
        {           
            m_active["Jab"] = true;
        }
        if(GUI.Button(new Rect(Screen.width - 190 , 90 ,100, 20), "Hikick"))
            m_active["HiKick"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 120 ,100, 20), "Spinkick"))
            m_active["Spin kick"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 150 ,100, 20), "Rising_P"))
            m_active["Rising_P"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 180 ,100, 20), "Headspring"))
            m_active["HeadSpring"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 210 ,100, 20), "Land"))
            m_active["Land"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 240 ,100, 20), "ScrewKick"))
            m_active["ScrewKick"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 270 ,100, 20), "DamageDown"))
            m_active["DamageDown"] = true;
        if (GUI.Button(new Rect(Screen.width - 190 , 300 ,100, 20), "SamK"))
            m_active["SamK"] = true;
   //     if (GUI.Button(new Rect(Screen.width - 190 , 330 ,100, 20), "Run"))
			//anim.SetBool ("Run", true);
		//if(GUI.Button(new Rect(Screen.width - 190 , 360 ,100, 20), "Run_end"))
		//	anim.SetBool ("Run", false);

;
	}

    public override bool ButtonDown(string name)
    {
        //Debug.Log(name);
        bool returnVal = m_active[name];

        if(m_active[name])
        {
            m_active[name] = false;
        }

        return returnVal;
    }

    public override bool ButtonHeld(string name)
    {
        throw new NotImplementedException();
    }

    public override bool ButtonUp(string name)
    {
        throw new NotImplementedException();
    }

    public override float GetAxis(string name)
    {
        throw new NotImplementedException();
    }

    public override float GetAxisRaw(string name)
    {
        throw new NotImplementedException();
    }

    public override void UpdateInput()
    {
       
    }
}