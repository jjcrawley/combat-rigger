﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

internal class Interop
{
    #region CallbackDelegates
    public delegate void SimpleCallback();
    public delegate void UpdateCallback(float normal);
    public delegate bool InputCallback(float normal);
    public delegate bool ButtonCallback(string name);
    public delegate float AxisCallback(string name);
    public delegate void Debugger(string message);
    public delegate void BoxHandler(int id);
    public delegate void FireDataHandler(float interval);
    #endregion

#if UNITY_STANDALONE_WIN
    const string Dllname = "CombatFramework";
#endif

    #region IObject
    [DllImport(Dllname)]
    public static extern void ObjectOnCreate(IntPtr obj);

    [DllImport(Dllname)]
    public static extern void ObjectOnRelease(IntPtr obj);
    #endregion

    #region ComboController
    [DllImport(Dllname)]
    public static extern IntPtr CreateComboController();
    [DllImport(Dllname)]
    public static extern void ReleaseComboController(IntPtr controller);
    //[DllImport(Dllname)]
    //public static extern void OnCreate(IntPtr controller);
    //[DllImport(Dllname)]
    //public static extern void UpdateController(IntPtr controller, float normal);
    [DllImport(Dllname)]
    public static extern void UpdateInteropController(IntPtr controller, float normalOne, float normalTwo);
    [DllImport(Dllname)]
    public static extern void AssignComboBox(IntPtr controller, IntPtr comboBox);
    [DllImport(Dllname)]
    public static extern void SetComboTree(IntPtr controller, string name);
    [DllImport(Dllname)]
    public static extern void SetSingleComboTree(IntPtr controller, IntPtr tree);
    [DllImport(Dllname)]
    public static extern void AssignInputHandler(IntPtr controller, IntPtr handler);
    [DllImport(Dllname)]
    public static extern void GetStateInfo(IntPtr controller, out StateInfo info);
    [DllImport(Dllname)]
    public static extern void SetHitboxController(IntPtr controller, IntPtr hitboxController);
    [DllImport(Dllname)]
    public static extern void TriggerEnter(IntPtr controller);
    [DllImport(Dllname)]
    public static extern void EndTransition(IntPtr controller);
    [DllImport(Dllname)]
    public static extern void ProcessHit(IntPtr controller);
    [DllImport(Dllname)]
    public static extern void SetParamsCallback(IntPtr controller, SimpleCallback callback);
    #endregion

    #region ComboTree
    [DllImport(Dllname)]
    public static extern IntPtr CreateAComboTree();
    [DllImport(Dllname)]
    public static extern IntPtr CreateComboTree(IntPtr comboBox);
    [DllImport(Dllname)]
    public static extern void SetTreeName(IntPtr tree, string name);
    [DllImport(Dllname)]
    public static extern IntPtr GetResetTransition(IntPtr tree);
    [DllImport(Dllname)]
    public static extern IntPtr AddEntryTransition(IntPtr tree, IntPtr state);
    #endregion

    #region ComboBox
    [DllImport(Dllname)]
    public static extern IntPtr CreateComboBox();
    [DllImport(Dllname)]
    public static extern void SetName(IntPtr comboBox, string name);
    //[DllImport(Dllname)]
    //public static extern void AddComboTree(IntPtr comboBox, IntPtr tree);
    //[DllImport(Dllname)]
    //public static extern void ReleaseComboBox(IntPtr box);
    #endregion

    #region ComboState
    [DllImport(Dllname)]
    public static extern IntPtr CreateCombatState(IntPtr tree);
    [DllImport(Dllname)]
    public static extern void AssignComboMove(IntPtr state, IntPtr move);
    [DllImport(Dllname)]
    public static extern void AddCombatModifier(IntPtr state, IntPtr mod);
    [DllImport(Dllname)]
    public static extern void SetResetFrame(IntPtr state, float frame);
    //[DllImport(Dllname)]
    //public static extern void ReleaseComboState(IntPtr state);
    #endregion

    #region CombatModifier
    [DllImport(Dllname)]
    public static extern IntPtr CreateCombatModifier(IntPtr move);    
    [DllImport(Dllname)]
    public static extern void SetModOnHit(IntPtr modifier, SimpleCallback callback);
    [DllImport(Dllname)]
    public static extern void SetModOnEnter(IntPtr modifier, SimpleCallback callback);
    [DllImport(Dllname)]
    public static extern void SetModOnExit(IntPtr modifier, SimpleCallback callback);
    [DllImport(Dllname)]
    public static extern void SetModUpdate(IntPtr modifier, UpdateCallback callback);
    //[DllImport(Dllname)]
    //public static extern void ReleaseModifier(IntPtr modifier);
    #endregion

    #region CombatMove
    [DllImport(Dllname)]
    public static extern IntPtr CreateCombatMoveInfo();
    //[DllImport(Dllname)]
    //public static extern void CombatMoveOnCreate(IntPtr move);   
    [DllImport(Dllname)]
    public static extern void SetMoveName(IntPtr move, string name);
    [DllImport(Dllname)]
    public static extern void SetMoveLength(IntPtr move, int frameCount);
    [DllImport(Dllname)]
    public static extern void SetMoveContinuous(IntPtr move, bool continuous);
    [DllImport(Dllname)]
    public static extern void SetMoveDamageInterval(IntPtr move, float interval);
    [DllImport(Dllname)]
    public static extern void SetMoveActivationPeriod(IntPtr move, float start, float end);
    [DllImport(Dllname)]
    public static extern void SetMoveID(IntPtr move, int id);
    [DllImport(Dllname)]
    public static extern void SetMoveCallbacks(IntPtr move, SimpleCallback enter, SimpleCallback exit, UpdateCallback update);
    [DllImport(Dllname)]
    public static extern void ReleaseMoveInfo(IntPtr move);
    #endregion

    #region ComboLink
    [DllImport(Dllname)]
    public static extern IntPtr CreateAndAddLink(IntPtr startState, IntPtr destination);
    [DllImport(Dllname)]
    public static extern IntPtr CreateAndAddPlayerLink(IntPtr startState, IntPtr destination);
    [DllImport(Dllname)]
    public static extern void SetPlayerLinkCallback(IntPtr link, SimpleCallback callback);
    [DllImport(Dllname)]
    public static extern void SetLinkCallback(IntPtr link, SimpleCallback callback);
    [DllImport(Dllname)]
    public static extern void SetButtonName(IntPtr link, string name);
    [DllImport(Dllname)]
    public static extern void SetInputPeriod(IntPtr link, float start, float end);
    [DllImport(Dllname)]
    public static extern void SetFirePeriod(IntPtr link, float start, float end);
    [DllImport(Dllname)]
    public static extern void SetInstantTransition(IntPtr link, bool instant);
    //[DllImport(Dllname)]
    //public static extern void ReleaseLink(IntPtr link);
    #endregion

    #region LinkLock
    [DllImport(Dllname)]
    public static extern IntPtr CreateAndAddLinkLock(IntPtr link);
    [DllImport(Dllname)]
    public static extern void SetLinkLockInputCallback(IntPtr link, InputCallback callback);
    //[DllImport(Dllname)]
    //public static extern void ReleaseLinkLock(IntPtr linkLock);
    #endregion

    #region InputHandler
    [DllImport(Dllname)]
    public static extern IntPtr CreateInputHandler();
    [DllImport(Dllname)]
    public static extern void RegisterInputButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld, IntPtr handler);
    [DllImport(Dllname)]
    public static extern void RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis, IntPtr handler);
    [DllImport(Dllname)]
    public static extern void ReleaseInputHandler(IntPtr handler);
    #endregion

    #region Hitboxes
    [DllImport(Dllname)]
    public static extern IntPtr CreateNewHitboxController();
    [DllImport(Dllname)]
    public static extern void SetHitBoxCallbacks(IntPtr controller, BoxHandler handler, BoxHandler deactivator, FireDataHandler fireDataHandler);
    [DllImport(Dllname)]
    public static extern void ReleaseHitboxController(IntPtr controller);
    #endregion

    #region Helper
    [DllImport(Dllname)]
    public static extern void SetDebugCallback(Debugger callback);
    #endregion
}