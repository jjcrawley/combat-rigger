﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestModifier : MoveModifier
{
    public bool debug = false;

    public override void OnFirstHit(HitInfo info, MoveModifierInfo modInfo)
    {
        if (debug)
        {
            Debug.Log("On Hit Move " + Move.name);
        }
    }

    public override void OnUpdate(float normalTime, MoveModifierInfo info)
    {
        if (debug)
        {
            Debug.Log("On Update Move " + Move.name + " Controller was " + info.Controller.gameObject);
        }
    }

    public override void OnEnter(MoveModifierInfo info)
    {
        if(debug)
        {
            Debug.Log("On Enter Move " + Move.name + " Controller was " + info.Controller.gameObject);
        }
        //Debug.Log("Hello from test modifier, number is: " + aNumber);
    }

    public override void OnExit(MoveModifierInfo info)
    {
        if(debug)
        {
            Debug.Log("On Exit Move " + Move.name + " Controller was " + info.Controller.gameObject);
        }
        //Debug.Log("Bye bye from test modifier");
    }    
}