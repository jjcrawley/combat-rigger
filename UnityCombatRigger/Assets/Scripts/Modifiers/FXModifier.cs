﻿using System.Collections.Generic;
using UnityEngine;

public class FXModifier : MoveModifier
{
    [SerializeField]
    private FXTable m_fxTable = null;

    [SerializeField]
    private FX[] m_onHitFX = null;

    [SerializeField]
    private FX[] m_onStartFX = null;
        
    [SerializeField]
    private AudioClip m_playClip = null;
        
    private void OnEnable()
    {
        if (m_onHitFX == null)
        {
            m_onHitFX = new FX[0];
        }

        if (m_onStartFX == null)
        {
            m_onStartFX = new FX[0];
        }        
    }

    public override void Setup(ComboController comboController)
    {
        FXPool.Instance.RegisterTable(m_fxTable);
    }

    public override void OnEnter(MoveModifierInfo info)
    {
        //Debug.Log("Entered fx");
        for (int i = 0; i < m_onStartFX.Length; i++)
        {            
            FX current = m_onStartFX[i];

            Transform transform = info.Controller.transform.Find(current.transformPath);

            SetupFX(current, transform);
        }
    }

    public override void OnFirstHit(HitInfo info, MoveModifierInfo modInfo)
    {
        if (m_playClip != null)
        {
            AudioSource source = modInfo.controller.AudioSource;

            source.Stop();

            source.clip = m_playClip;

            source.Play();
        }

        for(int i = 0; i < m_onHitFX.Length; i++)
        {
            FX current = m_onHitFX[i];

            SetupFX(current, info.Hitbox.transform);
        }       
    }

    private void SetupFX(FX fx, Transform transform)
    {
        FXController controller = FXPool.Instance.GetController(fx.fxName);

        if (controller != null)
        {
            controller.gameObject.SetActive(true);

            controller.Play(fx, transform);
        }
    }

    public override void OnExit(MoveModifierInfo info)
    {
        for(int i = 0; i < m_onHitFX.Length; i++)
        {
            FXPool.Instance.StopFX(m_onHitFX[i].fxName);
        }           

        for(int i = 0; i < m_onStartFX.Length; i++)
        {
            FXPool.Instance.StopFX(m_onStartFX[i].fxName);
        }
    }
}