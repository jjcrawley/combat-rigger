﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveModifier : ScriptableObject
{
    [SerializeField, HideInInspector]
    private CombatMove m_move;

    private Interop.SimpleCallback m_onEnter;
    private Interop.UpdateCallback m_onUpdate;
    private Interop.SimpleCallback m_onExit;
    private Interop.SimpleCallback m_onHit;

    private IntPtr m_pointer;

    internal IntPtr GetMoveBackend(IntPtr move)
    {
        if(m_pointer == IntPtr.Zero)
        {
            m_pointer = CreateModBackend(move);            
        }

        return m_pointer;
    }

    private IntPtr CreateModBackend(IntPtr move)
    {
        if (m_onEnter == null)
        {
            m_onEnter = new Interop.SimpleCallback(EnterBack);
        }

        if (m_onExit == null)
        {
            m_onExit = new Interop.SimpleCallback(ExitBack);
        }

        if (m_onHit == null)
        {
            m_onHit = new Interop.SimpleCallback(OnHit);
        }

        if (m_onUpdate == null)
        {
            m_onUpdate = new Interop.UpdateCallback(UpdateBack);
        }

        IntPtr pointer =  Interop.CreateCombatModifier(move);

        Interop.SetModOnEnter(m_pointer, m_onEnter);
        Interop.SetModOnExit(m_pointer, m_onExit);
        Interop.SetModOnHit(m_pointer, m_onHit);
        Interop.SetModUpdate(m_pointer, m_onUpdate);

        return pointer;
    }

    internal IntPtr GetBackend(IntPtr state)
    {        
        IntPtr pointer = CreateModBackend(IntPtr.Zero);

        Interop.AddCombatModifier(state, pointer);        

        //bool implementedUpdate;

        //Type type = GetType();

        //MethodInfo method = type.GetMethod("OnUpdate");
        
        //if (method.DeclaringType.Name == "MoveModifier")
        //{
        //    Debug.Log("Update not used");
        //}

        //method = type.GetMethod("OnEnter");

        //if(method.DeclaringType.Name == "MoveModifier")
        //{
        //    Debug.Log("On enter not used");
        //}
        
        return pointer;
    }

    public virtual void Setup(ComboController controller)
    {

    }
    
    private void EnterBack()
    {
        try
        {
            OnEnter(ComboController.s_modifierInfo);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }    

    public virtual void OnEnter(MoveModifierInfo info)
    {
        //Debug.Log("hello");
    }

    private void UpdateBack(float normalTime)
    {
        try
        {
            OnUpdate(normalTime, ComboController.s_modifierInfo);
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public virtual void OnUpdate(float normalTime, MoveModifierInfo info)
    {
        //Debug.Log("Updated");
    }

    private void ExitBack()
    {
        try
        {
            OnExit(ComboController.s_modifierInfo);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public virtual void OnExit(MoveModifierInfo info)
    {
        //Debug.Log("exitting");
    }

    private void OnHit()
    {
        try
        {
            OnFirstHit(ComboController.s_lastInfo, ComboController.s_modifierInfo);
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
        }

        //Debug.Log("hitting");
    }

    public virtual void OnFirstHit(HitInfo info, MoveModifierInfo modInfo)
    {
        
    }
        
    public CombatMove Move
    {
        internal set
        {
            m_move = value;
        }
        get
        {
            return m_move;
        }
    }
}

public class MoveModifierInfo
{
    internal ComboController controller;
    
    public ComboController Controller
    {
        get
        {
            return controller;
        }
    }
}