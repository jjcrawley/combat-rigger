﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create FX Table", fileName = "FX Table")]
public class FXTable : ScriptableObject
{
    [SerializeField]
    private PoolSettings[] m_fxPool;
              
    public PoolSettings[] FXPoolSettings
    {
        get
        {
            return m_fxPool;
        }
    }
}