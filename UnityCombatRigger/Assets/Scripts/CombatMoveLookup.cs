﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMoveLookup
{
    private static Dictionary<int, CombatMove> s_moveLookup;

    private static Dictionary<string, CombatMove> s_nameMoveLookup;

    static CombatMoveLookup()
    {
        s_moveLookup = new Dictionary<int, CombatMove>();
        s_nameMoveLookup = new Dictionary<string, CombatMove>();
    }

    public static void AddMove(CombatMove move)
    {
        if (!s_moveLookup.ContainsKey(move.GetInstanceID()))
        {
            s_moveLookup.Add(move.GetInstanceID(), move);
            s_nameMoveLookup.Add(move.name, move);
        }
    }

    public static CombatMove GetMove(int id)
    {
        CombatMove move;

        if(s_moveLookup.TryGetValue(id, out move))
        {
            return move;
        }

        return move;
    }

    public static CombatMove GetMove(string name)
    {
        CombatMove move;

        if(s_nameMoveLookup.TryGetValue(name, out move))
        {
            return move;
        }

        return move;
    }
    
    public static CombatMove[] GetMoves()
    {
        CombatMove[] moves = new CombatMove[s_moveLookup.Count];

        int i = 0;

        foreach(var keyPair in s_moveLookup)
        {
            moves[i] = keyPair.Value;
            i++;                        
        }

        return moves;       
    }
}