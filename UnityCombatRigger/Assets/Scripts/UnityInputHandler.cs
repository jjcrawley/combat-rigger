﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityInputHandler : InputHandler
{
    [SerializeField]
    private UnityInputSettings m_inputSettings;  

    private Dictionary<int, string> m_inputTable;

    private void Awake()
    {
        if(m_inputSettings == null)
        {
            return;
        }

        //Debug.Log("Setup stuff");

        InputSetting[] settings = m_inputSettings.Settings;

        m_inputTable = new Dictionary<int, string>(settings.Length);

        for (int i = 0; i < settings.Length; i++)
        {
            m_inputTable.Add(settings[i].name.GetHashCode(), settings[i].unityInput);
        }
    }

    public override void UpdateInput()
    {
    }

    public override bool ButtonDown(string name)
    {
        //Debug.Log(name);

        string inputName = m_inputTable[name.GetHashCode()];
        
        return Input.GetButtonDown(inputName);
    }

    public override bool ButtonHeld(string name)
    {
        string inputName = m_inputTable[name.GetHashCode()];

        return Input.GetButton(inputName);
    }

    public override bool ButtonUp(string name)
    {
        string inputName = m_inputTable[name.GetHashCode()];

        return Input.GetButtonUp(inputName);
    }

    public override float GetAxis(string name)
    {
        string inputName = m_inputTable[name.GetHashCode()];

        return Input.GetAxis(inputName);
    }

    public override float GetAxisRaw(string name)
    {
        string inputName = m_inputTable[name.GetHashCode()];

        return Input.GetAxisRaw(inputName);
    }   
    
    public override IInputSettings Settings
    {
        set
        {
            m_inputSettings = (UnityInputSettings)value;
            Awake();
        }
        get
        {
            return m_inputSettings;
        }
    }
}