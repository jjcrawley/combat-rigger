﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryState : ComboState
{
    public override void UpdateState(float normalTime, ComboController controller)
    {
        //Debug.Log(Links.Length);

        if (!controller.TransitionPending)
        {
            EvaluateTransitions(normalTime, controller);
        }
    }

    public override void OnEnter()
    {
    }

    public override void OnExit()
    {
    }

    public override void ProcessHit(HitInfo info)
    {
    }

#if UNITY_EDITOR
    public void ResetLinks(ComboLink[] entryLinks)
    {
        m_links = new ComboLink[entryLinks.Length];        

        for(int i = 0; i < entryLinks.Length; i++)
        {
            m_links[i] = entryLinks[i];
            m_links[i].Origin = this;
        }
    }

    public override void ReorderLinks(ComboLink[] orderedLinks)
    {
        Tree.ReorderEntryLinks(orderedLinks);
        ResetLinks(orderedLinks);
    }

#endif
    public ComboTree Tree
    {
        set;
        get;        
    }
}