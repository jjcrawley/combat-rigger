﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableEntity : MonoBehaviour, IDamageableEntity
{
    private Animator m_animator;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    public void TakeDamage(ComboController controller, float damage)
    {
        if(m_animator != null)
        {
            m_animator.SetTrigger("DamageDown");
        }
        else
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(20, 20, 20));
        }

        //GetComponent(new Vector3(20, 20, 20));
        //Debug.Log("Took damage " + gameObject.name, this);
    }
}

public interface IDamageableEntity : ICombatEntity
{
    void TakeDamage(ComboController controller, float damage);
}