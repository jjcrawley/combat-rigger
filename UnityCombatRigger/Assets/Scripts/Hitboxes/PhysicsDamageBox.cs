﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class PhysicsDamageBox : BaseHitbox
{
    private Collider m_collider;

    private readonly Color m_enabledColour = new Color(1.0f, 0.0f, 0.0f, 0.5f);

    private readonly Color m_disabledColour = Color.blue;

    private HashSet<Collider> m_collidedColliders = new HashSet<Collider>();

    public override void EnableHitbox()
    {
        enabled = true;

        if (m_collider != null)
        {
            //Debug.Log(enabled);
            m_collider.enabled = true;

            //m_hitbox.SetupHitBox(this);

            base.EnableHitbox();
        }           
    }

    public override void DisableHitbox()
    {
        enabled = false;

        if (m_collider != null)
        {
            m_collider.enabled = false;

            //Debug.Log("disabled");
            m_collidedColliders.Clear();
            base.DisableHitbox();
        }
    }

    private void OnDrawGizmos()
    {       
        if(m_hitbox.Type == HitboxInformation.HitboxType.Sphere)
        {
            DrawDebugSphere();
        }
        else if(m_hitbox.Type == HitboxInformation.HitboxType.Box)
        {
            DrawDebugCube();
        }
    }

    private void DrawDebugSphere()
    {
        if (enabled)
        {
            Vector3 centre = m_hitbox.Centre;
            centre = transform.TransformPoint(centre);

            Vector3 lossyScale = transform.lossyScale;

            float scale = Mathf.Max(Mathf.Abs(lossyScale.x), Mathf.Abs(lossyScale.y), Mathf.Abs(lossyScale.z));
            scale = Mathf.Max(scale, Mathf.Epsilon);

            float radius = Mathf.Abs(m_hitbox.SphereRadius) * scale;

            radius = Mathf.Max(radius, Mathf.Epsilon);
        
            Gizmos.color = m_enabledColour;
            Gizmos.DrawSphere(centre, radius);
        }     
    }

    private void DrawDebugCube()
    {        
        if(enabled)
        {
            Matrix4x4 transformMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

            Matrix4x4 matrix = Gizmos.matrix;

            Gizmos.matrix = transformMatrix;

            Vector3 size = m_hitbox.Dimensions;
            size.Scale(transform.lossyScale);

            Vector3 centre = m_hitbox.Centre;
            centre = transform.TransformPoint(centre);
            centre = transformMatrix.inverse.MultiplyPoint(centre);
            
            Gizmos.color = m_enabledColour;
            Gizmos.DrawCube(centre, size);

            Gizmos.matrix = matrix;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(!enabled || m_collidedColliders.Contains(collider))
        {
            return;
        }

        m_collidedColliders.Add(collider);    

        if (collider.CompareTag(m_hitbox.Tag))
        {
            m_hitboxController.NotifyOnHit(collider, this);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if(!enabled)
        {
            return;
        }

        if (m_collidedColliders.Remove(collider) && collider.CompareTag(m_hitbox.Tag))
        {
            m_hitboxController.NotifyOnExit(collider);
        }
    }

    public Collider Collider
    {
        set
        {
            m_collider = value;
        }
        get
        {
            return m_collider;
        }
    }
}