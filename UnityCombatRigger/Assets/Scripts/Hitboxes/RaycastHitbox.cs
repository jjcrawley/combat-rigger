﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastHitbox : BaseHitbox
{    
    public float m_radius = 0.0f;
    public LayerMask m_physicsMask;
    public Vector3 m_offset = new Vector3();

    private static Collider[] s_results = new Collider[100];
        
    public override void DisableHitbox()
    {
        base.DisableHitbox();
        enabled = false;
    }

    public override void EnableHitbox()
    {
        base.EnableHitbox();        
        enabled = true;       
    }

    public override void UpdateBox()
    {
        //Debug.Log("updating");
        int count; 

        if ((count = Physics.OverlapSphereNonAlloc(transform.position + m_offset, m_radius, s_results, m_physicsMask.value)) > 0)
        {            
            m_hitboxController.NotifyHits(count, s_results, this);                       
        }
    }   
   
    private void OnDrawGizmos()
    {
        if (enabled)
        {
            Color colour = Color.red;
            colour.a = 0.5f;
            Gizmos.color = colour;
            Gizmos.DrawSphere(transform.position + m_offset, m_radius);
        }
        //else
        //{
        //    Gizmos.color = Color.magenta;
        //    Gizmos.DrawWireSphere(transform.position + m_offset, m_radius);
        //}
    }      
}