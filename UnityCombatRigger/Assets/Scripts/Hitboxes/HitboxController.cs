﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxController : MonoBehaviour, ICombatEntity
{    
    private List<HitboxInformation> m_damageBoxInfo = new List<HitboxInformation>();  
        
    private Dictionary<int, List<BaseHitbox>> m_damageBoxLookup = new Dictionary<int, List<BaseHitbox>>();

    private ComboController m_controller;

    private HashSet<ICombatEntity> m_hitEntities = new HashSet<ICombatEntity>();
    
    protected List<IIHitboxListener> m_damageSendListeners = new List<IIHitboxListener>();

    private Dictionary<Component, ICombatEntity> m_contactEntityLookup = new Dictionary<Component, ICombatEntity>(20);

    private Dictionary<ICombatEntity, HashSet<Component>> m_contactColliderLookup = new Dictionary<ICombatEntity, HashSet<Component>>(20);

    private List<BaseHitbox> m_activeBoxes;

    private Coroutine m_updateRoutine;
        
    public void Setup(GameObject root, ComboController controller)
    {
        m_controller = controller;
        
        enabled = false;      
    }    
       
    public void ActivateDamageBoxes(CombatMove move)
    {
        int ID = move.GetInstanceID();

        ActivateDamageBoxes(ID);
    }

    public void ActivateDamageBoxes(int id)
    {
        List<BaseHitbox> hitboxes;

        if(m_activeBoxes != null)
        {
            m_activeBoxes.ForEach((BaseHitbox hitbox) => { hitbox.DisableHitbox(); });
        }

        if (m_damageBoxLookup.TryGetValue(id, out hitboxes))
        {
            hitboxes = m_damageBoxLookup[id];

            hitboxes.ForEach((BaseHitbox hitbox) => { hitbox.EnableHitbox(); });

            m_activeBoxes = hitboxes;            
        }
    }

    public void DisableDamageBoxes(CombatMove move)
    {
        int ID = move.GetInstanceID();

        DisableDamageBoxes(ID);        
    }

    public void DisableDamageBoxes(int id)
    {
        List<BaseHitbox> hitboxes;

        if (m_damageBoxLookup.TryGetValue(id, out hitboxes))
        {
            hitboxes = m_damageBoxLookup[id];

            hitboxes.ForEach((BaseHitbox hitbox) => { hitbox.DisableHitbox(); });

            m_activeBoxes = null;                             
        }

        m_hitEntities.Clear();        
        m_contactEntityLookup.Clear();
        m_controller.damageController.ClearTimers();             
    }

    public BaseHitbox AddHitbox(CombatMove move, HitboxInformation box, GameObject root, ComboController controller)
    {
        int moveInstanceID = move.GetInstanceID();

        return AddHitbox(moveInstanceID, box, root, controller);             
    }

    private BaseHitbox AddHitbox(int moveInstanceID, HitboxInformation box, GameObject root, ComboController controller)
    {
        m_damageBoxInfo.Add(box);

        BaseHitbox hitbox = box.BuildHitbox(root);

        hitbox.HitboxController = this;

        if (m_damageBoxLookup.ContainsKey(moveInstanceID))
        {
            List<BaseHitbox> info = m_damageBoxLookup[moveInstanceID];

            info.Add(hitbox);
        }
        else
        {
            List<BaseHitbox> info = new List<BaseHitbox>();

            info.Add(hitbox);

            m_damageBoxLookup.Add(moveInstanceID, info);
        }

        return hitbox;
    }   

    public void RegisterListener(IIHitboxListener listener)
    {
        m_damageSendListeners.Add(listener);
    }

    public void RemoveListener(IIHitboxListener listener)
    {
        m_damageSendListeners.Remove(listener);
    }

    public bool StillInContact(IDamageableEntity entity)
    {
        return m_contactColliderLookup.ContainsKey(entity);
    }

    protected void InvokeListeners(HitInfo info)
    {
        for (int i = 0; i < m_damageSendListeners.Count; i++)
        {
            m_damageSendListeners[i].OnHit(info);
        }
    }
    
    internal void NotifyOnHit(Component collider, BaseHitbox hitbox)
    {
        ICombatEntity entity;

        if (!m_contactEntityLookup.TryGetValue(collider, out entity))
        {
            entity = collider.GetComponent<ICombatEntity>();

            if (entity == null)
            {
                return;
            }

            m_contactEntityLookup.Add(collider, entity);

            HashSet<Component> colliders;

            if(!m_contactColliderLookup.TryGetValue(entity, out colliders))
            {
                colliders = new HashSet<Component>();

                colliders.Add(collider);

                m_contactColliderLookup.Add(entity, colliders);
            }
            else
            {
                colliders.Add(collider);
            }
        }
                   
        NotifyOnHit(entity as IDamageableEntity, hitbox, collider);
    }

    internal void NotifyHits(int count, Component[] colliders, BaseHitbox hitbox)
    {
        for (int i = 0; i < count; i++)
        {
            Component current = colliders[i];

            NotifyOnHit(current, hitbox);
        }
    }   
        
    public void NotifyOnHit(IDamageableEntity entity, BaseHitbox hitbox, Component collider)
    {        
        if (!m_hitEntities.Contains(entity) && entity != null)
        {
            HitInfo info = new HitInfo();
            info.senderController = m_controller;
            info.damageBox = hitbox;
            info.hitEntity = entity;
            info.hitCollider = collider;

            Component hitEntity = entity as Component;

            if(hitEntity != null)
            {
                info.hitObject = hitEntity.gameObject;
            }
            
            m_hitEntities.Add(entity);
            
            m_controller.OnHit(info);

            InvokeListeners(info);            
        }       
    }   

    internal void NotifyOnExit(Component collider)
    {
        ICombatEntity entity;

        if (m_contactEntityLookup.TryGetValue(collider, out entity))
        {
            m_contactEntityLookup.Remove(collider);

            HashSet<Component> current = m_contactColliderLookup[entity];

            current.Remove(collider);     
            
            if(current.Count == 0)
            {
                m_contactColliderLookup.Remove(entity);
            }       
        }
    }     

    public HitboxInformation[] DamageBoxes
    {
        get
        {
            return m_damageBoxInfo.ToArray();
        }
    }    
}

public interface IIHitboxListener
{
    void OnHit(HitInfo info);    
}