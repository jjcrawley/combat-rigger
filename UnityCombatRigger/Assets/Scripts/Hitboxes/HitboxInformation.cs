﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

[System.Serializable]
public class HitboxInformation
{
    //private Transform m_transform;

    [SerializeField, Tooltip("The path to the transform to place the hitbox at")]
    private string m_transformPath;

    [SerializeField, Tooltip("The hit box shape, sphere or cube")]
    private HitboxType m_type;

    [SerializeField, Tooltip("The centre of the hit box, relative to the target transform")]
    private Vector3 m_centre;

    [SerializeField, Tooltip("The height, width and length of the box")]
    private Vector3 m_dimensions;

    [SerializeField, Tooltip("The radius of the sphere")]
    private float m_sphereRadius;

    [SerializeField, Tooltip("The tag to detect collisions for")]
    private string m_tag;
        
    private CombatMove m_move;
        
    public BaseHitbox BuildHitbox(GameObject root)
    {
        Transform transform = GetTransform(root);
                
        Collider collider = BuildCollider(transform);

        if (collider == null)
        {
            return null;
        }

        PhysicsDamageBox listener = transform.gameObject.AddComponent<PhysicsDamageBox>();
        listener.Collider = collider;
        listener.enabled = false;
        listener.DisableHitbox();
        listener.Info = this;

        return listener;
    }

    private Collider BuildCollider(Transform transform)
    {
        Collider collider = null;

        if (m_type == HitboxType.Box)
        {
            BoxCollider boxCol = transform.gameObject.GetComponent<BoxCollider>();
            
            boxCol = transform.gameObject.AddComponent<BoxCollider>();
            boxCol.size = m_dimensions;
            boxCol.center = m_centre;
            boxCol.isTrigger = true;
            boxCol.enabled = false;
            collider = boxCol;            
        }
        else if (m_type == HitboxType.Sphere)
        {
            SphereCollider sphereCol = transform.gameObject.GetComponent<SphereCollider>();
            
            sphereCol = transform.gameObject.AddComponent<SphereCollider>();
            sphereCol.radius = m_sphereRadius;
            sphereCol.center = m_centre;
            sphereCol.isTrigger = true;
            sphereCol.enabled = false;
            collider = sphereCol;
        }

        if (collider == null)
        {
            Debug.Log("The collider was null, shouldn't happen");
            return null;
        }

        return collider;
    }

    public virtual void SetupHitBox(BaseHitbox hitbox)
    {
        PhysicsDamageBox current = hitbox as PhysicsDamageBox;

        if (current != null)
        {
            SphereCollider col = current.Collider as SphereCollider;

            if (col != null)
            {
                col.radius = m_sphereRadius;
                col.center = m_centre;
            }
        }
    }

    public void SetTransform(Transform transform)
    {
        if (transform == null)
        {
            return;
        }

        Stack<string> names = new Stack<string>();

        do
        {
            names.Push(transform.name);

            transform = transform.parent;
        }
        while (transform.parent != null);

        StringBuilder builder = new StringBuilder();

        while (names.Count > 0)
        {
            builder.Append(names.Pop() + "/");
        }

        builder.Remove(builder.Length - 1, 1);

        m_transformPath = builder.ToString();
    }

    public Transform GetTransform(GameObject root)
    {
        return root.transform.Find(m_transformPath);
    }

    public string TransformPath
    {
        get
        {
            return m_transformPath;
        }
    }

    public CombatMove Move
    {
        set
        {
            m_move = value;
        }
        get
        {
            return m_move;
        }
    }

    public float SphereRadius
    {
        get
        {
            return m_sphereRadius;
        }
    }

    public Vector3 Centre
    {
        get
        {
            return m_centre;
        }
    }

    public Vector3 Dimensions
    {
        get
        {
            return m_dimensions;
        }
    }

    public HitboxType Type
    {
        get
        {
            return m_type;
        }
    }

    public string Tag
    {
        get
        {
            return m_tag;
        }
    }

    public enum HitboxType
    {
        Sphere,
        Box      
    }
}