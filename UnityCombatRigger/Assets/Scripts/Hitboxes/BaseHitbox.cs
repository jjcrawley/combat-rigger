﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHitbox : MonoBehaviour, ICombatEntity
{
    protected HitboxInformation m_hitbox;   
    protected HitboxController m_hitboxController;
        
    protected virtual void OnEnable()
    {
        //EnableHitbox();
    }

    public virtual void Setup(HitboxInformation hitbox)
    {
        m_hitbox = hitbox;
    }

    protected virtual void OnDisable()
    {
        //DisableHitbox();
    }

    public virtual void DisableHitbox()
    {
       
    }

    public virtual void EnableHitbox()
    {
    }    

    public virtual void UpdateBox()
    {

    }
    
    public HitboxController HitboxController
    {
        set
        {
            m_hitboxController = value;
        }
        get
        {
            return m_hitboxController;
        }
    }

    public HitboxInformation Info
    {
        internal set
        {
            m_hitbox = value;
        }
        get
        {
            return m_hitbox;
        }
    }
}