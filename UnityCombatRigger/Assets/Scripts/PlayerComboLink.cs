﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComboLink : BasePlayerLink
{
    [SerializeField, Tooltip("The input period of the move")]
    private Period m_inputPeriod;

    private Period m_inputNormal;

    [SerializeField, Tooltip("Should the move instantly transition, or delay it till a specified point in the animation")]
    private bool b_instantTransition;

    [SerializeField, Tooltip("When you want the move to transition")]
    private Period m_firePeriod;

    private Period m_fireNormal;

    private Interop.SimpleCallback m_callback;   

    internal override IntPtr CreateBackend(IntPtr start, IntPtr destination)
    {
        IntPtr pointer = Interop.CreateAndAddPlayerLink(start, destination);

        Interop.SetButtonName(pointer, m_buttonName);

        Interop.SetInputPeriod(pointer, m_inputPeriod.start, m_inputPeriod.end);

        Interop.SetFirePeriod(pointer, m_firePeriod.start, m_firePeriod.end);

        //Debug.Log(b_instantTransition);

        Interop.SetInstantTransition(pointer, b_instantTransition);

        if (m_callback == null)
        {
            m_callback = new Interop.SimpleCallback(UpdateParams);
        }

        Interop.SetPlayerLinkCallback(pointer, m_callback);

        Interop.ObjectOnCreate(pointer);

        return pointer;
    }

    public override bool Transition(float normalTime, ComboController controller)
    {
        if (m_inputNormal.InRange(normalTime))
        {
            return base.Transition(normalTime, controller);                                                
        }

        return false;
    }

    public override void UpdateTransition(float normal, ComboController controller)
    {
        //Debug.Log("Updating link");
        //Debug.Log(m_fireNormal.ToString());
        if (m_fireNormal.InRange(normal))
        {
            //Debug.Log("firing transition");
            controller.FireTransition();
        }
    }

    public override void BeginTransition(ComboController controller)
    {
        controller.BeginTransition(this, b_instantTransition);
    }

    internal override void Init()
    {
        Normalise(Origin.Move.FrameCount);
    }

    public void Normalise(int frameCount)
    {
        m_inputNormal = Period.Normalise(m_inputPeriod, frameCount);
        m_fireNormal = Period.Normalise(m_firePeriod, frameCount);
    }

    public Period InputPeriodNormal
    {        
        get
        {
            return m_inputNormal;
        }
    }

    public Period InputPeriod
    {
        set
        {
            m_inputPeriod = value;
            m_inputNormal = Period.Normalise(m_inputPeriod, Origin.Move.FrameCount);
        }
        get
        {
            return m_inputPeriod;
        }
    }

    public Period FirePeriod
    {
        set
        {
            m_firePeriod = value;
            m_fireNormal = Period.Normalise(m_firePeriod, Origin.Move.FrameCount);
        }
        get
        {
            return m_firePeriod;
        }
    }    
}