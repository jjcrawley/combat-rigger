﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXPool
{
    private static FXPool s_instance;

    private Dictionary<string, List<FXController>> m_pooledObjects;

    private Dictionary<string, FXTable> m_registeredTables;
        
    static FXPool()
    {
        s_instance = new FXPool();
    }

    private FXPool()
    {
        m_pooledObjects = new Dictionary<string, List<FXController>>();
        m_registeredTables = new Dictionary<string, FXTable>();
    }

    public void RegisterTable(FXTable table)
    {
        if(m_registeredTables.ContainsKey(table.name))
        {
            return;
        }

        PoolSettings[] settings = table.FXPoolSettings;

        for(int i = 0; i < settings.Length; i++)
        {
            AddFX(settings[i]);
        }

        m_registeredTables.Add(table.name, table);
    }
    
    public void AddFX(string key, GameObject toPool, int amount)
    {
        if (m_pooledObjects.ContainsKey(key))
        {
            return;
        }
        
        if(toPool.GetComponent<FXController>() == null)
        {
            Debug.Log("The effect prefab needs to have an FxController on it");
            return;
        }

        List<FXController> controllers = new List<FXController>();

        for (int i = 0; i < amount; i++)
        {
            GameObject newObject = Object.Instantiate(toPool);
            
            newObject.transform.position = Vector3.zero;
            //newObject.hideFlags = HideFlags.HideInHierarchy;

            FXController controller = newObject.GetComponent<FXController>();
                        
            controllers.Add(controller);
            
            newObject.SetActive(false);
        }

        m_pooledObjects.Add(key, controllers);
    }

    public void AddFX(PoolSettings settings)
    {
        AddFX(settings.poolKey, settings.pooledObject, settings.amount);
    }

    public FXController GetController(string key)
    {
        List<FXController> controllers;

        if(m_pooledObjects.TryGetValue(key, out controllers))
        {            
            for (int i = 0; i < controllers.Count; i++)
            {
                if (!controllers[i].gameObject.activeInHierarchy)
                {
                    return controllers[i];
                }
            }
        }

        return null;
    }

    public void StopFX(string key)
    {
        List<FXController> controllers;
        
        if(m_pooledObjects.TryGetValue(key, out controllers))
        {
            for (int i = 0; i < controllers.Count; i++)
            {
                if (controllers[i].gameObject.activeInHierarchy)
                {
                    controllers[i].Stop();
                }
            }
        }                
    }

    public FXTable GetTable(string name)
    {
        FXTable table = null;

        m_registeredTables.TryGetValue(name, out table);

        return table;
    }

    public static FXPool Instance
    {
        get
        {
            return s_instance;
        }
    }
}

[System.Serializable]
public struct PoolSettings
{
    [Tooltip("The pooled object's key")]
    public string poolKey;

    [Tooltip("The object to pool")]
    public GameObject pooledObject;

    [Tooltip("The number of copies to create")]
    public int amount;
}

public enum PositionSettings
{
    Follow,
    Spawn
}