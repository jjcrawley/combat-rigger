﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePlayerLink : ComboLink
{
    [SerializeField, Tooltip("The name of the button we'll query from the input handler")]
    protected string m_buttonName;
    
    internal override void CreateBackend(IntPtr link)
    {
        Interop.SetButtonName(link, m_buttonName);

        base.CreateBackend(link);
    }

    public override bool Transition(float normalTime, ComboController controller)
    {
        if (controller.CurrentInputHandler.ButtonDown(m_buttonName))
        {
            return base.Transition(normalTime, controller);
        }

        return false;
    }

    public string ButtonName
    {
        set
        {
            m_buttonName = value;
        }
        get
        {
            return m_buttonName;
        }
    }
}