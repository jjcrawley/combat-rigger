﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Serialization;

[RequireComponent(typeof(HitboxController), typeof(Animator))]
public class ComboController : MonoBehaviour, IIHitboxListener
{
    internal static Animator s_lastAnimator;
    internal static IInputHandler s_lastHandler;
    internal static HitboxController s_lastHitboxRig;
    internal static HitInfo s_lastInfo;
    internal static MoveModifierInfo s_modifierInfo = new MoveModifierInfo();

    [SerializeField, Tooltip("The handler that will be used by the combo controller, if one isn't assigned then it will find use the handler on the gameobject")]
    private InputHandler m_handler;

    [SerializeField, Tooltip("The combo box to be used by the combo controller"), FormerlySerializedAs("m_testComboBox")]
    private ComboBox m_comboBox = null;

    [SerializeField, Tooltip("The source that hit sounds can be played through. Can be null, will use the source on the game object in that case.")]
    private AudioSource m_audioSource;

    private Animator m_animator;

    private ComboState m_currentState;
    private ComboState m_nextState;
    private ComboLink m_currentLink;

    private ComboTree m_currentTree;

    private bool m_firedTransition = false;

    private MoveModifier[] m_modifiers = null;

    private ComboLock[] m_allLocks = null;

    private IntPtr m_pointer = IntPtr.Zero;

    private Interop.SimpleCallback m_timingCallback;
    private Interop.Debugger m_debugCallback;

    public bool testBackend = true;

    private bool b_transitionPending;

    private InputHandlerBackend m_backendRep;
    private HitboxControllerBackend m_hitboxBackend;

    private HitboxController m_hitboxRig;
    
    private Dictionary<int, List<MoveModifier>> m_moveModifiers;

    private StateInfo m_currentStateInfo;
    private StateInfo m_nextStateInfo;

    internal DamageController damageController;

    private void Awake()
    {
        m_moveModifiers = new Dictionary<int, List<MoveModifier>>();

        if (m_pointer != IntPtr.Zero)
        {
            return;
        }

        if (m_handler == null)
        {
            m_handler = GetComponent<InputHandler>();
            
            if (m_handler == null && m_comboBox != null)
            {
                m_handler.Settings = m_comboBox.InputSettings;
            }
        }        

        m_animator = GetComponent<Animator>();

        if (m_animator != null)
        {
            if (m_animator.runtimeAnimatorController == null || m_animator.runtimeAnimatorController != m_comboBox.RunTimeAnim)
            {
                m_animator.runtimeAnimatorController = m_comboBox.RunTimeAnim;

                if (m_animator.runtimeAnimatorController != m_comboBox.RunTimeAnim)
                {
                    Debug.Log("The animation controller in the animator was different to the one given to the combo box, I took the liberty of changing it.");
                }
            }
        }

        m_hitboxRig = GetComponent<HitboxController>();

        if(m_audioSource == null)
        {
            m_audioSource = GetComponent<AudioSource>();
        }

        damageController = new DamageController(this, m_hitboxRig);

        if (m_hitboxRig != null)
        {
            m_hitboxRig.Setup(gameObject, this);
        }

        m_debugCallback = new Interop.Debugger(PrintThing);

        Interop.SetDebugCallback(m_debugCallback);
        
        s_modifierInfo.controller = this;

        if (testBackend)
        {
            m_comboBox.Init(this);
            BuildBackend();
        }
        else
        {            
            m_comboBox.Init(this);
            
            //m_modifiers = m_comboBox.GetMods();

            //for (int i = 0; i < m_modifiers.Length; i++)
            //{
            //    m_modifiers[i].Setup(this);
            //}
            
            m_currentTree = m_comboBox[0];
            m_currentState = m_currentTree.Start;
        }
    }

    private void OnEnable()
    {
        if(m_comboBox == null)
        {
            Debug.Log("I require a combo box to work");
            enabled = false;
        }
    }

    private void PrintThing(string message)
    {
        Debug.Log(message);
    }

    private void OnDestroy()
    {
        if (m_pointer != IntPtr.Zero)
        {
            Interop.ObjectOnRelease(m_pointer);
            //Interop.ReleaseComboController(m_pointer);

            m_backendRep.OnDestroy();
            m_hitboxBackend.OnDestroy();

            m_pointer = IntPtr.Zero;
        }
    }

    private void BuildBackend()
    {
        m_timingCallback = new Interop.SimpleCallback(TimingCallback);

        m_pointer = Interop.CreateComboController();
        m_backendRep = new InputHandlerBackend();
        m_backendRep.Init(m_handler, this);

        m_hitboxBackend = new HitboxControllerBackend(m_hitboxRig, m_pointer, this);

        //Debug.Log(m_pointer.ToString());
        Interop.SetParamsCallback(m_pointer, m_timingCallback);
        Interop.AssignInputHandler(m_pointer, m_backendRep.Pointer);

        IntPtr box = m_comboBox.BuildBackend();

        Interop.AssignComboBox(m_pointer, box);

        //m_modifiers = m_comboBox.GetMods();

        //for(int i = 0; i < m_modifiers.Length; i++)
        //{
        //    m_modifiers[i].Setup(this);
        //}

        m_allLocks = m_comboBox.GetLocks();

        Interop.ObjectOnCreate(m_pointer);
    }

    private void TestReleaseAndCreate()
    {
        BuildBackend();

        OnDestroy();
    }
    
    private void Update()
    {
        s_modifierInfo.controller = this;

        if (testBackend)
        {
            UpdateBackend();
        }
        else
        {
            UpdateFront();
        }
    }

    private void UpdateFront()
    {
        AnimatorStateInfo info = m_animator.GetCurrentAnimatorStateInfo(0);

        float normalTime = info.normalizedTime;

        if (m_firedTransition)
        {            
            m_currentState.UpdateState(normalTime, this);
                        
            info = m_animator.GetNextAnimatorStateInfo(0);

            m_nextState.UpdateState(info.normalizedTime, this);            

            return;
        }
                
        if (m_currentLink != null)
        {
            m_currentLink.UpdateTransition(normalTime, this);
        }

        //TestReleaseAndCreate();
        s_lastAnimator = m_animator;
        s_lastHandler = m_handler;
        s_lastHitboxRig = m_hitboxRig;

        m_currentState.UpdateState(normalTime, this);
    }

    private void UpdateBackend()
    {
        AnimatorStateInfo info = m_animator.GetCurrentAnimatorStateInfo(0);

        float normalTime = info.normalizedTime;

        float secondNormal = m_animator.GetNextAnimatorStateInfo(0).normalizedTime;
                
        s_lastAnimator = m_animator;
        s_lastHandler = m_handler;
        s_lastHitboxRig = m_hitboxRig;
        s_modifierInfo.controller = this;

        m_handler.UpdateInput();

        Interop.UpdateInteropController(m_pointer, normalTime, secondNormal);
    }

    public void BeginTransition(ComboLink link, bool immediate = false)
    {
        m_currentLink = link;
        b_transitionPending = true;

        if (immediate)
        {
            FireTransition();
        }            
    }

    public void FireTransition()
    {
        m_currentLink.UpdateParams(m_animator);
        m_nextState = m_currentLink.Destination;
        m_currentLink = null;

        m_nextStateInfo = m_nextState.StateInfo;

        StartCoroutine(FrameTiming());
    }

    public void ResetController()
    {
        m_nextState = m_currentTree.ApplyReset(m_animator);
        b_transitionPending = true;
        //Debug.Log("Applying reset");
        StartCoroutine(FrameTiming());
    }

    public void OnHit(HitInfo info)
    {
        s_modifierInfo.controller = this;
                
        if (testBackend)
        {
            s_lastInfo = info;
            
            StateInfo current = new StateInfo();

            Interop.GetStateInfo(m_pointer, out current);

            info.move = CombatMoveLookup.GetMove(current.moveID);
            
            Interop.ProcessHit(m_pointer);

            info.move.ProcessHit(info);
        }
        else
        {
            StateInfo moveInfo;

            m_currentState.ProcessHit(out moveInfo);
                        
            info.move = CombatMoveLookup.GetMove(moveInfo.moveID);

            m_currentState.ProcessHit(info);
        }
    }

    public void OnContinuousHit(HitInfo info)
    {
        if (testBackend)
        {
            s_lastInfo = info;
            
            info.move.ProcessContinuousHit(info);
        }
        else
        {
            s_lastInfo = info;

            m_currentState.ProcessContinuousHit(s_lastInfo);
        }
    }

    internal bool TransitionPending
    {
        get
        {
            return b_transitionPending;
        }
    }

    private IEnumerator FrameTiming()
    {
        if(testBackend)
        {
            yield return FrameTimingBackend();
        }
        else
        {
            yield return FrameTimingFrontEnd();
        }
    }

    IEnumerator FrameTimingFrontEnd()
    {
        Debug.Log("Entered here");
        m_firedTransition = true;

        AnimatorStateInfo info = m_animator.GetNextAnimatorStateInfo(0);

        //Debug.Log("Entered here " + info.normalizedTime);

        while (info.shortNameHash == 0)
        {            
            yield return null;

            info = m_animator.GetNextAnimatorStateInfo(0);
        }

        s_modifierInfo.controller = this;

        m_nextState.OnEnter();
        
        while (m_animator.IsInTransition(0))
        {
            yield return null;
        }

        m_firedTransition = false;
        b_transitionPending = false;
                        
        s_modifierInfo.controller = this;

        m_currentStateInfo = m_currentState.StateInfo;

        m_currentState.OnExit();

        m_currentState = m_nextState;

        m_nextStateInfo.moveID = 0;

        m_currentStateInfo = m_currentState.StateInfo;

        m_currentLink = null;
        m_nextState = null;

        //Debug.Log("exited here");       
    }
        
    IEnumerator FrameTimingBackend()
    {        
        AnimatorStateInfo info = m_animator.GetNextAnimatorStateInfo(0);
                
        while (info.shortNameHash == 0)
        {
            yield return null;

            info = m_animator.GetNextAnimatorStateInfo(0);
        }

        s_modifierInfo.controller = this;

        Interop.TriggerEnter(m_pointer);
                
        while (m_animator.IsInTransition(0))
        {
            yield return null;
        }
                
        s_modifierInfo.controller = this;

        Interop.EndTransition(m_pointer);
    }    

    private void TimingCallback()
    {
        //Debug.Log("initiating frame stuff");
        StartCoroutine(FrameTiming());
    }

    public IInputHandler CurrentInputHandler
    {       
        get
        {
            return GetHandler();
        }
    }

    public HitboxController HitboxRig
    {
        get
        {
            return m_hitboxRig;
        }
    }

    public AudioSource AudioSource
    {
        get
        {
            return m_audioSource;
        }
    }

    public IInputSettings InputSettings
    {
        get
        {
            return GetHandler().Settings;
        }
    }

    private IInputHandler GetHandler()
    {
        if(m_handler != null)
        {
            return m_handler;            
        }
        else
        {
            m_handler = GetComponent<InputHandler>();

            return m_handler;
        }
    }    

    private class HitboxControllerBackend
    {
        private Interop.BoxHandler m_activatorHandler;
        private Interop.BoxHandler m_deactivateHandler;
        private Interop.FireDataHandler m_fireDataHandler;

        private HitboxController m_controller;
        private IntPtr m_pointer = IntPtr.Zero;

        private ComboController m_comboController;

        public HitboxControllerBackend(HitboxController controller, IntPtr backEndComboController, ComboController comboController)
        {
            Init(controller, backEndComboController);
            m_comboController = comboController;
        }

        public void Init(HitboxController controller, IntPtr comboController)
        {
            if(m_pointer != IntPtr.Zero)
            {
                OnDestroy();
            }

            m_pointer = Interop.CreateNewHitboxController();
            m_controller = controller;
            m_activatorHandler = ActivateHitBoxes;
            m_fireDataHandler = QueueFireData;
            m_deactivateHandler = DeactivateHitBoxes;

            Interop.SetHitboxController(comboController, m_pointer);
            Interop.SetHitBoxCallbacks(m_pointer, m_activatorHandler, m_deactivateHandler, m_fireDataHandler);
        }

        private void ActivateHitBoxes(int id)
        {
            try
            {
                m_controller.ActivateDamageBoxes(id);
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        private void DeactivateHitBoxes(int id)
        {
            try
            {
                m_controller.DisableDamageBoxes(id);
                m_comboController.damageController.ClearTimers();
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        private void QueueFireData(float interval)
        {
            try
            {
                m_comboController.damageController.RegisterFire(interval, s_lastInfo);
                //m_controller.QueueFireData(interval, s_lastInfo);
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        ~HitboxControllerBackend()
        {
            OnDestroy();
        }

        public void OnDestroy()
        {
            if(m_pointer != IntPtr.Zero)
            {
                Interop.ObjectOnRelease(m_pointer);
                //Interop.ReleaseHitboxController(m_pointer);
                m_pointer = IntPtr.Zero;
            }
        }
    }

    private class InputHandlerBackend : IInputHandler
    {
        private Interop.ButtonCallback m_buttonDown;
        private Interop.ButtonCallback m_buttonUp;
        private Interop.ButtonCallback m_buttonHeld;
        private Interop.AxisCallback m_axisRaw;
        private Interop.AxisCallback m_axis;

        private IntPtr m_pointer = IntPtr.Zero;
        private IInputHandler m_handler;
        
        public void Init(IInputHandler handler, ComboController controller)
        {
            if (m_pointer == IntPtr.Zero)
            {
                m_handler = handler;

                if (m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler", controller);
                }

                m_buttonDown = new Interop.ButtonCallback(ButtonDown);
                m_buttonUp = new Interop.ButtonCallback(ButtonUp);
                m_buttonHeld = new Interop.ButtonCallback(ButtonHeld);
                m_axis = new Interop.AxisCallback(GetAxis);
                m_axisRaw = new Interop.AxisCallback(GetAxisRaw);

                m_pointer = Interop.CreateInputHandler();
                Interop.RegisterInputButtonCallbacks(m_buttonDown, m_buttonUp, m_buttonHeld, m_pointer);
                Interop.RegisterAxisCallbacks(m_axisRaw, m_axis, m_pointer);
            }
        }

        public void OnDestroy()
        {
            if (m_pointer != IntPtr.Zero)
            {
                Interop.ObjectOnRelease(m_pointer);
                //Interop.ReleaseInputHandler(m_pointer);
                m_pointer = IntPtr.Zero;
            }
        }

        public void UpdateInput()
        {
            m_handler.UpdateInput();
        }

        public bool ButtonDown(string name)
        {
            try
            {
                return m_handler.ButtonDown(name);
            }
            catch(Exception exception)
            {
                if(m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler");
                }

                Debug.LogException(exception);
                return false;
            }
        }

        public bool ButtonHeld(string name)
        {
            try
            {
                return m_handler.ButtonHeld(name);
            }
            catch(Exception e)
            {
                if (m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler");
                }
                
                Debug.LogException(e);
                return false;
            }
        }

        public bool ButtonUp(string name)
        {
            try
            {
                return m_handler.ButtonUp(name);
            }
            catch (Exception e)
            {
                if (m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler");
                }

                Debug.LogException(e);
                return false;
            }
        }

        public float GetAxisRaw(string name)
        {
            try
            {
                return m_handler.GetAxisRaw(name);
            }
            catch (Exception e)
            {
                if (m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler");
                }

                Debug.LogException(e);
                return 0;
            }
        }

        public float GetAxis(string name)
        {
            try
            {
                return m_handler.GetAxis(name);
            }
            catch (Exception e)
            {
                if(m_handler == null)
                {
                    Debug.Log("This combo controller needs an input handler");
                }

                Debug.LogException(e);
                return 0;
            }
        }

        public IntPtr Pointer
        {
            get
            {
                return m_pointer;
            }
        }

        public IInputSettings Settings
        {            
            set
            {
                m_handler.Settings = value;
            }
            get
            {
                return m_handler.Settings;
            }
        }
    }            
}

public interface ICombatEntity
{

}