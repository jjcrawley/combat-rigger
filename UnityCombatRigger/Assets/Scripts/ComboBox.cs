﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

[CreateAssetMenu(menuName = "Combo Box/Create Combo box", fileName = "ComboBox")]
public class ComboBox : ScriptableObject
{
    [SerializeField]
    private ComboTree[] m_trees;

    [SerializeField, Tooltip("The animator controller that will be assigned to the animator at runtime. This is also used by the Combo box window to provide the parameter dropdown for the combo link parameter settings")]
    private RuntimeAnimatorController m_syncController;

    [SerializeField, Tooltip("An input settings asset that is used for quick button assignment in the combo link inspector. These settings will also be passed to the input handler at runtime, can be null")]
    private InputSettings m_inputSettings;     
       
    private void OnEnable()
    {
        if(m_trees == null)
        {
            m_trees = new ComboTree[0];
        }        
    }    

    internal IntPtr BuildBackend()
    {
        IntPtr pointer = Interop.CreateComboBox();

        for(int i = 0; i < m_trees.Length; i++)
        {
            m_trees[i].BuildBackend(pointer);           
        }

        Interop.ObjectOnCreate(pointer);

        return pointer;
    }

    internal MoveModifier[] GetMods()
    {
        List<MoveModifier> mods = new List<MoveModifier>();

        for(int i = 0; i < m_trees.Length; i++)
        {
            mods.AddRange(m_trees[i].AllMods);
        }

        return mods.ToArray();
    }

    internal ComboLock[] GetLocks()
    {
        List<ComboLock> locks = new List<ComboLock>();

        for(int i = 0; i < m_trees.Length; i++)
        {
            locks.AddRange(m_trees[i].AllLocks);
        }

        return locks.ToArray();
    }

    public InputSettings InputSettings
    {
        get
        {
            return m_inputSettings;
        }
    }

    public RuntimeAnimatorController RunTimeAnim
    {
        get
        {
            return m_syncController;
        }
    }

    public void Init(ComboController controller)
    {
        for (int i = 0; i < m_trees.Length; i++)
        {
            m_trees[i].Init(controller);
        }
    }

    public ComboTree[] Trees
    {
        get
        {
            return m_trees;
        }
    }

    public ComboTree this[int index]
    {
        get
        {
            return m_trees[index];
        }
    }

#if UNITY_EDITOR   

    private void RemoveTree(ComboTree tree)
    {
        if (ArrayUtility.Contains(m_trees, tree))
        {
            ArrayUtility.Remove(ref m_trees, tree);
        }
    }

    public void RemoveTree(int index)
    {
        if(index >= 0 && index < m_trees.Length)
        {
            ComboTree tree = m_trees[index];

            if (AssetDatabase.Contains(this))
            {
                Undo.RegisterCompleteObjectUndo(this, "Removed tree");

                RemoveTree(tree);

                Undo.DestroyObjectImmediate(tree);

                tree.DestroyTree();                
            }
            else
            {
                RemoveTree(tree);
            }
        }
    }

    private void AddTree(ComboTree tree)
    {
        if (!ArrayUtility.Contains(m_trees, tree))
        {
            ArrayUtility.Add(ref m_trees, tree);
        }
    }

    public ComboTree AddTree()
    {
        ComboTree tree = CreateInstance<ComboTree>();

        tree.hideFlags = HideFlags.HideInHierarchy;
        tree.name = "ComboTree";
        tree.name = GetUniqueTreeName(tree);

        if (AssetDatabase.Contains(this))
        {
            Undo.RegisterCreatedObjectUndo(tree, "Added tree");
            Undo.RegisterCompleteObjectUndo(this, "Added tree");

            AssetDatabase.AddObjectToAsset(tree, this);

            EditorUtility.SetDirty(this);

            tree.Setup();

            AddTree(tree);                                  
        }
        else
        {    
            AddTree(tree);                      
        }

        return tree;
    }

    public string GetUniqueTreeName(ComboTree tree)
    {
        string finalName = tree.name;

        int currentIteration = 0;

        for (int i = 0; i < m_trees.Length; i++)
        {
            if(finalName == m_trees[i].name && m_trees[i] != tree)
            {
                currentIteration++;

                finalName = tree.name + currentIteration;

                i = -1;                
            }            
        }

        return finalName;
    }

    public void ReorderTrees(ComboTree[] reorderedTrees)
    {
        Undo.RegisterCompleteObjectUndo(this, "Tree reordering");

        Array.Copy(reorderedTrees, m_trees, reorderedTrees.Length);

        EditorUtility.SetDirty(this);      
    }    

    public static ComboBox CreateComboBoxAtPath(string assetPath)
    {
        ComboBox box = CreateInstance<ComboBox>();

        string path = AssetDatabase.GenerateUniqueAssetPath(assetPath);

        AssetDatabase.CreateAsset(box, path);

        return box;
    }

    public AnimatorController AnimController
    {
        set
        {
            m_syncController = value;
        }
        get
        {
            return m_syncController as AnimatorController;
        }
    }
#endif
}