﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveController : FXController
{
    private ParticleSystem m_system;

    private bool m_disabling;

    private void Awake()
    {
        m_system = GetComponentInChildren<ParticleSystem>();        
    }

    public override void Play()
    {
        m_system.Play(true);
    }

    public override void Stop()
    {
        if (!m_disabling)
        {
            m_disabling = true;

            StartCoroutine(Disable());
        }
        //m_system.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(2);
                
        m_disabling = false;

        base.Stop();
    }
}