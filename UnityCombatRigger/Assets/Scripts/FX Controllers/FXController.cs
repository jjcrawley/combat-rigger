﻿using System.Collections;
using UnityEngine;

public class FXController : MonoBehaviour
{
    private Coroutine m_followRoutine;
        
    private IEnumerator FollowTarget(Transform target)
    {
        while(true)
        {
            transform.position = target.position;

            yield return null;
        }
    }

    protected void FollowTransform(Transform target)
    {
        m_followRoutine = StartCoroutine(FollowTarget(target));
    }

    public virtual void Play(FX settings, Transform targetTransform)
    {
        if(settings.positionSetting == PositionSettings.Follow)
        {
            FollowTransform(targetTransform);
        }        
        else
        {
            transform.position = targetTransform.position;
        }

        Play();
    }

    public virtual void Play()
    {
        gameObject.SetActive(true);
    }

    public virtual void Stop()
    {
        if(m_followRoutine != null)
        {
            StopCoroutine(m_followRoutine);
        }

        gameObject.SetActive(false);
    }
}