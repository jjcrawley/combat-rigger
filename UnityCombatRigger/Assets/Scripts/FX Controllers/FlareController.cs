﻿using System.Collections;
using UnityEngine;

public class FlareController : FXController
{
    private ParticleSystem m_system;

    private void Awake()
    {
        m_system = GetComponent<ParticleSystem>();
    }
        
    public override void Play()
    {        
        m_system.Play(true);

        StartCoroutine(Disable());       
    }

    public override void Stop()
    {
    }

    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(2.0f);

        m_system.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);

        base.Stop();
    }
}