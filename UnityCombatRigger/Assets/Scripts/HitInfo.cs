﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HitInfo
{
    internal ComboController senderController;
    internal BaseHitbox damageBox;

    internal IDamageableEntity hitEntity;
    internal GameObject hitObject;
    internal Component hitCollider;

    internal CombatMove move;
    
    public ComboController Controller
    {
        get
        {
            return senderController;
        }
    }

    public IDamageableEntity HitEntity
    {
        get
        {
            return hitEntity;
        }
    }

    public GameObject HitObject
    {
        get
        {
            return hitObject;
        }
    }

    public BaseHitbox Hitbox
    {
        get
        {
            return damageBox;
        }
    }
        
    public Collider2D Collider2D
    {
        get
        {
            return hitCollider as Collider2D;
        }
    }

    public Collider Collider
    {
        get
        {
            return hitCollider as Collider;
        }
    }
}