﻿using System;
using UnityEngine;

[Serializable]
public class FX
{
    [Tooltip("The name of the fx to pull from the fxTable")]
    public string fxName;

    [Tooltip("The positioning type. This can be either follow or spawn. Follow will cause the effect to follow the target transform, spawn will spawn it at the transform's position")]
    public PositionSettings positionSetting;

    [TranformPreview, Tooltip("The path to use for positioning")]
    public string transformPath;
}