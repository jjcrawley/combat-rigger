﻿using UnityEngine;
using System;

public abstract class ComboLock : ScriptableObject
{
    private Interop.InputCallback m_callback;
        
    public IntPtr GetBackend(IntPtr link)
    {
        IntPtr lockPointer = Interop.CreateAndAddLinkLock(link);

        if(m_callback == null)
        {
            m_callback = new Interop.InputCallback(Unlock);
        }

        Interop.SetLinkLockInputCallback(lockPointer, m_callback);

        Interop.ObjectOnCreate(lockPointer);

        return lockPointer;
    }

    private bool Unlock(float normal)
    {
        try
        {
            return Unlock(normal, ComboController.s_modifierInfo.controller);
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
            return false;
        }
    }

    public abstract bool Unlock(float normal, ComboController handler);   
}