﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputHandler : MonoBehaviour, IInputHandler
{
    public abstract IInputSettings Settings { get; set; }

    public abstract bool ButtonDown(string name);
    public abstract bool ButtonHeld(string name);
    public abstract bool ButtonUp(string name);
    public abstract float GetAxis(string name);
    public abstract float GetAxisRaw(string name);
    public abstract void UpdateInput();
}

public interface IInputHandler
{
    void UpdateInput();
    bool ButtonDown(string name);
    bool ButtonHeld(string name);
    bool ButtonUp(string name);
    float GetAxisRaw(string name);
    float GetAxis(string name);
    IInputSettings Settings { get; set; } 
}

public interface IInputSettings
{
    List<string> ButtonNames { get; }
}