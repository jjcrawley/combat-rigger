﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour, IDamageableEntity
{
    private DamageableEntity m_parent;

    private void Awake()
    {
        m_parent = GetComponentInParent<DamageableEntity>();
    }

    public void TakeDamage(ComboController controller, float damage)
    {
        m_parent.TakeDamage(controller, damage);
    }
}