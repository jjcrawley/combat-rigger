﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ComboLink : ScriptableObject
{
    [SerializeField, Tooltip("The animation parameters to setup for the transition")]
    private AnimatorParam[] m_params;
       
    [SerializeField]
    private ComboState m_destination;

    [SerializeField, HideInInspector]
    private ComboState m_origin;   
     
    [SerializeField, Tooltip("The combo locks for the link")] 
    private ComboLock[] m_locks;

    private Interop.SimpleCallback m_paramSetup;

    private IntPtr m_tempPointer;

    private void OnEnable()
    {
        if (m_locks == null)
        {
            m_locks = new ComboLock[0];
        }

        if (m_params == null)
        {
            m_params = new AnimatorParam[0];
        }

        SetupParams();

        //m_locks.Clear();        
    }   
    
    private void SetupParams()
    {
        for(int i = 0; i < m_params.Length; i++)
        {
            m_params[i].Setup();
        }
    } 

    internal virtual IntPtr CreateBackend(IntPtr start, IntPtr destination)
    {
        SetupParams();
        
        m_tempPointer = Interop.CreateAndAddLink(start, destination);

        CreateBackend(m_tempPointer);

        //Debug.Log("hello");
        
        return m_tempPointer;
    }

    internal virtual void CreateBackend(IntPtr link)
    {
        SetupParams();

        if (m_paramSetup == null)
        {
            m_paramSetup = new Interop.SimpleCallback(UpdateParams);                                    
        }

        Interop.SetLinkCallback(link, m_paramSetup);

        m_tempPointer = link;
        
        Interop.ObjectOnCreate(m_tempPointer);
    }

    internal ComboLock[] CreateLockBackend()
    {
        ComboLock[] locks = new ComboLock[m_locks.Length];

        for(int i = 0; i < m_locks.Length; i++)
        {
            ComboLock temp = Instantiate(m_locks[i]);

            locks[i] = temp;

            locks[i].GetBackend(m_tempPointer);
        }

        return locks;
    }

    public virtual void UpdateTransition(float normal, ComboController controller)
    {          
    }

    public virtual bool Transition(float normalTime, ComboController controller)
    {        
        for(int i = 0; i < m_locks.Length; i++)
        {
            if(!m_locks[i].Unlock(normalTime, controller))
            {
                return false;
            }
        }

        return true;
    }

    public virtual void BeginTransition(ComboController controller)
    {
        controller.BeginTransition(this, true);
    }                   

    public void UpdateParams(Animator animator)
    {        
        for(int i = 0; i < m_params.Length; i++)
        {
            m_params[i].Setup(animator);
        }
    }

    internal protected void UpdateParams()
    {
        UpdateParams(ComboController.s_lastAnimator);
        //ComboController.s_lastAnimator.SetTrigger(m_triggerName);

        //Debug.Log("I was called");
    }

    internal virtual void Init()
    {

    }

#if UNITY_EDITOR

    public ComboLock AddLock(Type lockType)
    {
        ComboLock comboLock = (ComboLock)CreateInstance(lockType);
        comboLock.hideFlags = HideFlags.HideInHierarchy;

        Undo.RegisterCreatedObjectUndo(comboLock, "Added lock");
        Undo.RegisterCompleteObjectUndo(this, "Added lock");       

        if (AssetDatabase.Contains(this))
        {
            //Debug.Log("added a lock");
            AssetDatabase.AddObjectToAsset(comboLock, this);
            EditorUtility.SetDirty(this);                       
        }

        ArrayUtility.Add(ref m_locks, comboLock);

        return comboLock;
    }

    public T AddComboLock<T>() where T : ComboLock
    {
        return (T)AddLock(typeof(T));        
    }

    public void RemoveLock(int index)
    {
        if(index < m_locks.Length && index > -1)
        {
            ComboLock theLock = m_locks[index];

            Undo.RegisterCompleteObjectUndo(this, "Removed Lock");

            EditorUtility.SetDirty(this);

            ArrayUtility.RemoveAt(ref m_locks, index);

            Undo.DestroyObjectImmediate(theLock);           
        }
    }

    public T[] GetLocksOfType<T>() where T : ComboLock
    {
        List<T> objects = new List<T>();

        for(int i = 0; i < m_locks.Length; i++)
        {
            if(m_locks[i].GetType() == typeof(T))
            {
                objects.Add((T)m_locks[i]);
            }
        }

        return objects.ToArray();
    }

    public void AddParam(AnimatorControllerParameterType type, string name)
    {
        Undo.RegisterCompleteObjectUndo(this, "Added parameter");

        AnimatorParam param = new AnimatorParam();
        param.ParameterName = name;
        param.Type = type;

        ArrayUtility.Add(ref m_params, param);

        EditorUtility.SetDirty(this);
    }

    public void RemoveParam(int index)
    {
        Undo.RegisterCompleteObjectUndo(this, "Removed Parameter");

        ArrayUtility.RemoveAt(ref m_params, index);

        EditorUtility.SetDirty(this);
    }

    public bool IsOwner(ComboLock toCheck)
    {
        return ArrayUtility.Contains(m_locks, toCheck);
    }

    public void DestroyLink()
    {
        //Undo.RegisterCompleteObjectUndo(this, "Removed link");

        for(int i = 0; i < m_locks.Length; i++)
        {
            Undo.DestroyObjectImmediate(m_locks[i]);
        }       
    }

    public AnimatorParam[] Params
    {
        get
        {
            return m_params;
        }
    }


#endif

    public ComboLock this[int index]
    {
        get
        {
            return m_locks[index];
        }
    }

    public ComboLock[] Locks
    {
        get
        {
            return m_locks;
        }
    }

    public ComboState Destination
    {
        set
        {
            m_destination = value;
        }
        get
        {
            return m_destination;
        }
    }

    public ComboState Origin
    {
        set
        {
            m_origin = value;
        }
        get
        {
            return m_origin;
        }
    }
}