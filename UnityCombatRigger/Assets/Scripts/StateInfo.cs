﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct StateInfo
{
    internal int moveID;
        
    public CombatMove Move
    {
        get
        {
            return CombatMoveLookup.GetMove(moveID);
        }
    }
}