﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "CombatMove", menuName = "Combo Box/Combat Move")]
public class CombatMove : ScriptableObject
{
    [SerializeField, Tooltip("The animation clip for the combat move. This is used for setting up frame timing and is recommended. It can be null, then you'll have to manually input a frame count for the clip.")]
    private AnimationClip m_clip;    
    
    [SerializeField, Multiline, Tooltip("A small description of the combat move")]
    private string m_description;
       
    [SerializeField]
    private Period m_activationPeriod;   

    [SerializeField, Tooltip("Should the move apply continuous damage to the target")]
    private bool b_continuous;

    [SerializeField, Tooltip("The interval at which the continuous damage will be applied, in seconds")]
    private float m_damageInterval;

    [SerializeField]
    private HitboxInformation[] m_hitboxes = new HitboxInformation[0];

    [SerializeField]
    private MoveModifier[] m_modifiers;

    [SerializeField, Tooltip("The damage of the move")]
    private float m_damage;     
       
    private Period m_activationNormal;
    
    [SerializeField, Tooltip("The frame count of the move, required if the move isn't assigned an animation clip")]    
    private int m_frameCount;

    private IntPtr m_backend;

    private Interop.UpdateCallback m_updateCallback;

    private Interop.SimpleCallback m_enterCallback;

    private Interop.SimpleCallback m_exitCallback;

    protected virtual void OnEnable()
    {
        if(m_clip != null)
        {
            m_frameCount = Mathf.RoundToInt(m_clip.length * m_clip.frameRate);            
            m_activationNormal = Period.Normalise(m_activationPeriod, m_frameCount);
        }        
                
        if(m_modifiers == null)
        {
            m_modifiers = new MoveModifier[0];
        }

        CombatMoveLookup.AddMove(this);
    }

    internal void Init(ComboController controller)
    {
        int length = m_modifiers.Length;

        for(int i = 0; i < length; i++)
        {
            m_modifiers[i].Setup(controller);
        }
    }

    protected virtual void OnDisable()
    {
        //Debug.Log("Destroyed");
        Interop.ReleaseMoveInfo(m_backend);

        m_backend = IntPtr.Zero;      
    }

    internal MoveModifier[] GetMods()
    {        
        MoveModifier[] mods = new MoveModifier[m_modifiers.Length];

        for(int i = 0; i < m_modifiers.Length; i++)
        {
            mods[i] = Instantiate(m_modifiers[i]);
        }

        return mods;
    }

    internal IntPtr CreateBackend()
    {
        if (m_backend == IntPtr.Zero)
        {
            m_backend = Interop.CreateCombatMoveInfo();

            Interop.SetMoveName(m_backend, name);

            Interop.SetMoveLength(m_backend, FrameCount);
                       
            Interop.SetMoveContinuous(m_backend, b_continuous);

            Interop.SetMoveDamageInterval(m_backend, m_damageInterval);

            Interop.SetMoveID(m_backend, GetInstanceID());

            Interop.SetMoveActivationPeriod(m_backend, m_activationPeriod.start, m_activationPeriod.end);

            m_updateCallback = new Interop.UpdateCallback(OnUpdate);
            m_enterCallback = new Interop.SimpleCallback(OnEnter);
            m_exitCallback = new Interop.SimpleCallback(OnExit);

            Interop.SetMoveCallbacks(m_backend, m_enterCallback, m_exitCallback, m_updateCallback);

            for(int i = 0; i < m_modifiers.Length; i++)
            {
                m_modifiers[i].GetMoveBackend(m_backend);
            }

            Interop.ObjectOnCreate(m_backend);
        }

        return m_backend;
    }

    public List<BaseHitbox> BuildHitboxSetup(HitboxController rig, ComboController controller)
    {
        List<BaseHitbox> damageBoxes = new List<BaseHitbox>();

        for (int i = 0; i < m_hitboxes.Length; i++)
        {
            //Debug.Log(m_hitboxes[i].GetHashCode());
            m_hitboxes[i].Move = this;
            rig.AddHitbox(this, m_hitboxes[i], rig.gameObject, controller);
        }

        return damageBoxes;
    }

    public int FrameCount
    {
        get
        {
            if (m_clip == null)
            {
                return m_frameCount;
            }
            else
            {
                return m_frameCount = Mathf.RoundToInt(m_clip.length * m_clip.frameRate);
            }
        }
    }

    public virtual void ProcessHit(HitInfo info)
    {
        info.hitEntity.TakeDamage(info.senderController, m_damage);

        int length = m_modifiers.Length;

        for (int i = 0; i < length; i++)
        {
            m_modifiers[i].OnFirstHit(info, ComboController.s_modifierInfo);
        }
    }

    public void ProcessContinuousHit(HitInfo info)
    {
        info.hitEntity.TakeDamage(info.senderController, m_damage);
    }

    public string Description
    {
        set
        {
            m_description = value;
        }
        get
        {
            return m_description;
        }
    }

    private void OnEnter()
    {
        try
        {
            OnEnter(ComboController.s_modifierInfo);
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
        }
    }
    
    public virtual void OnEnter(MoveModifierInfo info)
    {
        int length = m_modifiers.Length;

        for(int i = 0; i < length; i++)
        {
            m_modifiers[i].OnEnter(info);
        }
    }

    private void OnExit()
    {
        try
        {
            OnExit(ComboController.s_modifierInfo);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public virtual void OnExit(MoveModifierInfo info)
    {
        int length = m_modifiers.Length;

        for(int i = 0; i < length; i++)
        {
            m_modifiers[i].OnExit(info);
        }
    }

    private void OnUpdate(float normalTime)
    {
        try
        {
            OnUpdate(normalTime, ComboController.s_modifierInfo);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public virtual void OnUpdate(float normalTime, MoveModifierInfo info)
    {
        int length = m_modifiers.Length;

        for(int i = 0; i < length; i++)
        {
            m_modifiers[i].OnUpdate(normalTime, info);
        }
    }

    public bool HitboxesOpen(float normal)
    {
        return m_activationNormal.InRange(normal);
    }

    public bool HitboxesOpen(int frame)
    {
        return m_activationPeriod.InRange(frame);
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        if(m_clip != null)
        {
            m_frameCount = Mathf.RoundToInt(m_clip.length * m_clip.frameRate);
        }        
    }   

    public T AddModifier<T>() where T : MoveModifier
    {
        T mod = CreateInstance<T>();

        AddModifier(mod);

        return mod;       
    }

    public MoveModifier AddModifier(Type type)
    {
        MoveModifier mod = (MoveModifier)CreateInstance(type);

        AddModifier(mod);

        return mod;
    }

    private void AddModifier(MoveModifier mod)
    {
        mod.Move = this;
        mod.hideFlags = HideFlags.HideInHierarchy;

        Undo.RegisterCreatedObjectUndo(mod, "Added modifier");
        Undo.RegisterCompleteObjectUndo(this, "Added modifier");

        if (AssetDatabase.Contains(this))
        {
            AssetDatabase.AddObjectToAsset(mod, this);

            EditorUtility.SetDirty(this);

            ArrayUtility.Add(ref m_modifiers, mod);
        }
    }

    public void RemoveModifier(int index)
    {
        RemoveModifier(m_modifiers[index]);
    }

    public void RemoveModifier(MoveModifier modifier)
    {
        Undo.RegisterCompleteObjectUndo(this, "Removed modifier");
       
        if(ArrayUtility.Contains(m_modifiers, modifier))
        {
            EditorUtility.SetDirty(this);

            Undo.DestroyObjectImmediate(modifier);

            ArrayUtility.Remove(ref m_modifiers, modifier);
        }
    }      

    public AnimationClip Clip
    {
        set
        {
            m_clip = value;
            m_frameCount = FrameCount;
        }
        get
        {
            return m_clip;
        }
    }

    public MoveModifier[] Modifiers
    {
        get
        {
            return m_modifiers;
        }
    }

#endif

    public bool Continuous
    {
        get
        {
            return b_continuous;
        }
    }

    public float DamageInterval
    {
        get
        {
            return m_damageInterval;
        }
    }

    public HitboxInformation[] HitboxInfo
    {
        get
        {
            return m_hitboxes;
        }
    }
}

[Serializable]
public struct Period
{
    public float start;
    public float end;

    public bool InRange(float normalTime)
    {
        return normalTime >= start && normalTime <= end;
    }

    public static Period Normalise(Period period, int frameCount)
    {
        Period normal;

        normal.start = period.start / frameCount;
        normal.end = period.end / frameCount;

        return normal;
    }

    public override string ToString()
    {
        return "Period Start: " + start + ", End: " + end;
    }
}