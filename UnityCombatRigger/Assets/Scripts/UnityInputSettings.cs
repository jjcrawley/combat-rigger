﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Combo Box/Input Settings", fileName = "New Input Settings")]
public class UnityInputSettings : InputSettings
{
    [SerializeField]
    private InputSetting[] m_inputSettings = new InputSetting[0];

    public InputSetting[] Settings
    {
        get
        {
            return m_inputSettings;
        }
    }

    public override List<string> ButtonNames
    {
        get
        {
            List<string> names = new List<string>();

            for(int i = 0; i < m_inputSettings.Length; i++)
            {
                names.Add(m_inputSettings[i].name);
            }

            return names;
        }
    }
}

[System.Serializable]
public struct InputSetting
{
    public string name;
    public string unityInput;
}