﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputSettings : ScriptableObject, IInputSettings
{
    public abstract List<string> ButtonNames { get; }
}