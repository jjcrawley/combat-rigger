﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public sealed class VisualPin
{
    [NonSerialized]
    private List<VisualEdge> m_edges;

    [SerializeField]
    private string m_pinName;

    [SerializeField]
    private PinType m_type;

    [SerializeField]
    private VisualNode m_ownerNode;
    
    public VisualPin()
    {
        m_edges = new List<VisualEdge>();
    }

    public VisualPin(string name, PinType type)
    {
        m_edges = new List<VisualEdge>();
        m_pinName = name;
        m_type = type;
    }

    public void MakeConnection(VisualPin toPin)
    {
        m_ownerNode.Owner.ConnectPins(this, toPin);
    }

    public void RemoveConnection(VisualPin toPin)
    {
        VisualEdge edge = null;

        for(int i = 0; i < m_edges.Count; i++)
        {
            if(edge.ToPin == toPin && edge.FromPin == this)
            {
                edge = m_edges[i];
                break;
            }
        }

        m_ownerNode.Owner.RemoveConnection(edge);
    }

    public void AddConnection(VisualEdge edge)
    {
        m_edges.Add(edge);
    }    

    public void RemoveConnection(VisualEdge edge)
    {
        m_edges.Remove(edge);
    }

    public void WakeupPin(VisualNode owner)
    {
        if(m_edges == null)
        {
            m_edges = new List<VisualEdge>();
        }

        m_edges.Clear();
        m_ownerNode = owner;
    }

    public string Name
    {
        set
        {
            m_pinName = value;
        }
        get
        {
            return m_pinName;
        }
    }

    public VisualNode Owner
    {
        internal set
        {
            m_ownerNode = value;
        }
        get
        {
            return m_ownerNode;
        }
    }

    public PinType Type
    {
        set
        {
            m_type = value;
        }
        get
        {
            return m_type;
        }
    }

    public List<VisualEdge> Edges
    {
        get
        {
            return m_edges;
        }
    }

    public enum PinType
    {
        Input,
        Output
    }
}