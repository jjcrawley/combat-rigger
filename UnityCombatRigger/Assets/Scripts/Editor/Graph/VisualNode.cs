﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Graphs;
using UnityEditor;
using System.Linq;

public class VisualNode : ScriptableObject
{
    [SerializeField]
    private VisualGraph m_ownerGraph;

    [SerializeField]
    protected Object m_target;

    [SerializeField]
    internal Rect position;

    [SerializeField]
    protected GUIContent m_content = new GUIContent();

    [SerializeField]
    private List<VisualPin> m_pins = new List<VisualPin>();

    [SerializeField]
    private string m_nodeStyle = "node";

    private bool b_dragging;
    private bool b_hasFocus;

    [SerializeField]
    private Styles.Color m_colour;

    protected virtual void OnEnable()
    {
        
    }

    public virtual VisualPin AddPin(string name, VisualPin.PinType type)
    {
        VisualPin pin = new VisualPin(name, type);
        
        m_pins.Add(pin);
        pin.Owner = this;
        
        return pin;        
    }

    public virtual void RemovePin(VisualPin pin)
    {
        if(!m_pins.Contains(pin) && pin == null)
        {
            return;
        }

        m_pins.Remove(pin);

        for(int i = 0; i < pin.Edges.Count; i++)
        {
            m_ownerGraph.RemoveConnection(pin.Edges[i]);            
        }
    }

    public void ClearAllConnections()
    {
        for(int i = 0; i < m_pins.Count; i++)
        {
            ClearAllConnections(m_pins[i]);
        }        
    }

    public virtual void ClearAllConnections(VisualPin pin)
    {
        System.Predicate<VisualEdge> remover = (VisualEdge edge) => { m_ownerGraph.RemoveConnection(edge); return true; };

        pin.Edges.RemoveAll(remover);
    }

    public virtual void OnNodeGUI(VisualGraphGUI gui)
    {
        if (Event.current.type == EventType.MouseUp && Event.current.button == 1)
        {
            GenericMenu menu = new GenericMenu();
            BuildContextMenu(menu, gui);            
            menu.ShowAsContext();
            Event.current.Use();                     
        }
    }

    protected virtual void BuildContextMenu(GenericMenu menu, VisualGraphGUI gui)
    {
        menu.AddItem(new GUIContent("Make link"), false, () =>
        {
            gui.EdgeGUI.BeginDragging(OutPins.First());
        });
    }   
    
    public virtual void OnFocus(VisualGraphGUI gui)
    {
        b_hasFocus = true;
    } 

    public virtual void OnLoseFocus(VisualGraphGUI gui)
    {
        b_hasFocus = false;
    }
        
    public virtual void OnDragBegin()
    {

    }

    public virtual void OnDrag()
    {
        b_dragging = true;
    }

    public virtual void OnEndDrag()
    {
        b_dragging = false;
    }    

    public void Wakeup(VisualGraph graph)
    {
        m_ownerGraph = graph;

        m_pins.ForEach((VisualPin pin) => { pin.WakeupPin(this); });
    }

    public virtual GUIContent GetContent()
    {
        return m_content;
    }

    public virtual Object InspectingObject
    {
        set
        {
            m_target = value;            
        }
        get
        {
            return m_target;
        }
    }    
    
    public VisualGraph Owner
    {
        internal set
        {
            if(m_ownerGraph != null)
            {
                m_ownerGraph.RemoveNode(this);
            }

            m_ownerGraph = value;
        }
        get
        {
            return m_ownerGraph;
        }
    }    

    public bool Dragging
    {
        get
        {
            return b_dragging;
        }
    }

    public bool Focused
    {
        get
        {
            return b_hasFocus;
        }
    }    

    public Rect Position
    {
        set
        {
            position = value;
        }
        get
        {
            return position;
        }
    }

    public VisualPin this[string name]
    {
        get
        {
            for (int i = 0; i < m_pins.Count; i++)
            {
                if (m_pins[i].Name == name)
                {
                    return m_pins[i];
                }
            }

            return null;
        }
    }

    public VisualPin this[int index]
    {
        get
        {
            return m_pins[index];
        }
    }

    public List<VisualPin> Pins
    {
        get
        {
            return m_pins;
        }
    }

    public string Style
    {
        set
        {
            m_nodeStyle = value;
        }
        get
        {
            return m_nodeStyle;
        }
    }

    public Styles.Color Colour
    {
        set
        {
            m_colour = value;
        }
        get
        {
            return m_colour;
        }
    }

    public IEnumerable<VisualPin> OutPins
    {
        get
        {
            return from pin in m_pins
                   where pin.Type == VisualPin.PinType.Output
                   select pin;
        }
    }

    public IEnumerable<VisualPin> InPins
    {
        get
        {
            return from pin in m_pins
                   where pin.Type == VisualPin.PinType.Input
                   select pin;
        }
    }
}