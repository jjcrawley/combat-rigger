﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEditor.Graphs;
using UnityEditor.AnimatedValues;

public class ComboStateNode : BaseCombatNode
{
    private bool b_inputOpen = false;
        
    private ComboLink m_selectedLink;
    private ComboTreeGraph.ComboLinkContext m_linkContext;
    
    protected override void OnEnable()
    {
        base.OnEnable();

        Colour = Styles.Color.Blue;
        m_content.tooltip = "A combo state";
    }
    
    public override void OnNodeGUI(VisualGraphGUI gui)
    {
        base.OnNodeGUI(gui);

        GUILayoutUtility.GetRect(250, 0);

        if (Event.current.LeftClick() && gui.EdgeGUI.Dragging)
        {
            gui.EdgeGUI.EndPinDrag(InPins.First());
            Event.current.Use();
        }

        DoLinkGUI(gui);
    }

    protected virtual void DoLinkGUI(VisualGraphGUI gui)
    {
        ComboLink[] links = m_targetRef.Links;
     
        if(links.Length <= 0)
        {
            return;            
        }

        EditorGUILayout.BeginHorizontal();

        GUILayout.Space(3);

        Rect positionRect = GUILayoutUtility.GetRect(new GUIContent("Input Settings"), EditorStyles.toolbarDropDown);  
              
        b_inputOpen = EditorGUI.Foldout(positionRect, b_inputOpen, "Input Settings", true);

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndHorizontal();

        if (!b_inputOpen)
        {
            ResetLink(gui);            
            return;
        }

        GUILayout.Space(2);

        for (int i = 0; i < links.Length; i++)
        {
            PlayerComboLink link = links[i] as PlayerComboLink;

            float min = link.InputPeriod.start;
            float max = link.InputPeriod.end;

            int frameCount = 60;

            if (link.Origin.Move != null)
            {
                frameCount = link.Origin.Move.FrameCount;
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(link.Destination.ComboName, m_selectedLink != link ? EditorStyles.label : EditorStyles.boldLabel, GUILayout.MaxWidth(100));            

            EditorGUI.BeginChangeCheck();       

            EditorGUILayout.MinMaxSlider(ref min, ref max, 0, frameCount, GUILayout.MinWidth(120));

            if (EditorGUI.EndChangeCheck())
            {                
                Undo.RegisterCompleteObjectUndo(link, "Changed Input Period");
                link.InputPeriod = ValidateInputPeriod(frameCount, min, max);
                InspectorHelper.RepaintInspectors();
            }

            EditorGUILayout.EndHorizontal();
            
            Rect checkRect = GUILayoutUtility.GetLastRect();

            Event current = Event.current;

            if (current.type == EventType.MouseDown && current.button == 0 && Event.current.InRect(checkRect))
            {
                if (m_selectedLink == link)
                {
                    ResetLink(gui);

                    Event.current.Use();
                }
                else
                {

                    ComboTreeGraph graph = gui.Graph as ComboTreeGraph;

                    ResetLink(gui);

                    m_selectedLink = link;

                    m_linkContext = graph.GetContext(m_selectedLink);

                    m_linkContext.previewType = ComboTreeGraph.PreviewType.Preview;

                    gui.EdgeGUI.ClearSelection();
                    gui.EdgeGUI.SelectedEdges.Add(graph[m_selectedLink]);

                    ComboStateInspector.UpdateLinkSelection(link);

                    Event.current.Use();
                }
            }

            if(m_selectedLink == link)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUI.BeginChangeCheck();

                EditorGUILayout.LabelField("Start", GUILayout.MaxWidth(40));

                min = EditorGUILayout.DelayedFloatField(min, GUILayout.MaxWidth(40));

                EditorGUILayout.LabelField("End", GUILayout.MaxWidth(40));

                max = EditorGUILayout.DelayedFloatField(max, GUILayout.MaxWidth(40));

                if(EditorGUI.EndChangeCheck())
                {
                    Undo.RegisterCompleteObjectUndo(link, "Changed Input Period");
                    link.InputPeriod = ValidateInputPeriod(frameCount, min, max);
                    InspectorHelper.RepaintInspectors();
                }

                EditorGUILayout.EndHorizontal();
            }

            GUILayout.Space(2);
        }

        GUILayout.Space(2);
    }    
        
    public override void OnLoseFocus(VisualGraphGUI gui)
    {
        //Debug.Log("Lost focus");
        ResetLink(gui);
        base.OnLoseFocus(gui);
    }

    private Period ValidateInputPeriod(float frameCount, float start, float end)
    {
        Period period;

        start = Mathf.Round(start);
        end = Mathf.Round(end);

        end = Mathf.Clamp(end, 0, frameCount);
        start = Mathf.Clamp(start, 0, frameCount);

        end = Mathf.Max(end, start);

        period.start = start;
        period.end = end;

        return period;
    }

    private void ResetLink(VisualGraphGUI gui)
    {
        if(m_selectedLink != null)
        {
            m_linkContext.previewType = ComboTreeGraph.PreviewType.Default;

            ComboTreeGraph graph = gui.Graph as ComboTreeGraph;

            gui.EdgeGUI.SelectedEdges.Remove(graph[m_selectedLink]);

            m_linkContext = null;
            m_selectedLink = null;
        }
    }   
}