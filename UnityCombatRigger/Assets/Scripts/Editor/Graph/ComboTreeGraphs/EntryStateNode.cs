﻿using UnityEngine;
using UnityEditor.Graphs;
using UnityEditor;
using System.Linq;

public class EntryStateNode : BaseCombatNode
{
    private ComboTree m_tree;
    
    protected override void OnEnable()
    {
        Colour = Styles.Color.Green;
        m_content = new GUIContent("Start", "The starting point of the combo chain");
    }  

    protected override void BuildContextMenu(GenericMenu menu, VisualGraphGUI gui)
    {
        menu.AddItem(new GUIContent("Make link"), false, () => 
        {
            gui.EdgeGUI.BeginDragging(OutPins.First());
        });
    }

    public override void OnEndDrag()
    {
        //base.OnEndDrag();
        Undo.RecordObject(m_tree, "Moved entry state");
        m_tree.entryNodeData.position = position.position;        
    }

    public override void OnConnect(ComboState state, ComboState destination)
    {
        if (destination != null)
        {
            Debug.Log("Added state");
            m_tree.AddEntryLink(destination);
            ComboBoxWindow.OnComboBoxDirty();
        }
    }

    public override GUIContent GetContent()
    {
        return m_content;
    }

    public ComboTree Tree
    {
        set
        {
            m_tree = value;
        }
        get
        {
            return m_tree;
        }
    }    
}