﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEditor.Graphs;

public class ComboTreeGraphGUI : VisualGraphGUI
{  
    private ComboTree m_currentTree;

    [SerializeField]
    private ComboTreeGraph m_treeGraph;

    private List<ComboState> m_lastCreatedStates = new List<ComboState>();

    private ComboTreeEdgeGUI m_treeEdgeGUI;

    protected override void OnEnable()
    {
        if (m_graph == null)
        {
            m_graph = CreateInstance<ComboTreeGraph>();
            m_graph.hideFlags = HideFlags.HideAndDontSave;
        }

        m_edgeGUI = new ComboTreeEdgeGUI();

        m_treeEdgeGUI = m_edgeGUI as ComboTreeEdgeGUI;
        m_edgeGUI.Owner = this;

        m_treeGraph = m_graph as ComboTreeGraph;
        CentreGraph();

        Undo.undoRedoPerformed += OnUndoRedo;         
    }

    private void OnUndoRedo()
    {
        Rebuild();
    }    

    private void OnDisable()
    {
        Undo.undoRedoPerformed -= OnUndoRedo;
        //ComboBoxWindow.onComboBoxDirty -= Rebuild;           
    }

    public void Rebuild()
    {
        m_treeGraph.BuildGraph(m_currentTree);

        if (m_lastCreatedStates.Count > 0)
        {
            for (int i = 0; i < m_lastCreatedStates.Count; i++)
            {
                m_selectedNodes.Add(m_treeGraph[m_lastCreatedStates[i]]);
            }

            m_lastCreatedStates.Clear();
        }
        else
        {
            SyncWithSelection();
        }
    }

    public override void DoGUI()
    {
        DrawNodes();

        HandleContext();
        ProcessKeyBoardEvents();
        ProcessDragAndDrop();
        ProcessDragArea();
        //HandleBackgroundClick();
    }

    protected override void DrawNode(VisualNode node)
    {
        ProcessNodeSelect(node);

        node.OnNodeGUI(this);
        
        ProcessNodeDrag();
    }

    protected override void BuildContextMenu(GenericMenu menu)
    {
        if (!m_edgeGUI.Dragging)
        {
            menu.AddItem(new GUIContent("Add combo state"), false, (AddNewComboState));
        }
        else
        {
            menu.AddItem(new GUIContent("Add combo state"), false, CreateAndLinkState);
        }

        List<MoveLookup.MoveAssetInfo> contexts = MoveLookup.GetInfo();

        if (!m_edgeGUI.Dragging)
        {
            for (int i = 0; i < contexts.Count; i++)
            {
                menu.AddItem(new GUIContent("Create from/" + contexts[i].menuPath), false, AddFromMove, contexts[i].move);
            }
        }
        else
        {
            for (int i = 0; i < contexts.Count; i++)
            {
                menu.AddItem(new GUIContent("Create from/" + contexts[i].menuPath), false, CreateAndLinkMove, contexts[i].move);
            }
        }
    }   

    protected override void PopulateSelection(List<Object> objects)
    {
        for (int i = 0; i < m_selectedNodes.Count; i++)
        {
            objects.Add(m_selectedNodes[i].InspectingObject);
        }

        List<ComboTreeGraph.ComboLinkContext> contexts = m_treeGraph.GetLinks(m_edgeGUI.SelectedEdges);

        for (int i = 0; i < contexts.Count; i++)
        {
            if (contexts[i].previewType != ComboTreeGraph.PreviewType.Preview)
            {
                objects.Add(contexts[i].link);
            }
        }
    }

    private void CreateAndLinkState()
    {
        m_selectedNodes.Clear();

        ComboState state = m_currentTree.AddState();

        state.data.position = m_contextClickPos;

        m_lastCreatedStates.Add(state);
        Undo.RegisterCompleteObjectUndo(state, "Created state");

        BaseCombatNode node = m_edgeGUI.DraggingFromNode as BaseCombatNode;

        m_edgeGUI.EndPinDrag(null);

        if(node != null)
        {
            node.OnConnect((ComboState)node.InspectingObject, state);
        }

        //ComboBoxWindow.OnComboBoxDirty();
    }

    private void AddNewComboState()
    {
        ClearSelection();

        ComboState state = m_currentTree.AddState();
        state.data.position = m_contextClickPos;

        m_lastCreatedStates.Add(state);
        Undo.RegisterCompleteObjectUndo(state, "Created state");

        ComboBoxWindow.OnComboBoxDirty();          
    }

    private void AddFromMove(object move)
    {
        ClearSelection();

        CombatMove theMove = move as CombatMove;

        ComboState state = m_currentTree.AddMove(theMove);
        state.data.position = m_contextClickPos;

        m_lastCreatedStates.Add(state);
        Undo.RegisterCompleteObjectUndo(state, "Created state");

        ComboBoxWindow.OnComboBoxDirty();
    }

    private void CreateAndLinkMove(object move)
    {
        ClearSelection();

        CombatMove theMove = move as CombatMove;

        ComboState state = m_currentTree.AddMove(theMove);
        state.data.position = m_contextClickPos;

        m_lastCreatedStates.Add(state);
        Undo.RegisterCompleteObjectUndo(state, "Created state");

        BaseCombatNode node = m_edgeGUI.DraggingFromNode as BaseCombatNode;

        m_edgeGUI.EndPinDrag(null);

        if(node != null)
        {
            node.OnConnect((ComboState)node.InspectingObject, state);
        }

        //ComboBoxWindow.OnComboBoxDirty();        
    }

    protected override void DeleteSelection()
    {
        m_treeGraph.RemoveNodes(m_selectedNodes);
        
        m_treeEdgeGUI.DeleteSelection();

        ClearSelection();

        ComboBoxWindow.OnComboBoxDirty();

        HandleUtility.Repaint();
    }

    public override void SyncWithSelection()
    {
        if (GUIUtility.hotControl == 0)
        {
            m_selectedNodes.Clear();
            m_edgeGUI.SelectedEdges.Clear();

            Object[] selection = Selection.objects;

            for (int i = 0; i < selection.Length; i++)
            {
                ComboState state = selection[i] as ComboState;

                ComboLink link = selection[i] as ComboLink;
                                
                if (state != null)
                {
                    VisualNode node = m_treeGraph[state];
                    m_selectedNodes.Add(node);
                }
                else if (link != null)
                {
                    VisualEdge edge = m_treeGraph.GetEdge(link);

                    if (edge != null)
                    {
                        m_edgeGUI.SelectedEdges.Add(edge);
                    }
                }
            }
        }
    }

    private void ProcessDragAndDrop()
    {
        Event current = Event.current;
        EventType type = current.type;               

        CombatMove[] moves = DragAndDrop.objectReferences.OfType<CombatMove>().ToArray();

        switch (type)
        {
            case EventType.DragExited:
                //Debug.Log("Drag exited");
                break;
            case EventType.DragUpdated:
                if(moves.Length > 0)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
                }
                else
                {                    
                    DragAndDrop.visualMode = DragAndDropVisualMode.None;
                }

                current.Use();

                break;
            case EventType.DragPerform:
                
                if(moves.Length > 0)
                {
                    DragAndDrop.AcceptDrag();
                    
                    for(int i = 0; i < moves.Length; i++)
                    {
                        ComboState state = m_currentTree.AddMove(moves[i]);
                        state.data.position = current.mousePosition + new Vector2(12f, 30f) * i;

                        m_lastCreatedStates.Add(state);
                    }                    

                    ComboBoxWindow.OnComboBoxDirty();
                }
                current.Use();

                break;
        }
    }

    public override void DoBackgroundClickAction()
    {       
        if (m_selectedNodes.Count == 0 && m_edgeGUI.SelectedEdges.Count == 0)
        {             
            Selection.objects = new List<Object> { m_currentTree }.ToArray();
        }
    }

    public override void ClearSelection()
    {
        base.ClearSelection();
        UpdateUnitySelection();
    }

    public ComboTree Tree
    {
        set
        {
            m_currentTree = value;

            Rebuild();
        }
        get
        {
            return m_currentTree;            
        }
    }
}