﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Graphs;
using System.Linq;
using UnityEditor;

public class ComboTreeGraph : VisualGraph
{
    private ComboTree m_tree;

    private Dictionary<VisualNode, ComboState> m_stateLookup = new Dictionary<VisualNode, ComboState>();
    private Dictionary<ComboState, VisualNode> m_stateNodeLookup = new Dictionary<ComboState, VisualNode>();
    private Dictionary<VisualEdge, ComboLinkContext> m_linkLookup = new Dictionary<VisualEdge, ComboLinkContext>();
    private Dictionary<ComboLink, VisualEdge> m_edgeLookup = new Dictionary<ComboLink, VisualEdge>();

    private EntryStateNode m_entryNode;

    public void BuildGraph(ComboTree tree)
    {
        ClearGraph();
        
        if (tree != null)
        {
            m_tree = tree;           

            BuildStateLookup(tree);
            SetupAllEdges();
        }        
    }

    private void SetupAllEdges()
    {
        m_linkLookup.Clear();
        m_edgeLookup.Clear();

        for (int i = 0; i < m_tree.States.Length; i++)
        {
            SetupEdges(m_tree[i]);            
        }

        SetupStartEdges();
    }

    private void SetupEdges(ComboState state)
    {
        VisualNode nodeOne = m_stateNodeLookup[state];

        ComboLink[] links = state.Links;

        for (int i = 0; i < links.Length; i++)
        {
            ComboLink link = links[i];

            VisualNode nodeTwo;

            if (m_stateNodeLookup.ContainsKey(link.Destination))
            {
                nodeTwo = m_stateNodeLookup[link.Destination];
            }
            else
            {
                Debug.Log(link.Destination.ComboName);
                continue;
            }
                
            VisualEdge edge = ConnectPins(nodeOne.OutPins.First(), nodeTwo.InPins.First());

            ComboLinkContext context = new ComboLinkContext(link, m_tree);
            context.type = LinkType.Player;

            m_linkLookup.Add(edge, context);
            m_edgeLookup.Add(link, edge);
        }             
    }    

    private void SetupStartEdges()
    {
        for (int i = 0; i < m_tree.EntryLinks.Length; i++)
        {
            ComboLink link = m_tree.EntryLinks[i];

            VisualNode destination = m_stateNodeLookup[link.Destination];

            VisualEdge edge = ConnectPins(m_entryNode.OutPins.First(), destination.InPins.First());

            ComboLinkContext context = new ComboLinkContext(link, m_tree);
            context.type = LinkType.Entry;

            if (edge != null)
            {
                m_linkLookup.Add(edge, context);
            }
        }
    }

    private void BuildStateLookup(ComboTree tree)
    {
        m_stateLookup.Clear();
        m_stateNodeLookup.Clear();

        for (int i = 0; i < tree.States.Length; i++)
        {
            ComboState state = tree.States[i];

            VisualNode node = BuildState(state);            

            if(node == null)
            {
                Debug.Log("It's null?");
                continue;
            }
            
            m_stateLookup.Add(node, state);
            m_stateNodeLookup.Add(state, node);
        }

        m_entryNode = BuildStartState();
    }   

    private EntryStateNode BuildStartState()
    {
        EntryStateNode node = AddNode<EntryStateNode>();

        node.hideFlags = HideFlags.HideAndDontSave;
        node.position.position = m_tree.entryNodeData.position;
        node.AddPin("In", VisualPin.PinType.Input);
        node.AddPin("Out", VisualPin.PinType.Output);
        node.Style = "node";
        node.Tree = m_tree;

        EntryState state = m_tree.Start as EntryState;        

        node.InspectingObject = state;
        node.name = "Start";
        state.ComboName = "Start";

        return node;
    }

    private VisualNode BuildState(ComboState state)
    {
        if(state == null)
        {
            Debug.Log("A combo state was null...");
            return null;
        }

        VisualNode node = AddNode<ComboStateNode>();

        node.hideFlags = HideFlags.HideAndDontSave;
        node.position.position = state.data.position;
        node.Style = "node";
        //node.Colour = Styles.Color.Blue;
        node.InspectingObject = state;
        node.AddPin("In", VisualPin.PinType.Input);
        node.AddPin("Out", VisualPin.PinType.Output);

        return node;                                      
    }

    public override void RemoveNodes(List<VisualNode> nodes)
    {
        foreach(VisualNode node in nodes)
        {
            ComboState state = node.InspectingObject as ComboState;

            if (state != null && state != m_entryNode.InspectingObject)
            {
                int index = ArrayUtility.IndexOf(m_tree.States, state);

                if (index != -1)
                {
                    m_tree.RemoveState(index);
                }
            }
        }        
    }

    public List<ComboLinkContext> GetLinks(List<VisualEdge> edges)
    {
        List<ComboLinkContext> links = new List<ComboLinkContext>();

        for (int i = 0; i < edges.Count; i++)
        {
            links.Add(m_linkLookup[edges[i]]);
        }

        return links;
    }  
    
    public ComboLinkContext GetContext(ComboLink link)
    {
        return m_linkLookup.Values.First((ComboLinkContext context) => { return context.link == link; });
    }
            
    public VisualEdge GetEdge(ComboLink link)
    {
        for(int i = 0; i < m_edges.Count; i++)
        {
            ComboLinkContext current = m_linkLookup[m_edges[i]];
            
            if(current.link == link)
            {
                return m_edges[i];
            }
        }

        return null;
    }  

    public VisualNode GetNode(ComboState state)
    {
        if(m_stateNodeLookup.ContainsKey(state))
        {
            return m_stateNodeLookup[state];
        }
        else
        {
            return m_entryNode;
        }
    }

    public VisualNode this[ComboState state]
    {
        get
        {
            return GetNode(state);
        }
    }

    public VisualEdge this[ComboLink link]
    {
        get
        {
            return m_edgeLookup[link];
        }
    }

    public ComboLinkContext this[VisualEdge edge]
    {
        get
        {
            return m_linkLookup[edge];
        }
    }

    public ComboTree Tree
    {       
        get
        {
            return m_tree;
        }
    }

    public class ComboLinkContext
    {
        public readonly ComboLink link;

        public readonly ComboTree tree;
                
        public LinkType type;

        public PreviewType previewType = PreviewType.Default;                  
        
        public ComboLinkContext(ComboLink link, ComboTree tree)
        {
            this.link = link;
            this.tree = tree;           
        }     
    }

    public enum LinkType
    {
        Player,
        Entry
    }

    public enum PreviewType
    {
        Preview,
        Default
    }
}