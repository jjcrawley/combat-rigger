﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class BaseCombatNode : VisualNode
{
    protected ComboState m_targetRef;

    public override void OnNodeGUI(VisualGraphGUI gui)
    {
        base.OnNodeGUI(gui);

        GUILayoutUtility.GetRect(200, 0);        

        if (Event.current.LeftClick() && gui.EdgeGUI.Dragging)
        {
            gui.EdgeGUI.EndPinDrag(InPins.First());
            Event.current.Use();
        }
    }    

    protected override void BuildContextMenu(GenericMenu menu, VisualGraphGUI gui)
    {
        menu.AddItem(new GUIContent("Make link"), false, () =>
        {
            gui.EdgeGUI.BeginDragging(OutPins.First());
        });
    }

    public virtual void OnConnect(ComboState state, ComboState destination)
    {
        if (state != null && destination as EntryState == null)
        {
            state.AddPlayerLink(destination);
            ComboBoxWindow.OnComboBoxDirty();
        }
    }

    public override void OnEndDrag()
    {
        base.OnEndDrag();

        if (m_targetRef != null)
        {
            Undo.RecordObject(m_targetRef, "Move Combo state");
            m_targetRef.data.position = position.position;
        }
    }

    public override GUIContent GetContent()
    {
        m_content.text = m_targetRef.ComboName;

        return base.GetContent();
    }

    public override Object InspectingObject
    {
        set
        {
            base.InspectingObject = value;
            m_targetRef = value as ComboState;
        }
        get
        {
            return base.InspectingObject;
        }
    }
}