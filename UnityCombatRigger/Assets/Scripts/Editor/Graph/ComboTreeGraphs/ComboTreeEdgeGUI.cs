﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Graphs;

public class ComboTreeEdgeGUI : VisualEdgeGUI
{
    protected override void DrawEdge(VisualEdge edge)
    {
        Vector2 start = edge.FromPin.Owner.Position.center;
        Vector2 end = edge.ToPin.Owner.Position.center;

        bool inSelection = m_selectedEdges.Contains(edge);

        Color lineColor = !inSelection ? Color.white : Color.cyan;

        Handles.color = lineColor;

        //GetStartAndEndRect(edge, out start, out end);
        
        GetStartAndEnd(edge, out start, out end);

        edge.endPosition = end;
        edge.startPosition = start;

        Handles.DrawAAPolyLine((Texture2D)Styles.connectionTexture.image, 10f, start, end);               

        //Handles.DrawLine(start, end);

        DrawArrow(start, end);        
    }

    private void GetStartAndEnd(VisualEdge edge, out Vector2 start, out Vector2 end)
    {
        Rect startRect = edge.FromPin.Owner.position;
        Rect endRect = edge.ToPin.Owner.position;

        Vector3 cross = Vector3.Cross(endRect.center - startRect.center, Vector3.back);
        cross.Normalize();

        startRect.center += (Vector2)cross * 8.0f;
        endRect.center += (Vector2)cross * 8.0f;
                        
        end = GetPointOnRect(endRect, startRect.center, endRect.center);
        start = GetPointOnRect(startRect, endRect.center, startRect.center);                
    }

    private Vector2 GetPointOnRect(Rect position, Vector2 start, Vector2 end)
    {
        //get gradient
        float slope = (end.y - start.y) / (end.x - start.x);

        position.width -= 9.0f;
        position.xMin += 9.0f;

        position.yMin += 9.0f;
        position.height -= 9.0f;

        //check which edge the line intersects
        if (-position.height / 2 <= slope * position.width / 2 && slope * position.width / 2 <= position.height / 2)
        {
            //intersects left edge
            if (end.x > start.x)
            {
                end.x = position.xMin;
                end.y = end.y - slope * position.width / 2;
                return end;
            }
            //intersects right edge
            else if (end.x < start.x)
            {
                end.x = position.xMax;
                end.y = end.y + slope * position.width / 2;
                return end;
            }
        }

        //check the top and bottom edges this time
        if (-position.width / 2 <= position.height / 2 / slope && position.height / 2 / slope <= position.width / 2)
        {
            //intersects top edge
            if (end.y > start.y)
            {                
                end.y = position.yMin;
                end.x = end.x - position.height / 2 / slope;
                return end;
            }
            //intersects bottom edge
            else if (end.y < start.y)
            {
                end.y = position.yMax;
                end.x = end.x + position.height / 2 / slope;
                return end;
            }
        }

        return end;
    }

    private void GetStartAndEndRect(VisualEdge edge, out Vector2 start, out Vector2 end)
    {
        Rect startPos = edge.FromPin.Owner.position;
        Rect endPos = edge.ToPin.Owner.position;

        start = startPos.center;
        end = endPos.center;

        start = GetIntersection(startPos, end);
        end = GetIntersection(endPos, start);          
    }

    private Vector2 GetIntersection(Rect position, Vector2 point)
    {
        if (point.x > position.xMax)
        {
            point.x = position.xMax;
        }
        else if(point.x < position.xMin)
        {
            point.x = position.xMin;
        }
        
        if(point.y < position.y)
        {
            point.y = position.yMin;           
        }
        else if(point.y > position.yMax)
        {
            point.y = position.yMax;
        }

        return point;
    }

    private void DrawArrow(VisualEdge edge)
    {
        DrawArrow(edge.FromPin.Owner.Position.center, edge.ToPin.Owner.Position.center);
    }

    private void DrawArrow(Vector2 start, Vector2 end)
    {        
        Vector2 lineAngle = (end - start).normalized;

        Vector3 crossAngle = Vector3.Cross(end - start, Vector3.back);

        crossAngle.Normalize();

        Vector2 topPoint = Vector2.Lerp(start, end, 0.50f) + lineAngle * 7.5f - (Vector2)crossAngle * 1.05f;
        Vector2 basePoint = topPoint - lineAngle * 15.0f;        

        Vector2 leftCorner = basePoint + (Vector2)crossAngle * 7.0f;
        Vector2 rightCorner = basePoint - (Vector2)crossAngle * 7.0f;

        Handles.DrawAAConvexPolygon(leftCorner, topPoint, rightCorner);        
    }

    private void DrawButtonName(VisualEdge edge)
    {
        ComboTreeGraph graph = Owner.Graph as ComboTreeGraph;

        ComboTreeGraph.ComboLinkContext context = graph[edge];

        BasePlayerLink link = context.link as BasePlayerLink;

        Vector2 start = edge.startPosition;
        Vector2 end = edge.endPosition;

        //GetStartAndEnd(edge, out start, out end);

        if (link != null)
        {            
            DrawButtonName(start, end, link.ButtonName);
        }
        else
        {
            ButtonLock[] theLock = context.link.GetLocksOfType<ButtonLock>();

            if(theLock.Length > 0)
            {
                DrawButtonName(start, end, theLock[0].ButtonName);
            }
        }
    }

    private void DrawButtonName(Vector2 start, Vector2 end, string buttonName)
    {        
        Vector2 centrePoint = Vector2.Lerp(start, end, 0.5f);

        Vector3 angle = Vector3.Cross(end - start, Vector3.back);

        angle.Normalize();

        Vector2 dimensions = EditorStyles.textField.CalcSize(new GUIContent(buttonName));

        Vector2 position = centrePoint;
        
        //position -= (Vector2)angle * width;       
       
        Rect positionLabel = new Rect(0, 0, dimensions.x, dimensions.y + 2);

        positionLabel.position = position;

        if (!string.IsNullOrEmpty(buttonName))
        {
            EditorGUI.HelpBox(positionLabel, buttonName, MessageType.None);                        
        }
    }

    public override void DrawOverlayGUI()
    {
        if (Event.current.type != EventType.Repaint)
        {
            return;
        }       

        for(int i = 0; i < m_selectedEdges.Count; i++)
        {
            DrawButtonName(m_selectedEdges[i]);
        }
    }

    public override void EndPinDrag(VisualPin endPin)
    {
        if (endPin != null)
        {
            BaseCombatNode node = m_startingPin.Owner as BaseCombatNode;
            BaseCombatNode nodeDest = endPin.Owner as BaseCombatNode;
            
            ComboState start = node.InspectingObject as ComboState;
            ComboState end = nodeDest.InspectingObject as ComboState;
            
            if(end is EntryState || start is EntryState)
            {
                if(start is EntryState)
                {                    
                    node.OnConnect(start, end);
                }
                else
                {
                    nodeDest.OnConnect(end, start);
                }
            }
            else
            {
                node.OnConnect(start, end);               
            }
        }

        m_startingPin = null;
        Owner.Host.wantsMouseMove = false;
    }     

    public override void DeleteSelection()
    {
        ComboTreeGraph graph = Owner.Graph as ComboTreeGraph;

        List<ComboTreeGraph.ComboLinkContext> contexts = graph.GetLinks(m_selectedEdges);

        //Debug.Log(contexts.Count);

        for (int i = 0; i < contexts.Count; i++)
        {
            ComboTreeGraph.ComboLinkContext context = contexts[i];

            ComboLink link = contexts[i].link;

            if (link != null && link.Origin != null)
            {
                if (context.type == ComboTreeGraph.LinkType.Player)
                {
                    int index = ArrayUtility.IndexOf(link.Origin.Links, link);

                    contexts[i].link.Origin.RemoveLink(index);
                }
                else if(context.type == ComboTreeGraph.LinkType.Entry)
                {
                    int index = ArrayUtility.IndexOf(context.tree.EntryLinks, link);

                    context.tree.RemoveEntryLink(index);
                }
            }
        }

        ClearSelection();
        ComboBoxWindow.OnComboBoxDirty();
    }   
}