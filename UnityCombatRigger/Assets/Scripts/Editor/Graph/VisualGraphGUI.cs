﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Graphs;
using System.Linq;

public class VisualGraphGUI : ScriptableObject
{
    protected readonly GridSettings m_settings = new GridSettings();

    [SerializeField]
    protected VisualGraph m_graph;

    protected List<VisualNode> m_selectedNodes;
    private List<Rect> m_selectedPositions;
    private Vector2 m_nodeDragDistance;

    protected Rect m_graphArea;
    private Rect m_previousGraphArea;
   
    protected Rect m_drawArea;

    protected EditorWindow m_hostWindow;

    private Rect m_selectionArea;
    private Vector2 m_dragStartPos;
    private bool b_draggingSelection;

    protected Vector2 m_contextClickPos;
    private Vector2 m_scrollPosition;

    private bool b_centreGraph;

    protected VisualEdgeGUI m_edgeGUI;
        
    public VisualGraphGUI()
    {
        m_graphArea = new Rect(-800, -20, 1000, 2000);

        m_scrollPosition = new Vector2(0, 0);
        
        m_selectedNodes = new List<VisualNode>();

        m_selectedPositions = new List<Rect>();
        
        if(m_edgeGUI == null)
        {
            m_edgeGUI = new VisualEdgeGUI();
            m_edgeGUI.Owner = this;
        }

        b_centreGraph = false;
    }

    protected virtual void OnEnable()
    {
        if (m_graph == null)
        {
            m_graph = CreateInstance<VisualGraph>();             
        }
    }   

    public void BeginDraw(EditorWindow host, Rect drawArea)
    {
        m_hostWindow = host;
        m_drawArea = drawArea;

        if(Event.current.type == EventType.repaint)
        {
            Styles.graphBackground.Draw(m_drawArea, false, false, false, false);            
        }

        ClampScrollPos();

        Vector2 scrollPos = new Vector2(Mathf.Round(-m_scrollPosition.x - m_graphArea.x), Mathf.Round(-m_scrollPosition.y - m_graphArea.y));

        GUI.BeginClip(m_drawArea, scrollPos, Vector2.zero, false);
                
        DrawGrid();
    }

    private void ClampScrollPos()
    {        
        if (Event.current.type != EventType.Layout)
        {
            m_scrollPosition.x = Mathf.Clamp(m_scrollPosition.x, 0f, Mathf.Max(m_graphArea.width - m_drawArea.width, 0));
            m_scrollPosition.y = Mathf.Clamp(m_scrollPosition.y, 0f, Mathf.Max(m_graphArea.height - m_drawArea.height, 0));
        }
    }

    private void DrawGrid()
    {
        if(Event.current.type == EventType.Repaint)
        {
            DrawGridLines(m_settings.minorLine, m_settings.minorLineColour);
            DrawGridLines(m_settings.majorLine, m_settings.majorLineColour);
        }
    }

    private void DrawGridLines(float gridSize, Color lineColour)
    {
        Handles.color = lineColour;

        float xStart = m_graphArea.xMin - m_graphArea.xMin % gridSize;
        float yStart = m_graphArea.yMin - m_graphArea.yMin % gridSize;

        for(float lineX = xStart; lineX < m_graphArea.xMax; lineX += gridSize)
        {
            Vector2 start = new Vector2(lineX, m_graphArea.yMin);
            Vector2 end = new Vector2(lineX, m_graphArea.yMax);

            Handles.DrawLine(start, end);
        }

        for(float lineY = yStart; lineY < m_graphArea.yMax; lineY += gridSize)
        {
            Vector2 start = new Vector2(m_graphArea.xMin, lineY);
            Vector2 end = new Vector2(m_graphArea.xMax, lineY);

            Handles.DrawLine(start, end);
        }
    }

    public void CentreGraph()
    {
        b_centreGraph = true;
    }
        
    public virtual void DoGUI()
    {
        DrawNodes();
        
        HandleContext();
        ProcessKeyBoardEvents();
        ProcessDragArea();       
    }

    protected virtual void DrawNodes()
    {
        m_hostWindow.BeginWindows();

        IEnumerator nodeEnumerator = m_graph.Nodes.GetEnumerator();

        while (nodeEnumerator.MoveNext())
        {
            VisualNode current = (VisualNode)nodeEnumerator.Current;

            bool selected = m_selectedNodes.Contains(current);

            current.Position = GUILayout.Window(current.GetInstanceID(), current.Position,
                (int id) => { DrawNode(current); }, current.GetContent(),
                Styles.GetNodeStyle(current.Style, current.Colour, selected),
                GUILayout.Width(0),
                GUILayout.Height(0));
        }

        m_edgeGUI.OnEdgeGUI();

        m_hostWindow.EndWindows();

        m_edgeGUI.DrawOverlayGUI();      
    }

    protected void HandleContext()
    {
        Event current = Event.current;

        switch (current.type)
        {
            case EventType.ContextClick:
                m_contextClickPos = current.mousePosition;
                GenericMenu menu = new GenericMenu();
                BuildContextMenu(menu);
                menu.ShowAsContext();
                current.Use();
                break;
            default:
                break;
        }
    }

    protected virtual void BuildContextMenu(GenericMenu menu)
    {
        menu.AddItem(new GUIContent("Add node"), false, () => 
        {
            VisualNode node = m_graph.AddNode<VisualNode>();

            node.position.position = m_contextClickPos;
        });
    }

    protected virtual void DrawNode(VisualNode node)
    {
        ProcessNodeSelect(node);

        node.OnNodeGUI(this);

        if (GUILayout.Button("Make a link"))
        {
            if (!m_edgeGUI.Dragging)
            {
                m_edgeGUI.BeginDragging(node.OutPins.First());
            }
            else
            {
                m_edgeGUI.EndPinDrag(node.InPins.First());
            }
        }

        ProcessNodeDrag();          
    }

    protected virtual void ProcessKeyBoardEvents()
    {
        Event current = Event.current;

        switch (current.type)
        {
            case EventType.KeyDown:
                if(current.keyCode == KeyCode.Delete)
                {
                    DeleteSelection();
                }
                break;
        }
    }

    protected void ProcessNodeSelect(VisualNode node)
    {
        Event current = Event.current;

        switch (current.type)
        {
            case EventType.MouseDown:
                if (current.button == 0 || current.button == 1 && current.clickCount == 1)
                {
                    bool contained = m_selectedNodes.Contains(node);

                    if (current.shift)
                    {
                        if (contained)
                        {
                            m_selectedNodes.Remove(node);                           
                            node.OnLoseFocus(this);
                        }
                        else
                        {
                            m_selectedNodes.Add(node);
                            node.OnFocus(this);
                        }

                        current.Use();
                    }
                    else
                    {
                        if (!contained)
                        {
                            ClearSelection();
                            m_selectedNodes.Add(node);
                            
                            node.OnFocus(this);
                        }
                        
                        HandleUtility.Repaint();
                    }
                                      
                    UpdateUnitySelection();
                                        
                    GUIUtility.keyboardControl = 0;
                }               
                break;           
            default:
                break;
        }
    }

    protected virtual void DeleteSelection()
    {
        m_graph.RemoveNodes(m_selectedNodes);

        m_selectedNodes.Clear();

        m_edgeGUI.DeleteSelection();
        //m_edgeGUI.ClearSelection();

        HandleUtility.Repaint();
    }

    public virtual void ClearSelection()
    {
        for(int i = 0; i < m_selectedNodes.Count; i++)
        {
            m_selectedNodes[i].OnLoseFocus(this);
        }

        m_selectedNodes.Clear();
        m_edgeGUI.ClearSelection();       
    }

    protected void ProcessNodeDrag()
    {
        Event current = Event.current;
        int id = GUIUtility.GetControlID(FocusType.Passive);

        switch(current.GetTypeForControl(id))
        {
            case EventType.MouseDown:
                if(current.button == 0)
                {
                    GUIUtility.hotControl = id;

                    m_selectedPositions.Clear();
                    m_nodeDragDistance = new Vector2();

                    foreach(VisualNode node in m_selectedNodes)
                    {
                        m_selectedPositions.Add(node.position);
                                               
                        node.OnDragBegin();
                    }

                    current.Use();
                }                
                break;
            case EventType.MouseDrag:
                if(GUIUtility.hotControl == id)
                {
                    if(m_settings.snap)
                    {
                        m_nodeDragDistance += current.delta;

                        for (int i = 0; i < m_selectedNodes.Count; i++)
                        {
                            VisualNode node = m_selectedNodes[i];

                            Rect position = m_selectedPositions[i];
                            position.position += m_nodeDragDistance;

                            node.position = SnapPosition(position);
                            node.OnDrag();
                        }
                    }
                    else
                    {
                        foreach(VisualNode node in m_selectedNodes)
                        {
                            node.position.position += current.delta;
                            node.OnDrag();
                        }                        
                    }
                   
                    current.Use();
                }
                break;
            case EventType.MouseUp:
                if(GUIUtility.hotControl == id)
                {
                    GUIUtility.hotControl = 0;                   
                    
                    foreach (VisualNode node in m_selectedNodes)
                    {
                        node.OnEndDrag();
                    }

                    current.Use();
                }
                break;
            default:
                break;
        }
    }

    public void EndDraw()
    {        
        CalculateGraphExtents();
        RecalculateScrollPos();
        ProcessGraphDrag();

        GUI.EndClip();
    } 

    protected void ProcessGraphDrag()
    {        
        Event current = Event.current;

        if(current.button != 2)
        {
            return;
        }

        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        switch (current.GetTypeForControl(controlID))
        {
            case EventType.MouseDown:
                GUIUtility.hotControl = controlID;
                EditorGUIUtility.SetWantsMouseJumping(1);
                current.Use();
                break;
            case EventType.MouseUp:
                if(GUIUtility.hotControl == controlID)
                {
                    GUIUtility.hotControl = 0;
                    EditorGUIUtility.SetWantsMouseJumping(0);
                    current.Use();
                }
                break;            
            case EventType.MouseDrag:
                if(GUIUtility.hotControl == controlID)
                {
                    m_scrollPosition -= current.delta;                                   
                    current.Use();
                }
                break;            
            default:
                break;
        }        
    }  
    
    protected void ProcessDragArea()
    {
        int id = GUIUtility.GetControlID(FocusType.Passive);

        Event current = Event.current;

        switch (current.GetTypeForControl(id))
        {
            case EventType.MouseDown:
                if (current.button == 0 && current.clickCount != 2)
                {
                    GUIUtility.hotControl = id;
                    m_dragStartPos = current.mousePosition;
                                          
                    m_edgeGUI.EndPinDrag(null);

                    VisualEdge edge = m_edgeGUI.GetNearestEdge();

                    if(edge != null)
                    {
                        if(!current.shift)
                        {
                            ClearSelection();
                        }

                        if (!m_edgeGUI.SelectedEdges.Contains(edge))
                        {
                            m_edgeGUI.SelectedEdges.Add(edge);
                        }
                        else
                        {
                            m_edgeGUI.SelectedEdges.Remove(edge);
                        }
                                      
                        current.Use();
                    }                                      
                }
                break;
            case EventType.MouseDrag:
                if(GUIUtility.hotControl == id)
                {
                    GUIUtility.hotControl = id;
                    GUIUtility.keyboardControl = 0;

                    Vector2 currentPos = current.mousePosition;

                    m_selectionArea = EditorUtilities.FromToRect(m_dragStartPos, currentPos);

                    if (!Event.current.shift && !b_draggingSelection)
                    {
                        ClearSelection();
                    }

                    SelectNodesInArea(m_selectionArea);

                    m_edgeGUI.SelectEdges(m_selectedNodes);

                    b_draggingSelection = true;
                    current.Use();
                }
                break;
            case EventType.MouseUp:
                if(GUIUtility.hotControl == id)
                {
                    GUIUtility.hotControl = 0;
                    b_draggingSelection = false;
                                 
                    foreach(VisualNode node in m_selectedNodes)
                    {
                        node.OnFocus(this);
                    }

                    UpdateUnitySelection();                    

                    current.Use();
                }
                break;
            case EventType.Repaint:
                if(b_draggingSelection)
                {
                    Styles.selectionRect.Draw(m_selectionArea, false, false, false, false);
                }
                break;
            default:
                break;
        }
    }

    protected void SelectNodesInArea(Rect dragRect)
    {
        m_selectedNodes.Clear();

        for(int i = 0; i < m_graph.Nodes.Count; i++)
        {
            VisualNode node = m_graph.Nodes[i];

            if(dragRect.Overlaps(node.position))
            {
                m_selectedNodes.Add(node);                          
            }
        }
    }

    protected void HandleBackgroundClick()
    {
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && Event.current.clickCount == 1)
        {
            ClearSelection();
            DoBackgroundClickAction();
        }
    }

    public virtual void DoBackgroundClickAction()
    {
                
    }

    public virtual void SyncWithSelection()
    {

    }

    public void UpdateUnitySelection()
    {
        List<Object> objects = new List<Object>();

        PopulateSelection(objects); 
               
        if (objects.Count > 0)
        {
            Selection.objects = objects.ToArray();
        }
    }

    protected virtual void PopulateSelection(List<Object> objects)
    {
        for (int i = 0; i < m_selectedNodes.Count; i++)
        {
            objects.Add(m_selectedNodes[i].InspectingObject);
        }
    }

    protected Rect SnapPosition(Rect position)
    {
        int posX = Mathf.RoundToInt(position.x / m_settings.snapInterval);
        int posY = Mathf.RoundToInt(position.y / m_settings.snapInterval);

        position.x = posX * m_settings.snapInterval;
        position.y = posY * m_settings.snapInterval;

        return position;                
    }

    private void CalculateGraphExtents()
    {
        m_graphArea = GetExtents();        

        m_graphArea.xMax += m_settings.paddingMultiplier * m_hostWindow.position.width;
        m_graphArea.xMin -= m_settings.paddingMultiplier * m_hostWindow.position.width;
        m_graphArea.yMax += m_settings.paddingMultiplier * m_hostWindow.position.height;
        m_graphArea.yMin -= m_settings.paddingMultiplier * m_hostWindow.position.height;               
    }   

    private void RecalculateScrollPos()
    {
        m_scrollPosition.x += m_previousGraphArea.xMin - m_graphArea.xMin;
        m_scrollPosition.y += m_previousGraphArea.yMin - m_graphArea.yMin;

        m_previousGraphArea = m_graphArea;

        DoCentreGraph();
    }

    private void DoCentreGraph()
    {
        if (b_centreGraph && Event.current.type == EventType.Layout)
        {
            //Rect graphArea = m_graphArea;

            //graphArea.position += m_scrollPosition;

            //Rect extents = GetExtents();

            //float delta = extents.width - m_graphArea.width;

            //if(delta > 0)
            //{
            //    graphArea.width -= delta;

            //    graphArea.x += delta * 0.5f;
            //}

            //delta = extents.height - m_graphArea.height;

            //if(delta > 0)
            //{
            //    graphArea.height -= delta;

            //    graphArea.y += delta * 0.5f;
            //}

            //Vector2 scrollPos = Vector2.zero;

            //if (graphArea.xMax > extents.xMax)
            //{
            //    scrollPos.x = extents.xMax - graphArea.xMax;
            //}
            //else if (graphArea.xMin < extents.xMin)
            //{
            //    scrollPos.x -= graphArea.xMin - extents.xMin;
            //}

            //if (graphArea.yMax > extents.yMax)
            //{
            //    scrollPos.y = extents.yMax - graphArea.yMax;
            //}
            //else if (graphArea.yMin < extents.yMin)
            //{
            //    scrollPos.y -= graphArea.yMin - extents.yMin;
            //}

            //Rect drawArea = m_drawArea;

            //drawArea.width = Mathf.Max(drawArea.width, graphArea.width);
            //drawArea.height = Mathf.Max(drawArea.height, graphArea.height);

            //scrollPos.x = Mathf.Clamp(scrollPos.x, drawArea.xMin - scrollPos.x, drawArea.xMax - m_graphArea.width - m_scrollPosition.x);
            //scrollPos.y = Mathf.Clamp(scrollPos.y, drawArea.yMin - scrollPos.y, drawArea.yMax - m_graphArea.height - m_scrollPosition.y);

            //m_scrollPosition = scrollPos;

            m_scrollPosition.x = m_graphArea.width / 2 - m_hostWindow.position.width / 2;
            m_scrollPosition.y = m_graphArea.height / 2 - m_hostWindow.position.height / 2;

            b_centreGraph = false;
        }
    }

    private Rect GetExtents()
    {
        Rect extents = new Rect();

        if (m_graph != null)
        {
            List<VisualNode>.Enumerator nodes = m_graph.Nodes.GetEnumerator();

            while (nodes.MoveNext())
            {
                Rect position = nodes.Current.position;

                extents.xMin = Mathf.Min(position.xMin, extents.xMin);
                extents.xMax = Mathf.Max(position.xMax, extents.xMax);
                extents.yMax = Mathf.Max(position.yMax, extents.yMax);
                extents.yMin = Mathf.Min(position.yMin, extents.yMin);
            }
        }

        return extents;
    }
    
    public List<VisualNode> SelectedNodes
    {
        get
        {
            return m_selectedNodes;
        }
    }

    public VisualGraph Graph
    {
        set
        {
            m_graph = value;
        }
        get
        {
            return m_graph;
        }
    }

    public VisualEdgeGUI EdgeGUI
    {
        get
        {
            return m_edgeGUI;
        }
    }

    public EditorWindow Host
    {
        get
        {
            return m_hostWindow;
        }
    }
    
    public class GridSettings
    {
        public float paddingMultiplier = 0.6f;
        public float blockSize = 12.0f;

        public float minorLine = 12.0f;
        public float majorLine = 120.0f;

        public Color minorLineColour = new Color(0, 0, 0, 0.18f);
        public Color majorLineColour = new Color(0, 0, 0, 0.28f);

        public bool snap = false;
        public float snapInterval = 12.0f;
    }
}