﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualGraph : ScriptableObject
{
    [SerializeField]
    protected List<VisualNode> m_nodes;

    [SerializeField]
    protected List<VisualEdge> m_edges;

    protected virtual void OnEnable()
    {
        if(m_nodes == null)
        {
            m_nodes = new List<VisualNode>();
        }

        if(m_edges == null)
        {
            m_edges = new List<VisualEdge>();
        }

        WakeupGraph();
    }    
    
    public virtual VisualEdge ConnectPins(VisualPin fromPin, VisualPin toPin)
    {
        VisualEdge edge = null;

        if (CanConnect(fromPin, toPin))
        {
            edge = new VisualEdge(fromPin, toPin);
            //Debug.Log("Connected some edges");
            m_edges.Add(edge);
        }

        return edge;              
    }

    public bool CanConnect(VisualPin fromPin, VisualPin toPin)
    {
        if(fromPin.Owner != toPin.Owner)            
        {
            if(fromPin.Type != toPin.Type)
            {
                return !IsConnected(fromPin, toPin);
            }            
        }

        return false;
    }

    public bool IsConnected(VisualPin pinOne, VisualPin pinTwo)
    {
        foreach(VisualEdge edge in pinOne.Edges)
        {
            if(edge.ToPin == pinOne || edge.ToPin == pinTwo)
            {
                return true;
            }
        }

        return false;
    } 

    public void RemoveConnection(VisualEdge edge)
    {
        if (edge != null)
        {
            if (m_edges.Contains(edge))
            {
                m_edges.Remove(edge);
                edge.FromPin.RemoveConnection(edge);
                edge.ToPin.RemoveConnection(edge);
            }
        }         
    }

    public virtual void RemoveNodes(List<VisualNode> nodes)
    {
        for(int i = 0; i < nodes.Count; i++)
        {
            RemoveNode(nodes[i]);
        }
    }

    public T AddNode<T>() where T : VisualNode
    {
        return (T)AddNode(typeof(T));        
    }

    public VisualNode AddNode(System.Type type)
    {
        VisualNode node = CreateInstance(type) as VisualNode;
        
        if(node == null)
        {
            return null;
        }        

        AddNode(node);

        return node;
    }

    public void AddNode(VisualNode node)
    {
        if (!m_nodes.Contains(node))
        {
            if(node.Owner != null)
            {
                node.Owner.RemoveNode(node);
            }

            m_nodes.Add(node);
            node.Owner = this;
        }
    }

    public void RemoveNode(VisualNode node)
    {
        if (node != null && m_nodes.Contains(node))
        {
            m_nodes.Remove(node);

            node.ClearAllConnections();
        }
    }

    public void ClearGraph()
    {
        m_nodes.Clear();
        m_edges.Clear();                
    }
    
    public void RemoveInvalidEdges()
    {
        m_edges.RemoveAll(EdgeIsValid);        
    }

    private bool EdgeIsValid(VisualEdge edge)
    {
        if(edge.FromPin == null || edge.ToPin == null)
        {
            if(edge.FromPin != null)
            {
                edge.FromPin.RemoveConnection(edge);
            }

            if(edge.ToPin != null)
            {
                edge.ToPin.RemoveConnection(edge);                                
            }

            return true;
        }

        return false;
    }

    public void WakeupGraph()
    {
        m_nodes.RemoveAll((VisualNode node) => { return node == null; });

        m_nodes.ForEach((VisualNode node) => { node.Wakeup(this); });

        m_edges.ForEach((VisualEdge edge) => { edge.Rebuild(); });

        RemoveInvalidEdges();
    }
    
    public List<VisualEdge> Edges
    {
        get
        {
            return m_edges;
        }
    }

    public List<VisualNode> Nodes
    {
        get
        {
            return m_nodes;
        }
    }
}