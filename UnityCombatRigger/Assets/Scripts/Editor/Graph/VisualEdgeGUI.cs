﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Graphs;

public class VisualEdgeGUI
{
    protected List<VisualEdge> m_selectedEdges;

    private VisualGraphGUI m_host;

    protected VisualPin m_startingPin;

    public VisualEdgeGUI()
    {
        m_selectedEdges = new List<VisualEdge>();
    }

    public virtual void OnEdgeGUI()
    {
        Handles.BeginGUI();

        DrawEdges();

        HandleDraggingEdge();
        
        Handles.EndGUI();
    }

    public virtual void HandleDraggingEdge()
    {
        if (Dragging)
        {
            if (Event.current.type == EventType.MouseMove)
            {
                Event.current.Use();
            }
            else
            {
                Handles.color = Color.white;

                Handles.DrawAAPolyLine((Texture2D)Styles.connectionTexture.image, 10f, m_startingPin.Owner.Position.center, Event.current.mousePosition);

                //HandleUtility.Repaint();
            }
        }
    }

    protected virtual void DrawEdges()
    {
        if (Event.current.type != EventType.Repaint)
        {
            return;           
        }

        List<VisualEdge> edges = m_host.Graph.Edges;

        //Debug.Log(m_selectedEdges.Count);

        for (int i = 0; i < edges.Count; i++)
        {
            DrawEdge(edges[i]);
        }
    }

    protected virtual void DrawEdge(VisualEdge edge)
    {
        Vector2 start = edge.FromPin.Owner.Position.center;
        Vector2 end = edge.ToPin.Owner.Position.center;

        Color lineColor = !m_selectedEdges.Contains(edge) ? Color.white : Color.cyan;

        Handles.color = lineColor;

        Handles.DrawAAPolyLine((Texture2D)Styles.connectionTexture.image, 10f, start, end);
    }
    
    public virtual void BeginDragging(VisualPin startPin)
    {
        if (startPin != null)
        {
            m_startingPin = startPin;
            m_host.Host.wantsMouseMove = true;
        }
    }

    public virtual void EndPinDrag(VisualPin endPin)
    {
        if (endPin != null && Dragging)
        {
            m_host.Graph.ConnectPins(m_startingPin, endPin);                     
        }

        m_startingPin = null;
        m_host.Host.wantsMouseMove = false;
    }

    public virtual void SelectEdges(List<VisualNode> selectedNodes)
    {
        m_selectedEdges.Clear();

        List<VisualEdge>.Enumerator edgeEnumerator = m_host.Graph.Edges.GetEnumerator();

        while(edgeEnumerator.MoveNext())
        {
            VisualEdge edge = edgeEnumerator.Current;

            if(selectedNodes.Contains(edge.FromPin.Owner) && selectedNodes.Contains(edge.ToPin.Owner))
            {
                m_selectedEdges.Add(edge);                
            }
        }
    }
    
    public virtual void DrawOverlayGUI()
    {

    } 
    
    public virtual VisualEdge GetNearestEdge()
    {
        VisualEdge edge = null;

        Vector3 mousePos = Event.current.mousePosition;

        float bestDistance = 10;

        foreach(VisualEdge currentEdge in m_host.Graph.Edges)
        {
            Vector3 startPoint = currentEdge.startPosition;
            Vector3 endPoint = currentEdge.endPosition;            

            float distance = HandleUtility.DistancePointLine(mousePos, startPoint, endPoint);

            if(distance < bestDistance)
            {
                edge = currentEdge;
                bestDistance = distance;
            }
        }

        return edge;
    }

    public virtual void ClearSelection()
    {
        m_selectedEdges.Clear();
    }

    public virtual void DeleteSelection()
    {
        for(int i = 0; i < m_selectedEdges.Count; i++)
        {
            m_host.Graph.RemoveConnection(m_selectedEdges[i]);
        }
    }

    public List<VisualEdge> SelectedEdges
    {
        get
        {
            return m_selectedEdges;
        }
    }

    public VisualGraphGUI Owner
    {
        set
        {
            m_host = value;
        }
        get
        {
            return m_host;
        }
    }

    public VisualNode DraggingFromNode
    {
        get
        {
            return m_startingPin.Owner;
        }
    }

    public bool Dragging
    {
        get
        {
            return m_startingPin != null;
        }
    }
}