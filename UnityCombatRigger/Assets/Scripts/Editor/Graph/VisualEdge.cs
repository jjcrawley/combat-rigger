﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class VisualEdge
{
    [SerializeField]
    private Color m_edgeColour;

    [SerializeField]
    private VisualNode m_fromNode;
    [SerializeField]
    private VisualNode m_toNode;

    [SerializeField]
    private string m_fromPinName;
    [SerializeField]
    private string m_toPinName;

    [NonSerialized]
    private VisualPin m_fromPin;

    [NonSerialized]
    private VisualPin m_toPin;

    public Vector2 startPosition;
    public Vector2 endPosition;

    public VisualEdge()
    {

    }

    public VisualEdge(VisualPin from, VisualPin to)
    {
        SetFromPin(from);
        SetToPin(to);
    }

    private void SetFromPin(VisualPin from)
    {
        if(from == null)
        {
            return;
        }

        if(m_fromPin != null)
        {
            m_fromPin.RemoveConnection(this);
        }

        m_fromPin = from;
        m_fromNode = from.Owner;
        m_fromPinName = from.Name;

        m_fromPin.AddConnection(this);
    }

    private void SetToPin(VisualPin to)
    {
        if(to == null)
        {
            return;
        }

        if(m_toPin != null)
        {
            m_toPin.RemoveConnection(this);
        }

        m_toPin = to;
        m_toNode = to.Owner;
        m_toPinName = to.Name;

        m_toPin.AddConnection(this);
    }

    internal void Rebuild()
    {
        if(m_fromNode == null || m_toNode == null)
        {
            return;
        }

        VisualPin pin = m_fromNode[m_fromPinName];
        VisualPin secondPin = m_toNode[m_toPinName];

        SetFromPin(pin);
        SetToPin(secondPin);
    }

    public VisualPin ToPin
    {
        set
        {
            SetToPin(value);                       
        }
        get
        {
            return m_toPin;
        }
    }

    public VisualPin FromPin
    {
        set
        {
            SetFromPin(value);
        }
        get
        {
            return m_fromPin;
        }
    }

    public Color EdgeColour
    {
        set
        {
            m_edgeColour = value;
        }
        get
        {
            return m_edgeColour;
        }
    }
}