﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

internal class AnimControllerLookup
{
    private static List<AnimatorControllerParameter> s_params;

    private static AnimatorController s_controller;

    public static void SetController(AnimatorController controller)
    {
        s_controller = controller;             
    }

    public static List<AnimatorControllerParameter> BuildLookup(AnimatorParam[] parameters)
    {
        if (s_params == null)
        {
            s_params = new List<AnimatorControllerParameter>();
        }

        s_params.Clear();

        if (s_controller == null)
        {
            return s_params;
        }
        
        AnimatorControllerParameter[] controllerParams = s_controller.parameters;

        for (int i = 0; i < controllerParams.Length; i++)
        {
            bool found = false;

            for (int j = 0; j < parameters.Length; j++)
            {
                if (parameters[j].ParameterName == controllerParams[i].name)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                s_params.Add(controllerParams[i]);
            }
        }

        return s_params;
    }  

    public static bool ContainsParameter(string name, AnimatorControllerParameterType type)
    {
        if(s_controller == null)
        {
            return false;
        }

        AnimatorControllerParameter[] parameters = s_controller.parameters;

        for(int i = 0; i < parameters.Length; i++)
        {
            if(parameters[i].name == name && type == parameters[i].type)
            {
                return true;
            }
        }

        return false;               
    }
    
    public static List<AnimatorControllerParameter> GetParams()
    {
        return new List<AnimatorControllerParameter>(s_params);
    }      

    public static bool HasController()
    {
        return s_controller != null;
    }
}