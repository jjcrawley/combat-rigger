﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class GUISplitter
{
    public enum SplitType
    {
        Vertical,
        Horizontal
    }

    [SerializeField]
    private float m_splitWidth;

    [SerializeField]
    private float m_space;

    [SerializeField]
    private Rect m_splitArea;

    [SerializeField]
    private SplitType m_type;
    
    [SerializeField]
    private float m_minSplit;

    [SerializeField]
    private float m_maxSplit;

    [SerializeField]
    private EditorWindow m_window;   

    public GUISplitter()
    {
        m_splitWidth = 0;
        m_type = SplitType.Horizontal;
    }        

    public void DoSplit(Rect resizeArea)
    {
        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        MouseCursor cursor = m_type == SplitType.Horizontal ? MouseCursor.SplitResizeLeftRight : MouseCursor.SplitResizeUpDown;

        resizeArea.width = m_space;
        //resizeArea.position = resizeArea.position;

        EditorGUIUtility.AddCursorRect(resizeArea, cursor, controlID);

        Event current = Event.current;
        
        switch(current.GetTypeForControl(controlID))
        {
            case EventType.MouseDown:
                if(current.button == 0 && resizeArea.Contains(current.mousePosition))
                {
                    GUIUtility.hotControl = controlID;
                    current.Use();
                }
                break;
            case EventType.MouseDrag:
                if (GUIUtility.hotControl == controlID)
                {
                    if (m_type == SplitType.Horizontal)
                    {
                        m_splitWidth += current.delta.x;
                        m_splitWidth = Mathf.Clamp(m_splitWidth, m_minSplit, m_window.position.width - 20);
                    }
                    else
                    {
                        m_splitWidth += current.delta.y;
                        m_splitWidth = Mathf.Clamp(m_splitWidth, m_minSplit, m_window.position.height - 20);
                    }                   
                                        
                    current.Use();
                }
                break;
            case EventType.MouseUp:
                if(GUIUtility.hotControl == controlID)
                {
                    GUIUtility.hotControl = 0;
                    current.Use();
                }
                break;
        }
        
        //EditorGUI.DrawRect(resizeArea, Color.black);
    }

    public float Width
    {
        set
        {
            m_splitWidth = value;            
        }
        get
        {
            return m_splitWidth;
        }
    }

    public float Space
    {
        set
        {
            m_space = value;
        }
        get
        {
            return m_space;
        }
    }

    public float MinSplit
    {
        set
        {
            m_minSplit = value;
            
            if(m_splitWidth < m_minSplit)
            {
                m_splitWidth = value;
            }
        }
        get
        {
            return m_minSplit;
        }
    }

    public float MaxSplit
    {
        set
        {
            m_maxSplit = value;

            if(m_maxSplit < m_splitWidth)
            {
                m_splitWidth = m_maxSplit;
            }
        }
        get
        {
            return m_maxSplit;
        }
    }

    public SplitType Type
    {
        set
        {
            m_type = value;
        }
        get
        {
            return m_type;
        }
    }

    public EditorWindow Window
    {
        set
        {
            m_window = value;
        }
    }
}