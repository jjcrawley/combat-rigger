﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public static class GUIHelpers
{
    private static UsefulStyles s_styles;

    public static void TextFieldWithDropDown(Rect position, SerializedProperty prop, List<string> names, bool alwaysShow)
    {
        TextFieldWithDropDown(position, prop, prop.stringValue, names, alwaysShow);
    }

    public static void TextFieldWithDropDown(Rect namePos, SerializedProperty name, string nameString, List<string> names, bool alwaysShow)
    {
        InitStyles();

        Rect dropDownPos = namePos;

        dropDownPos.width = s_styles.dropButton.fixedWidth;
        dropDownPos.x = namePos.xMax - dropDownPos.width;

        namePos.width -= dropDownPos.width;

        EditorGUI.BeginChangeCheck();

        nameString = EditorGUI.DelayedTextField(namePos, nameString);

        if (EditorGUI.EndChangeCheck())
        {
            name.stringValue = nameString;
        }

        if (!alwaysShow && names.Count <= 0)
        {
            return;
        }

        EditorGUI.BeginChangeCheck();

        int index = EditorGUI.Popup(dropDownPos, -1, names.ToArray(), s_styles.dropButton);

        if (EditorGUI.EndChangeCheck())
        {
            name.stringValue = names[index];
        }
    }

    public static void TextFieldWithDropDown(Rect namePos, SerializedProperty name, GUIContent label, List<string> names, bool alwaysShow)
    {
        InitStyles();

        Rect dropDownPos = namePos;

        dropDownPos.width = s_styles.dropButton.fixedWidth;
        dropDownPos.x = namePos.xMax - dropDownPos.width;

        namePos.width -= dropDownPos.width;

        EditorGUI.BeginChangeCheck();

        string nameString = EditorGUI.DelayedTextField(namePos, name.stringValue);

        if (EditorGUI.EndChangeCheck())
        {
            name.stringValue = nameString;
        }

        if (!alwaysShow && names.Count <= 0)
        {
            return;
        }

        EditorGUI.BeginChangeCheck();

        int index = EditorGUI.Popup(dropDownPos, -1, names.ToArray(), s_styles.dropButton);

        if (EditorGUI.EndChangeCheck())
        {
            name.stringValue = names[index];
        }
    }

    public static void DrawSplitLine(float y, float width)
    {
        Rect position = new Rect(0.0f, y, width, 1.0f);

        Rect texCoords = new Rect(0.0f, 1.0f, 1.0f, 1.0f); //- 1.0f / Style.inspectorTitle.normal.background.height);
        //Debug.Log(texCoords);
        //GUI.DrawTexture(position, Style.inspectorTitle.normal.background);
        GUI.DrawTextureWithTexCoords(position, Styles.inspectorTitle.normal.background, texCoords);
    }

    private static void InitStyles()
    {
        if(s_styles == null)
        {
            s_styles = new UsefulStyles();
        }
    }

    public static UsefulStyles Styles
    {
        get
        {
            InitStyles();
            return s_styles;
        }
    }

    public class UsefulStyles
    {
        public readonly GUIStyle ACButton = "AC Button";      

        public readonly GUIStyle inspectorTitle = "IN Title";

        public readonly GUIContent addButton = EditorGUIUtility.IconContent("Toolbar Plus", "Add parameter");

        public readonly GUIContent removeButton = EditorGUIUtility.IconContent("Toolbar Minus", "Remove Selected parameter");

        public readonly GUIStyle listHeaderStyle = "RL Header";

        public readonly GUIStyle dropButton = "TextFieldDropDown";

        public readonly GUIStyle invisibleButton = "InvisibleButton";        
    }
}