﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class MoveLookup
{
    private static List<CombatMove> s_moves;

    private static List<CombatMove> s_cachedFilter;

    private static List<MoveAssetInfo> s_moveContexts;

    public static void Init()
    {
        CombatMove[] moves = Resources.FindObjectsOfTypeAll<CombatMove>();

        s_moves = new List<CombatMove>(moves);

        s_moveContexts = new List<MoveAssetInfo>(s_moves.Count);

        for (int i = 0; i < s_moves.Count; i++)
        {
            string path = AssetDatabase.GetAssetPath(moves[i]);

            path = path.Remove(0, 7);
            path = path.Remove(path.Length - 6, 6);

            //Debug.Log(path);
            MoveAssetInfo move = new MoveAssetInfo();
            move.move = s_moves[i];
            move.menuPath = path;

            s_moveContexts.Add(move);
        }

        s_moveContexts.Sort((MoveAssetInfo info, MoveAssetInfo other) =>
        {
            return info.menuPath.CompareTo(other.menuPath);
        });
    }

    public static List<CombatMove> GetFiltered(ComboState[] states)
    {
        if(s_cachedFilter == null)
        {
            s_cachedFilter = new List<CombatMove>();
        }

        s_cachedFilter.Clear();

        for(int i = 0; i < states.Length; i++)
        {
            s_cachedFilter.Add(states[i].Move);
        }

        s_cachedFilter = s_cachedFilter.Except(s_moves).ToList();

        return s_cachedFilter;
    }        

    public static List<CombatMove> GetMoves()
    {
        return s_moves;
    }

    public static List<CombatMove> GetCached()
    {
        return s_cachedFilter;
    }

    public static List<MoveAssetInfo> GetInfo()
    {
        return s_moveContexts;
    }

    public class MoveAssetInfo
    {
        public string menuPath;
        public CombatMove move;
    }
}