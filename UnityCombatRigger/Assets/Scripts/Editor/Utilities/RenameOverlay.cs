﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class RenameOverlay
{
    [SerializeField]
    private string m_originalString;

    [SerializeField]
    private string m_editedString;
    
    private bool b_renaming = false;
    
    private Rect m_renameArea;

    private bool b_justStarted;

    private bool b_acceptRename;

    public void BeginRename(string name)
    {
        if(b_renaming)
        {            
            return;
        }

        m_editedString = name;
        m_originalString = name;
        b_renaming = true;
        b_justStarted = true;
        //Debug.Log(name);                
    }

    public bool RenamingGUI(Rect position)
    {
        if(!b_renaming)
        {
            return true;
        }

        if(b_justStarted)
        {
            b_justStarted = false;
            GUI.SetNextControlName("Rename");
        }

        m_renameArea = EditorGUI.IndentedRect(position);

        //Debug.Log(m_renameArea);

        if (!ShouldContinueRename())
        {
            return false;
        }

        //Debug.Log("renaming");       

        m_editedString = EditorGUI.TextField(EditorGUI.IndentedRect(position), m_editedString);
        EditorGUI.FocusTextInControl("Rename");

        return true;
    }

    private bool ShouldContinueRename()
    {
        Event current = Event.current;

        if(current.type == EventType.KeyDown)
        {
            if(current.keyCode == KeyCode.Return || current.keyCode == KeyCode.KeypadEnter)
            {
                current.Use();
                EndRename(true);
                return false;
            }

            if(current.keyCode == KeyCode.Escape)
            {
                current.Use();
                EndRename(false);
                return false;
            }
        }

        //Debug.Log(m_renameArea);
        //Debug.Log(Event.current.mousePosition);

        bool mouseInArea = current.type == EventType.MouseDown && !m_renameArea.Contains(current.mousePosition);

        if(mouseInArea)
        {
            EndRename(false);
            return false;
        }
        
        return true;
    }

    public void EndRename(bool keepChanges)
    {
        b_renaming = false;

        if(!keepChanges)
        {
            m_editedString = m_originalString;
            b_acceptRename = false;
        }

        b_acceptRename = true;

        GUIUtility.keyboardControl = 0;
    }

    public void ApplyRenameWithUndo(Object toRename, string undoMessage)
    {
        Undo.RecordObject(toRename, undoMessage);

        ApplyRename(toRename);
    }

    public void ApplyRename(Object toRename)
    {
        toRename.name = m_editedString;
    }

    public bool Renaming
    {
        get
        {
            return b_renaming;
        }
    }

    public string EditedString
    {
        get
        {
            return m_editedString;
        }
    }

    public bool Accepted
    {
        get
        {
            return b_acceptRename;
        }
    }
}