﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;

internal class MovePreviewer : ScriptableSingleton<MovePreviewer>
{
    [SerializeField]
    private GameObject m_previewObject;

    private Animator m_animator;

    private GUIContent m_previewSettings = new GUIContent("Preview Object", "The object that will be used for previewing hitbox placement");

    private CombatMove m_move;

    private ComboState m_previewingState;

    private ComboLink m_previewingLink;

    internal event Action onAssignTransform;

    private float m_time;

    private bool m_previewing = false;

    private GameObject[] m_validObjects;

    private string[] m_objectNames;

    private int m_chosenObject;

    private bool m_previewActive = true;

    [SerializeField]
    private bool m_previewHitboxes = true;

    private GUIContent m_previewContent = new GUIContent("Preview");

    private DamageBoxPreviewer m_previewer;
        
    private void OnEnable()
    {
        if(m_previewObject != null)
        {
            m_animator = m_previewObject.GetComponent<Animator>();
        }

        m_previewer = new DamageBoxPreviewer();

        CombatMoveInspector.onCombatMoveChange += OnMoveDirty;            
    }

    public void OnDisable()
    {
        if (AnimationMode.InAnimationMode())
        {
            AnimationMode.StopAnimationMode();
            EditorApplication.update -= Update;
        }

        CombatMoveInspector.onCombatMoveChange -= OnMoveDirty;
        SceneView.RepaintAll();

        //Debug.Log(m_usingPreviewer);

        UpdateMove(null);        
    }

    private void OnMoveDirty(CombatMove move)
    {
        if(move != m_move)
        {
            move = m_move;
            m_previewer.Move = move;
        }

        m_previewer.UpdateBoxes();
    }
    
    public void OnSelectionChange()
    {
        if (Selection.objects.Length > 1)
        {
            m_previewActive = false;
            return;
        }
        else
        {
            m_previewActive = true;
        }

        CombatMove move = Selection.activeObject as CombatMove;

        ComboState state = Selection.activeObject as ComboState;

        ComboLink link = Selection.activeObject as ComboLink;
        
        //Debug.Log(selectedObject.name);
        
        if(state != null)
        {
            if(move == null && !(state is EntryState))
            {
                move = state.Move;
                m_previewingState = state;
                m_previewingLink = null;
            }
        }

        if(link != null)
        {
            if(move == null && !(link.Origin is EntryState))
            {
                move = link.Origin.Move;

                m_previewingLink = link;
                m_previewingState = null;
            }                
        }

        if (move != null)
        {
            UpdateMove(move);
            m_previewActive = true;
        }
        else
        {
            m_previewActive = false;
            UpdateMove(null);
        }

        m_time = 0.0f;    
    }

    private void UpdateMove(CombatMove move)
    {
        m_move = move;
        m_previewer.Move = move;

        if (move != null)
        {
            m_previewContent = new GUIContent("Preview " + move.name);
            m_previewActive = true;
        }
        else
        {
            m_previewContent = new GUIContent("Preview");
            m_previewActive = false;
        }
    }

    public void OnOpen()
    {
        UpdateControllerLookup();
        //OnEnable();
    }

    public void ResetPreview()
    {
        m_time = 0.0f;

        if(AnimationMode.InAnimationMode())
        {
            ToggleAnimationMode();
        }
    }

    private void UpdateControllerLookup()
    {
        Animator[] animators = FindObjectsOfType<Animator>();     
        
        List<string> names = new List<string>();

        List<GameObject> objects = new List<GameObject>();

        for(int i = 0; i < animators.Length; i++)
        {            
            names.Add(animators[i].gameObject.name);
            objects.Add(animators[i].gameObject);                                  
        }

        m_validObjects = objects.ToArray();
        m_objectNames = names.ToArray();

        if(m_validObjects.Length > 0)
        {
            m_previewObject = m_validObjects[0];

            OnEnable();

            if(onAssignTransform != null)
            {
                onAssignTransform();
            }
        }
    }

    public void OnGUI()
    {
        EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying || !m_previewActive);

        EditorGUI.BeginDisabledGroup(AnimationMode.InAnimationMode());

        EditorGUI.BeginChangeCheck();

        m_chosenObject = EditorGUILayout.Popup(m_chosenObject, m_objectNames);

            //GameObject current = EditorGUILayout.ObjectField(m_previewSettings, m_previewObject, typeof(GameObject), true) as GameObject;

        if (EditorGUI.EndChangeCheck())
        {
            m_previewObject = m_validObjects[m_chosenObject];

            EditorGUIUtility.PingObject(m_previewObject);

            OnEnable();

            if (onAssignTransform != null)
            {
                onAssignTransform();
            }
        }

        EditorGUI.EndDisabledGroup();

        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginDisabledGroup(m_animator == null);

        EditorGUI.BeginChangeCheck();
        
        EditorGUILayout.ToggleLeft(m_previewContent, AnimationMode.InAnimationMode());

        if(EditorGUI.EndChangeCheck())
        {
            ToggleAnimationMode();
            //m_previewing = AnimationMode.InAnimationMode();
        }                

        EditorGUILayout.EndHorizontal();

        EditorGUI.BeginChangeCheck();

        m_previewHitboxes = EditorGUILayout.ToggleLeft("Preview Hitboxes", m_previewHitboxes);

        if(EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }

        if (m_move == null)
        {
            EditorGUILayout.LabelField("Requires a move to preview");
        }
        else if (m_move.Clip == null)
        {
            EditorGUILayout.LabelField("An animation clip is required to preview the move");
        }
        else
        {
            EditorGUI.BeginDisabledGroup(!AnimationMode.InAnimationMode());             
                              
            EditorGUILayout.BeginHorizontal();

            m_time = GUILayout.HorizontalSlider(m_time, 0, m_move.Clip.length);

            EditorGUILayout.LabelField(GetFrame(m_time).ToString(), GUILayout.Width(50));

            EditorGUILayout.EndHorizontal();

            EditorGUI.EndDisabledGroup();
        }      
        
        EditorGUI.EndDisabledGroup();

        EditorGUI.EndDisabledGroup();
    }

    public void Update()
    {
        if(!Valid)
        {
            return;
        }                
        
        if(!EditorApplication.isPlaying && AnimationMode.InAnimationMode())
        {
            AnimationMode.BeginSampling();

            AnimationMode.SampleAnimationClip(m_previewObject, m_move.Clip, m_time);

            AnimationMode.EndSampling();

            SceneView.RepaintAll();
        }        
    }

    public void OnSceneGUI()
    {
        if (m_previewHitboxes)
        {
            m_previewer.OnSceneGUI();
        }
    }

    private int GetFrame(float time)
    {
        return Mathf.RoundToInt(time * m_move.Clip.frameRate);
    }

    private void ToggleAnimationMode()
    {
        if (!AnimationMode.InAnimationMode())
        {
            EditorApplication.update += Update;
            AnimationMode.StartAnimationMode();
        }
        else
        {
            EditorApplication.update -= Update;
            AnimationMode.StopAnimationMode();
        }
    }

    private bool Valid
    {
        get
        {
            return m_previewObject != null && m_animator != null && m_move != null; 
        }
    }

    public Transform[] GetTransforms(HitboxInformation[] information)
    {
        if (m_previewObject == null)
        {
            return new Transform[0];
        }

        Transform[] transforms = new Transform[information.Length];

        for (int i = 0; i < information.Length; i++)
        {
            transforms[i] = GetTransform(information[i].TransformPath);
        }

        return transforms;
    }

    public Transform GetTransform(string path)
    {
        if (m_previewObject != null && !string.IsNullOrEmpty(path))
        {
            return m_previewObject.transform.FindChild(path);
        }

        return null;
    }

    public GameObject PreviewObject
    {
        set
        {
            m_previewObject = value;
            OnEnable();
        }
        get
        {
            return m_previewObject;
        }
    }

    public bool Previewing
    {
        get
        {
            return m_previewing;
        }
    }

    public bool PreviewActive
    {
        set
        {
            m_previewActive = value;

            if(AnimationMode.InAnimationMode() && !m_previewActive)
            {
                AnimationMode.StopAnimationMode();
                EditorApplication.update -= Update;
            }
        }
        get
        {
            return m_previewActive;
        }
    }
    
    public float CurrentTime
    {
        set
        {
            m_time = value;
        }
        get
        {
            return m_time;
        }
    }

    public CombatMove Move
    {
        set
        {
            UpdateMove(value);      
        }
        get
        {
            return m_move;
        }
    }

    public ComboState State
    {
        set
        {
            if (value != null)
            {
                UpdateMove(value.Move);

                m_previewingState = value;
                m_previewingLink = null;
            }
        }
        get
        {
            return m_previewingState;
        }
    }

    public ComboLink Link
    {
        set
        {
            if (value != null)
            {
                UpdateMove(value.Origin.Move);

                m_previewingLink = value;
                m_previewingState = null;
            }
        }
        get
        {
            return m_previewingLink;
        }
    }
}