﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLookup
{
    private static List<string> s_buttonNames = new List<string>(); 

    public static void InitLookup(IInputSettings handler)
    {
        if(handler == null)
        {
            return;
        }

        s_buttonNames = handler.ButtonNames;

        s_buttonNames.RemoveAll((string test) => 
        {
            return string.IsNullOrEmpty(test);
        });
    }

    public static List<string> ButtonNames
    {
        get
        {
            return s_buttonNames;
        }
    }
}