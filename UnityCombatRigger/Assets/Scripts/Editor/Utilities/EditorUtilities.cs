﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using System.Linq;
using System.Text;

public static class EditorUtilities
{
    public static bool LeftClick(this Event current)
    {
        return current.type == EventType.MouseDown && current.button == 0;
    }    

    public static bool RightClick(this Event current)
    {
        return current.type == EventType.MouseDown && current.button == 1;
    }

    public static bool MiddleClick(this Event current)
    {
        return current.type == EventType.MouseDown && current.button == 2;
    }

    public static bool DoubleClick(this Event current)
    {      
        return current.type == EventType.MouseDown && current.button == 0 && current.clickCount == 2;
    }

    public static bool InRect(this Event current, Rect rect)
    {
        return rect.Contains(current.mousePosition);
    }

    public static List<Type> GetTypes(Type typeToFind)
    {
        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

        List<Type> returnTypes = new List<Type>();
        
        for (int i = 0; i < assemblies.Length; i++)
        {
            Type[] types = assemblies[i].GetTypes();

            returnTypes.AddRange((from Type type in types where type.IsSubclassOf(typeToFind) select type).ToArray());
        }

        return returnTypes;
    }

    public static Rect FromToRect(Vector2 from, Vector2 to)
    {        
        Rect fromTo = new Rect(from.x, from.y, to.x - from.x, to.y - from.y);

        if(fromTo.width < 0)
        {
            fromTo.x += fromTo.width;
            fromTo.width *= -1;
        }

        if(fromTo.height < 0)
        {
            fromTo.y += fromTo.height;
            fromTo.height *= -1;
        }

        return fromTo;
    }

    public static string GetTransformPath(Transform transform)
    {
        Stack<string> names = new Stack<string>();

        do
        {
            names.Push(transform.name);

            transform = transform.parent;
        }
        while (transform.parent != null);

        StringBuilder builder = new StringBuilder();

        while (names.Count > 0)
        {
            builder.Append(names.Pop() + "/");
        }

        builder.Remove(builder.Length - 1, 1);

        return builder.ToString();
    }

    public static string GetTransformNameFromPath(string path)
    {
        if(path.Length == 0)
        {
            return "";
        }

        int lastIndex = path.LastIndexOf("/");

        if(lastIndex == path.Length - 1)
        {
            lastIndex = path.LastIndexOf("/", path.Length - 2, path.Length - 2);
        }

        string result = path.Substring(lastIndex + 1);

        if(result.Contains("/"))
        {
            result = result.Substring(0, result.Length - 1);
        }

        return result;
    }
}