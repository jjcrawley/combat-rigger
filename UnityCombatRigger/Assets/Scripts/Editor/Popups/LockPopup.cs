﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.Callbacks;

[InitializeOnLoad]
public class LockPopup : PopupWindowContent
{
    private static List<Type> s_lockTypes;
    private ComboLink m_editingLink;
    private Action m_repaintCallback;

    private Vector2 m_scrollPos;

    static LockPopup()
    {
        Setup();
    }

    private static void Setup()
    {
        s_lockTypes = EditorUtilities.GetTypes(typeof(ComboLock));
    }

    [DidReloadScripts]
    private static void ReloadScripts()
    {
        Setup();        
    }

    public void Setup(ComboLink link, Action repaintCallback)
    {
        m_editingLink = link;
        m_repaintCallback = repaintCallback;
    }

    public override void OnGUI(Rect rect)
    {
        m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
        
        for(int i = 0; i < s_lockTypes.Count; i++)
        {
            if(GUILayout.Button(ObjectNames.NicifyVariableName(s_lockTypes[i].Name)))
            {
                m_editingLink.AddLock(s_lockTypes[i]);
                editorWindow.Close();
                m_repaintCallback();
            }
        }

        EditorGUILayout.EndScrollView();               
    }
}