﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.Callbacks;

[InitializeOnLoad]
public class ModifierPopup : PopupWindowContent
{
    private static List<Type> s_types;
    private CombatMove m_move;
    private Vector2 m_scrollPos;

    private Action m_repaintFunc;

    static ModifierPopup()
    {
        SetupScripts();
    }

    [DidReloadScripts]
    private static void SetupScripts()
    {
        s_types = EditorUtilities.GetTypes(typeof(MoveModifier));

        //Debug.Log(s_types.Count);
    }

    public void Setup(CombatMove move)
    {
        m_move = move;            
    }

    public void RegisterRepaint(Action action)
    {
        m_repaintFunc = action;
    }

    public override void OnGUI(Rect rect)
    {
        m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);

        for(int i = 0; i < s_types.Count; i++)
        {
            if(GUILayout.Button(ObjectNames.NicifyVariableName(s_types[i].Name)))
            {
                m_move.AddModifier(s_types[i]);
                m_repaintFunc();
                editorWindow.Close();
            }
        }

        EditorGUILayout.EndScrollView();        
    }
}