﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;

[CustomPropertyDrawer(typeof(AllowSceneObjectAttribute))]
public class AllowSceneObjectsDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginChangeCheck();
             
        Transform transform = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Transform), true) as Transform;

        if(EditorGUI.EndChangeCheck())
        {
            property.Next(false);

            Stack<string> names = new Stack<string>();

            do
            {
                names.Push(transform.name);

                transform = transform.parent;
            }
            while (transform.parent != null);

            StringBuilder builder = new StringBuilder();

            while (names.Count > 0)
            {
                builder.Append(names.Pop() + "/");
            }

            builder.Remove(builder.Length - 1, 1);

            property.stringValue = builder.ToString();
        }
    }
}