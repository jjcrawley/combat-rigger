﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SingleLayerAttribute))]
public class SingleLayerPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if(property.propertyType == SerializedPropertyType.Integer)
        {
            EditorGUI.BeginChangeCheck();

            int value = EditorGUI.LayerField(position, label, property.intValue); 
            
            if(EditorGUI.EndChangeCheck())
            {
                property.intValue = value;
            }                       
        }
        else
        {
            EditorGUI.HelpBox(position, "Only use single layer attribute with ints", MessageType.Error);
        }
    }    
}