﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(TranformPreviewAttribute))]
public class TransformPreviewerDrawer : PropertyDrawer
{
    private Transform m_cachedTransform;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if(property.propertyType != SerializedPropertyType.String)
        {
            EditorGUI.HelpBox(position, "This property drawer can only be used with strings", MessageType.Info);                        
        }

        string value = property.stringValue;

        m_cachedTransform = MovePreviewer.instance.GetTransform(value);

        EditorGUI.BeginChangeCheck();

        Transform transform = EditorGUI.ObjectField(new Rect(position.x, position.y, position.width, 16f), "Transform", m_cachedTransform, typeof(Transform), true) as Transform;

        if(!string.IsNullOrEmpty(property.stringValue))
        {
            Rect labelPos = position;
            labelPos.height = 16f;
            labelPos.y += labelPos.height;
            labelPos.width = 200;

            Rect pathLabel = labelPos;
            pathLabel.width = position.width - 50;
            pathLabel.x += 50.0f;

            EditorGUI.LabelField(labelPos, new GUIContent("Path: "));
            EditorGUI.SelectableLabel(pathLabel, property.stringValue);
        }

        if(EditorGUI.EndChangeCheck())
        {
            property.stringValue = EditorUtilities.GetTransformPath(transform);           
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            string value = property.stringValue;

            if (!string.IsNullOrEmpty(value))
            {
                return 32;
            }
        }

        return base.GetPropertyHeight(property, label);
    }
}