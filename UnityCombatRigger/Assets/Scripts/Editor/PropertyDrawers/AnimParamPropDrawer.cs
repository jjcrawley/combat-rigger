﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(AnimatorParam))]
public class AnimParamPropDrawer : PropertyDrawer
{    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {        
        SerializedProperty type = property.FindPropertyRelative("m_type");       

        int theType = type.enumValueIndex;

        SerializedProperty name = property.FindPropertyRelative("m_name");

        SerializedProperty value = null;

        switch (theType)
        {
            case 0:
                value = property.FindPropertyRelative("m_defaultFloat");
                break;
            case 1:
                value = property.FindPropertyRelative("m_defaultInt");
                break;
            case 2:
                value = property.FindPropertyRelative("b_defaultBool");
                break;
            case 3:
                break;
            default:
                break;
        }

        //position.height -= 3;
        position.y += 1.5f;

        if (value != null)
        {
            Rect namePos = new Rect(position.x + 5, position.y, position.width / 3 - 10, position.height);

            Rect typePos = new Rect(namePos.xMax + 10, position.y, position.width / 3 - 10, position.height);

            float width = position.width / 3 - 10;

            Rect valuePos = new Rect(position.xMax - width - 6, position.y, width, position.height);

            EditorGUI.DelayedTextField(namePos, name, GUIContent.none);

            EditorGUI.PropertyField(typePos, type, GUIContent.none);

            EditorGUI.PropertyField(valuePos, value, GUIContent.none);
        }
        else
        {
            Rect namePos = new Rect(position.x + 5, position.y, position.width / 2 - 10, position.height);

            float width = position.width / 2 - 10;

            Rect typePos = new Rect(position.xMax - width + 2, position.y, width, position.height);

            EditorGUI.DelayedTextField(namePos, name, GUIContent.none);

            EditorGUI.PropertyField(typePos, type, GUIContent.none);
        }
    } 
}