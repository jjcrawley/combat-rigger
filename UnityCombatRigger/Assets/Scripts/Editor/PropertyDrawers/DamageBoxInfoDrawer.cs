﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;

[CustomPropertyDrawer(typeof(HitboxInformation))]
public class DamageBoxInfoDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty type = property.FindPropertyRelative("m_type");
        SerializedProperty transformPath = property.FindPropertyRelative("m_transformPath");
        SerializedProperty centre = property.FindPropertyRelative("m_centre");

        EditorGUI.BeginChangeCheck();

        Transform transform = EditorGUILayout.ObjectField(new GUIContent("Transform"), null, typeof(Transform), true) as Transform;

        if (EditorGUI.EndChangeCheck())
        {
            transformPath.stringValue = EditorUtilities.GetTransformPath(transform);
        }

        EditorGUILayout.LabelField(transformPath.stringValue);

        EditorGUILayout.PropertyField(type);

        EditorGUILayout.PropertyField(centre);

        SerializedProperty shapeSpecific = null;

        if (type.enumValueIndex == 0)
        {
            shapeSpecific = property.FindPropertyRelative("m_sphereRadius");

            //EditorGUILayout.LabelField("Found sphere");
        }
        else if (type.enumValueIndex == 1)
        {
            shapeSpecific = property.FindPropertyRelative("m_dimensions");

            //EditorGUILayout.LabelField("Found box");
        }

        EditorGUILayout.PropertyField(shapeSpecific);        
    }    
}