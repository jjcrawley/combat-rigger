﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ComboState), true)]
public class ComboStateInspector : Editor
{
    private class Styles
    {
        public readonly GUIStyle addLockStyle = "AC Button";

        public readonly GUIContent addLockContent = new GUIContent("Add lock");

        public readonly GUIStyle inspectorTitle = "IN Title";

        public readonly GUIContent addButton;

        public readonly GUIContent removeButton;

        public readonly GUIContent headerContent = new GUIContent("Links", "The outgoing links of this state");

        public readonly GUIStyle invisibleButton = "InvisibleButton";

        public Styles()
        {
            removeButton = EditorGUIUtility.IconContent("Toolbar Minus", "Remove Selected parameter");
            removeButton.tooltip = "Remove the selected combo link";

            addButton = EditorGUIUtility.IconContent("Toolbar Plus", "Add parameter");
            addButton.tooltip = "Add a new outgoing link";
        }
    }

    private GUIContent m_resetFrameContent = new GUIContent("Reset Frame", "The frame after which this move will revert the combo controller back to the start");

    public static event System.Action<ComboLink> onLinkSet;

    private Styles m_styles;

    protected Editor m_linkEditor;

    protected ReorderableList m_linkList;

    protected ComboState m_state;

    private SerializedProperty m_resetFrame;

    private SerializedProperty m_combatMove;

    private SerializedProperty m_comboName;

    private Vector2 m_moveScrollPos;

    [SerializeField]
    protected int m_lastSelectedIndex = -1;
      
    private void OnEnable()
    {
        m_state = target as ComboState;

        m_resetFrame = serializedObject.FindProperty("m_resetFrame");

        m_combatMove = serializedObject.FindProperty("m_moveInfo");

        m_comboName = serializedObject.FindProperty("m_comboName");

        SetupList();

        Undo.undoRedoPerformed += OnUndoAndRedo;
        ComboBoxWindow.onComboBoxDirty += OnUndoAndRedo;
        onLinkSet += SetLink; 
    }

    private void OnDestroy()
    {
        if(m_linkEditor != null)
        {
            DestroyImmediate(m_linkEditor);
        }
        
        Undo.undoRedoPerformed -= OnUndoAndRedo;
        ComboBoxWindow.onComboBoxDirty -= OnUndoAndRedo;
        onLinkSet -= SetLink;
    }

    protected virtual void OnUndoAndRedo()
    {
        ResetList();
        m_linkList.ReleaseKeyboardFocus();
        m_linkList.index = -1;
        
        Repaint();
    }    

    private void SetupList()
    {
        ComboLink[] links = new ComboLink[m_state.Links.Length];

        System.Array.Copy(m_state.Links, links, links.Length);  

        if(m_linkList == null)
        {
            m_linkList = new ReorderableList(links, typeof(ComboLink));
            m_linkList.drawElementCallback = OnElementDraw;
            m_linkList.onSelectCallback = OnSelect;
            m_linkList.drawHeaderCallback = OnDrawHeader;
            m_linkList.onReorderCallback = OnReorder;

            m_linkList.footerHeight = 2;

            m_linkList.displayAdd = false;
            m_linkList.displayRemove = false;
        }
        else
        {
            m_linkList.list = links;
        }
    }

    private void OnDrawHeader(Rect position)
    {
        Rect headerPos = new Rect(position.x + 3, position.y, 200, position.height);
        
        Rect removeButton = new Rect(position.xMax - 30, position.y, 25, position.height);

        Rect addButton = new Rect(removeButton.x - 30, position.y, 25, position.height);

        EditorGUI.LabelField(headerPos, m_styles.headerContent);

        EditorGUI.BeginDisabledGroup(m_lastSelectedIndex == -1);

        if(GUI.Button(removeButton, m_styles.removeButton, m_styles.invisibleButton))
        {
            OnRemove(m_lastSelectedIndex);                                    
        }

        EditorGUI.EndDisabledGroup();

        if(GUI.Button(addButton, m_styles.addButton, m_styles.invisibleButton))
        {
            OnAdd(addButton);
        }       
    }
    
    private void OnElementDraw(Rect position, int index, bool selected, bool focused)
    {
        ComboLink link = m_state.Links[index];

        string message = "";
        
        if(link == null)
        {
            return;
        }

        message += link.Origin.ComboName;
                
        message += " -> ";
        
        message += link.Destination.ComboName;       

        EditorGUI.LabelField(position, message);                               
    }

    protected virtual void OnReorder(ReorderableList list)
    {
        m_lastSelectedIndex = list.index;

        ComboLink[] states = list.list as ComboLink[];        

        m_state.ReorderLinks(states);

        SetupList();
    }
    
    private void OnSelect(ReorderableList list)
    {
        if(m_linkEditor != null)
        {
            DestroyImmediate(m_linkEditor);
        }        
        
        ComboLink link = m_state.Links[list.index];

        m_linkEditor = CreateEditor(link);
        m_lastSelectedIndex = list.index;
    }
    
    protected virtual void OnRemove(int index)
    {
        if(m_lastSelectedIndex != -1)
        {
            m_state.RemoveLink(index);            
            ResetList();                
            Repaint();
            ComboBoxWindow.OnComboBoxDirty();            
        }
    }

    protected void ResetList()
    {
        if(m_linkEditor != null)
        {
            DestroyImmediate(m_linkEditor);
        }
        
        m_linkList.ReleaseKeyboardFocus();
        m_lastSelectedIndex = -1;
        m_linkList.index = -1;
        SetupList();
    }

    private void OnAdd(Rect rect)
    {
        GenericMenu menu = new GenericMenu();

        ComboState[] states = ComboBoxWindow.States;
               
        for(int i = 0; i < states.Length; i++)
        {
            string name;
            ComboState state = states[i];

            if(state == target || m_state.IsConnected(state))
            {
                continue;
            }

            name = state.ComboName;
                                   
            menu.AddItem(new GUIContent(name), false, AddLink, state);
        }        
                
        menu.DropDown(rect);                
    }   

    protected virtual void AddLink(object state)
    {
        m_state.AddPlayerLink((ComboState)state);
        SetupList();

        ComboBoxWindow.OnComboBoxDirty();
    }
    
    public override void OnInspectorGUI()
    {
        SetupStyles();

        serializedObject.Update();
                
        EditorGUILayout.PropertyField(m_comboName);
                
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.PropertyField(m_combatMove);

        EditorGUI.BeginDisabledGroup(m_combatMove.objectReferenceValue == null);

        if (GUILayout.Button("Open", EditorStyles.miniButton, GUILayout.Width(40)))
        {
            MoveWindow.OpenWindow(m_combatMove.objectReferenceValue as CombatMove);
        }        

        EditorGUI.EndDisabledGroup();

        EditorGUILayout.EndHorizontal();

        DrawResetFrame();

        DrawLinks();

        serializedObject.ApplyModifiedProperties();
    }

    protected void DrawLinks()
    {
        SetupStyles();

        m_linkList.DoLayoutList();

        if (m_linkEditor != null)
        {
            m_linkEditor.OnInspectorGUI();
        }
    }

    protected virtual void SetLink(ComboLink link)
    {
        int index = ArrayUtility.IndexOf(m_state.Links, link);

        if(index != -1)
        {
            m_linkList.index = index;
            m_lastSelectedIndex = index;

            if (m_linkEditor != null)
            {
                DestroyImmediate(m_linkEditor);
            }

            m_linkEditor = CreateEditor(link);
            Repaint();
        }
    }

    private void DrawResetFrame()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginChangeCheck();
        
        int currentFrame = (int)m_resetFrame.floatValue;

        int maxFrame = m_state.Move == null ? 0 : m_state.Move.FrameCount;

        int frame = EditorGUILayout.IntSlider(m_resetFrameContent, currentFrame, 0, maxFrame);

        if (EditorGUI.EndChangeCheck())
        {
            m_resetFrame.floatValue = frame;
        }

        EditorGUILayout.EndHorizontal();
    }

    public static void UpdateLinkSelection(ComboLink link)
    {
        if (onLinkSet != null)
        {
            onLinkSet(link);
        }
    }

    protected void SetupStyles()
    {
        if(m_styles == null)
        {
            m_styles = new Styles();
        }
    }
}