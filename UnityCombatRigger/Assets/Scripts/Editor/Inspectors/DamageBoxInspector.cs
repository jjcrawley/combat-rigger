﻿using UnityEditor;
using UnityEngine;
using UnityEditor.AnimatedValues;

public class DamageBoxInspector
{
    private SerializedProperty m_damageBoxList;
    private SerializedObject m_serializedMove;

    private bool b_folded;

    private AnimBool m_foldList;
    private CombatMoveInspector m_owner;

    private GUIContent m_hitboxHeader = new GUIContent("Hitbox Information", "The hitbox information for this combat move");

    private GUIContent m_pathContent = new GUIContent("Path: ", "The path to the transform of this hitbox");

    private GUIContent m_tagContent = new GUIContent("Hit Tag", "The tag to detect collisions for");

    private GUIContent m_centreContent = new GUIContent("Centre", "The centre of the hit box, relative to the target transform");

    private GUIContent m_dimensionsContent = new GUIContent("Dimensions", "The height, width and length of the box");

    private GUIContent m_transformContent = new GUIContent("Transform", "The transform for this hitbox, this is setup based on the object being used for previewing set in the move previewer.");

    private GUIContent m_addButton;

    private GUIContent m_removeButton;

    private CombatMove m_move;

    public DamageBoxInspector(SerializedObject move, CombatMoveInspector owner)
    {
        m_damageBoxList = move.FindProperty("m_hitboxes");
        m_serializedMove = move;

        m_foldList = new AnimBool(m_damageBoxList.isExpanded);

        m_owner = owner;
        m_move = m_owner.target as CombatMove;    

        m_foldList.valueChanged.AddListener(owner.RepaintAll);
    }     
        
    public void InitContent()
    {
        m_addButton = new GUIContent(GUIHelpers.Styles.addButton);
        m_addButton.tooltip = "Add a new hitbox definition";

        m_removeButton = new GUIContent(GUIHelpers.Styles.removeButton);
        m_removeButton.tooltip = "Remove hitbox definition";
    }

    public void OnGUI()
    {
        EditorGUILayout.Space();

        DrawHitboxesInfo();
    }    

    private void DrawHitboxesInfo()
    {
        EditorGUILayout.Space();

        //m_previewer.OnGUI();

        EditorGUILayout.BeginVertical(GUI.skin.box);

        DrawHeader();        
        
        if (EditorGUILayout.BeginFadeGroup(m_foldList.faded))
        {
            if (m_serializedMove.isEditingMultipleObjects)
            {
                EditorGUILayout.HelpBox("Damge boxes can't be multi-edited", MessageType.Info);
                return;
            }

            EditorGUI.indentLevel++;

            for (int i = 0; i < m_damageBoxList.arraySize; i++)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);

                SerializedProperty prop = m_damageBoxList.GetArrayElementAtIndex(i);

                DrawHitboxInfo(prop, i);

                EditorGUILayout.EndVertical();

                GUILayout.Space(3);
            }

            EditorGUI.indentLevel--;
        }

        EditorGUILayout.EndFadeGroup();

        EditorGUILayout.EndVertical();        
    }

    private void DrawHitboxInfo(SerializedProperty property, int index)
    {
        SerializedProperty type = property.FindPropertyRelative("m_type");
        SerializedProperty transformPath = property.FindPropertyRelative("m_transformPath");
        SerializedProperty centre = property.FindPropertyRelative("m_centre");
        SerializedProperty tag = property.FindPropertyRelative("m_tag");

        Rect rect = GUILayoutUtility.GetRect(0, 18, GUILayout.ExpandWidth(true));

        string transformName = EditorUtilities.GetTransformNameFromPath(transformPath.stringValue);
        
        Rect dropDownPos = rect;

        GUIContent hitboxContent = new GUIContent("Hitbox " + transformName);

        if (Event.current.type != EventType.Layout)
        {
            dropDownPos.width = EditorStyles.label.CalcSize(hitboxContent).x + 20;
        }        

        GUI.Box(rect, "");

        if (Event.current.type == EventType.Repaint)
        {
            Rect lineRect = dropDownPos;
            lineRect.width = 2;
            lineRect.x = dropDownPos.xMax;

            EditorGUI.DrawRect(lineRect, Color.gray);
        }

        EditorGUI.BeginChangeCheck();

        bool folded = EditorGUI.Foldout(dropDownPos, property.isExpanded, new GUIContent("Hitbox " + transformName), true);

        if(EditorGUI.EndChangeCheck())
        {
            property.isExpanded = folded;
        }

        Rect removeRect = rect;
        removeRect.xMin = rect.xMax - 25;
        removeRect.x -= 10;

        if(GUI.Button(removeRect, m_removeButton, GUIHelpers.Styles.invisibleButton))
        {
            m_damageBoxList.DeleteArrayElementAtIndex(index);
            m_serializedMove.ApplyModifiedProperties();
            m_owner.OnMoveDirty();
            GUIUtility.ExitGUI();
            return;
        }

        if (!property.isExpanded)
        {
            return;
        }

        SerializedProperty shapeSpecific = null;

        EditorGUI.BeginChangeCheck();
        
        Transform transform;

        transform = MovePreviewer.instance.GetTransform(transformPath.stringValue);

        transform = EditorGUILayout.ObjectField(m_transformContent, transform, typeof(Transform), true) as Transform;

        if (EditorGUI.EndChangeCheck())
        {
            transformPath.stringValue = EditorUtilities.GetTransformPath(transform);
        }

        if(!string.IsNullOrEmpty(transformPath.stringValue))
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(m_pathContent, GUILayout.Width(50));
            EditorGUILayout.SelectableLabel(transformPath.stringValue, EditorStyles.label, GUILayout.Height(20));
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.Space(2);

        EditorGUILayout.LabelField("Detection settings", EditorStyles.boldLabel);

        GUILayout.Space(1);

        EditorGUI.BeginChangeCheck();

        string tagVal = EditorGUILayout.TagField(m_tagContent, tag.stringValue);

        if(EditorGUI.EndChangeCheck())
        {
            tag.stringValue = tagVal;
        }

        GUILayout.Space(2);

        EditorGUILayout.LabelField("Shape settings", EditorStyles.boldLabel);

        GUILayout.Space(1);

        EditorGUILayout.PropertyField(type);

        EditorGUI.BeginChangeCheck();
                
        Vector3 currentCentre = EditorGUILayout.Vector3Field(m_centreContent, centre.vector3Value);
                
        if(EditorGUI.EndChangeCheck())
        {
            centre.vector3Value = currentCentre;
        }

        if (type.enumValueIndex == 0)
        {
            shapeSpecific = property.FindPropertyRelative("m_sphereRadius");

            EditorGUILayout.PropertyField(shapeSpecific);
        }
        else if (type.enumValueIndex == 1)
        {
            shapeSpecific = property.FindPropertyRelative("m_dimensions");

            EditorGUI.BeginChangeCheck();

            Vector3 dimens = EditorGUILayout.Vector3Field(m_dimensionsContent, shapeSpecific.vector3Value);

            if(EditorGUI.EndChangeCheck())
            {
                shapeSpecific.vector3Value = dimens;
            }
        }
    }     
    
    private void DrawHeader()
    {        
        Rect rect = GUILayoutUtility.GetRect(0, 18, GUILayout.ExpandWidth(true));

        if (Event.current.type == EventType.Repaint)
        {
            GUIHelpers.Styles.listHeaderStyle.Draw(rect, false, false, false, false);
        }

        Rect headerPos = rect;
        headerPos.xMin += 17;
        headerPos.y += 1;
        headerPos.width = 150;

        if (m_damageBoxList.arraySize > 0)
        {
            EditorGUI.BeginChangeCheck();

            m_foldList.target = EditorGUI.Foldout(headerPos, m_foldList.target, m_hitboxHeader, true);

            if (EditorGUI.EndChangeCheck())
            {
                m_damageBoxList.isExpanded = m_foldList.target;
            }
        }
        else
        {
            EditorGUI.LabelField(headerPos, m_hitboxHeader);
        }

        Rect addButtonPos = rect;
        addButtonPos.width = 25;
        addButtonPos.y += 1;
        addButtonPos.x = rect.xMax - 40;

        if (GUI.Button(addButtonPos, m_addButton, GUIHelpers.Styles.invisibleButton))
        {
            AddBox();
            m_owner.OnMoveDirty();
            GUIUtility.ExitGUI();
        }               
    }    
    
    private void AddBox()
    {
        AddBox(null);        
    } 

    private void AddBox(Transform transform)
    {
        m_damageBoxList.arraySize++;

        SerializedProperty newOne = m_damageBoxList.GetArrayElementAtIndex(m_damageBoxList.arraySize - 1);

        SerializedProperty transformPath = newOne.FindPropertyRelative("m_transformPath");

        if (transform == null)
        {
            transformPath.stringValue = "";
        }
        else
        {
            transformPath.stringValue = EditorUtilities.GetTransformPath(transform);
        }

        SerializedProperty type = newOne.FindPropertyRelative("m_type");
        type.enumValueIndex = 0;

        SerializedProperty radius = newOne.FindPropertyRelative("m_sphereRadius");
        radius.floatValue = 0;

        SerializedProperty dimensions = newOne.FindPropertyRelative("m_dimensions");
        dimensions.vector3Value = Vector3.zero;

        SerializedProperty centre = newOne.FindPropertyRelative("m_centre");
        centre.vector3Value = Vector3.zero;

        m_serializedMove.ApplyModifiedProperties();

        m_damageBoxList.isExpanded = true;
        m_foldList.target = true;
    }  
}