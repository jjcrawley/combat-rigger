﻿using UnityEngine;
using UnityEditor;

[CustomEditor(inspectedType : typeof(ComboBox))]
public class ComboBoxInspector : Editor
{
    private SerializedProperty m_animator;
    private SerializedProperty m_inputSettings;

    private void OnEnable()
    {
        m_animator = serializedObject.FindProperty("m_syncController");
        m_inputSettings = serializedObject.FindProperty("m_inputSettings");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(m_animator);
        EditorGUILayout.PropertyField(m_inputSettings);
        
        serializedObject.ApplyModifiedProperties();

        if (EditorGUI.EndChangeCheck())
        {
            ComboBox box = target as ComboBox;

            AnimControllerLookup.SetController(box.AnimController);
            ButtonLookup.InitLookup(box.InputSettings);
        }
    }
}