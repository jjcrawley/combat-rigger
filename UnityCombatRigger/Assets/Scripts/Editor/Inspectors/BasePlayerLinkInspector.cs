﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BasePlayerLink))]
public class BasePlayerLinkInspector : ComboLinkInspector
{
    private SerializedProperty m_buttonName;

    private GUIContent m_buttonNameContent;

    protected override void OnEnable()
    {
        m_buttonName = serializedObject.FindProperty("m_buttonName");

        m_buttonNameContent = new GUIContent(m_buttonName.displayName, "The button to query from the input handler");

        base.OnEnable();
    }

    protected override void OnSpecialistGUI()
    {
        base.OnSpecialistGUI();
                
        Rect position = GUILayoutUtility.GetRect(m_buttonNameContent, EditorStyles.textField, GUILayout.ExpandWidth(true));
                
        int controlID = GUIUtility.GetControlID(FocusType.Passive);

        Rect labelPos = EditorGUI.PrefixLabel(position, controlID, m_buttonNameContent);
                
        GUIHelpers.TextFieldWithDropDown(labelPos, m_buttonName, m_buttonNameContent, ButtonLookup.ButtonNames, false);                
    }
}