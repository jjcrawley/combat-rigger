﻿using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

[CustomEditor(typeof(PlayerComboLink))]
public class PlayerComboLinkInspector : BasePlayerLinkInspector
{
    private PlayerComboLink m_playerLink;

    private SerializedProperty m_inputStart;
    private SerializedProperty m_inputEnd;

    private SerializedProperty m_fireStart;
    private SerializedProperty m_fireEnd;

    private SerializedProperty m_instantTransition;    

    private AnimBool m_animInstant;

    protected override void OnEnable()
    {
        base.OnEnable();

        m_playerLink = target as PlayerComboLink;

        m_inputEnd = serializedObject.FindProperty("m_inputPeriod.end");
        m_inputStart = serializedObject.FindProperty("m_inputPeriod.start");

        m_fireStart = serializedObject.FindProperty("m_firePeriod.start");
        m_fireEnd = serializedObject.FindProperty("m_firePeriod.end");

        m_instantTransition = serializedObject.FindProperty("b_instantTransition");        

        m_animInstant = new AnimBool(!m_instantTransition.boolValue);
        m_animInstant.valueChanged.AddListener(Repaint);
        InspectorHelper.onInspectorRepaint += Repaint;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        m_animInstant.valueChanged.RemoveAllListeners();
        InspectorHelper.onInspectorRepaint -= Repaint;
    }

    protected override void OnSpecialistGUI()
    {
        base.OnSpecialistGUI();
                
        int frameCount = 60;

        if (m_playerLink.Origin != null && m_playerLink.Origin.Move != null)
        {
            frameCount = m_playerLink.Origin.Move.FrameCount;
        }

        DrawPeriod(m_inputStart, m_inputEnd, frameCount, "Input Period: ", "The frames in which this move will register input");

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(m_instantTransition, new GUIContent("Instant Transition", "Will this link transition instantly, or in a predefined period"));
        
        if(EditorGUI.EndChangeCheck())
        {
            m_animInstant.target = !m_instantTransition.boolValue;
        }

        if(EditorGUILayout.BeginFadeGroup(m_animInstant.faded))
        {
            DrawPeriod(m_fireStart, m_fireEnd, frameCount, "Fire Period: ", "The frames in which this move will fire");
        }

        EditorGUILayout.EndFadeGroup();
    }

    private void DrawPeriod(SerializedProperty startProp, SerializedProperty endProp, int frameCount, string label, string toolTip)
    {
        float end = endProp.floatValue;
        float start = startProp.floatValue;

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(new GUIContent(label, toolTip), GUILayout.Width(100));
        
        start = EditorGUILayout.DelayedFloatField(start, GUILayout.MaxWidth(50));

        GUILayout.Space(2);

        end = EditorGUILayout.DelayedFloatField(end, GUILayout.MaxWidth(50));

        GUILayout.Space(2);

        EditorGUILayout.MinMaxSlider(ref start, ref end, 0, frameCount);

        EditorGUILayout.EndHorizontal();

        if (EditorGUI.EndChangeCheck())
        {
            end = Mathf.Round(end);
            start = Mathf.Round(start);

            end = Mathf.Clamp(end, 0, frameCount);
            start = Mathf.Clamp(start, 0, frameCount);

            end = Mathf.Max(start, end);

            if(float.IsNaN(end))
            {
                end = 0;
            }
            
            if(float.IsNaN(start))
            {
                start = 0;
            }

            startProp.floatValue = start;
            endProp.floatValue = end;
        }
    }    
}