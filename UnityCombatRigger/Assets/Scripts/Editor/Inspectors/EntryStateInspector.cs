﻿using UnityEditor;

[CustomEditor(typeof(EntryState))]
public class EntryStateInspector : ComboStateInspector
{    
    public override void OnInspectorGUI()
    {
        DrawLinks();
    }

    protected override void AddLink(object state)
    {
        ComboTree tree = ComboBoxWindow.EditingComboTree;

        tree.AddEntryLink(state as ComboState);

        m_lastSelectedIndex = -1;
        ResetList();

        ComboBoxWindow.OnComboBoxDirty();
    }   

    protected override void OnRemove(int index)
    {
        ComboTree tree = ComboBoxWindow.EditingComboTree;

        if(index == -1)
        {
            return;
        }

        tree.RemoveEntryLink(index);

        m_lastSelectedIndex = -1;
        ResetList();

        ComboBoxWindow.OnComboBoxDirty();
    }
}