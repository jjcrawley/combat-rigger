﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ComboTree))]
public class ComboTreeInspector : Editor
{
    private ComboTree m_tree;

    private ResetLink m_resetLink;

    private ResetLinkInspector m_resetLinkInspector;
    
    private void OnEnable()
    {
        m_tree = target as ComboTree;

        m_resetLink = m_tree.ResetTransition as ResetLink;

        if(m_resetLink == null)
        {
            Debug.Log("Reset link is null?");
        }
        else
        {
            m_resetLinkInspector = CreateEditor(m_resetLink) as ResetLinkInspector;
        }
    }

    private void OnDisable()
    {
        if(m_resetLinkInspector != null)
        {
            DestroyImmediate(m_resetLinkInspector);
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Animation Reset Settings");

        if(m_resetLinkInspector != null)
        {
            m_resetLinkInspector.OnInspectorGUI();
        }
    }
}