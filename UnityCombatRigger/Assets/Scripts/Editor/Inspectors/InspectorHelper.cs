﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InspectorHelper
{
    public static event System.Action onInspectorRepaint;

    public static void RepaintInspectors()
    {
        if(onInspectorRepaint != null)
        {
            onInspectorRepaint();
        }
    }
}