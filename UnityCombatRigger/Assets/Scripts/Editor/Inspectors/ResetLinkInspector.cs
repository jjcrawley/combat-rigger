﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ResetLink))]
public sealed class ResetLinkInspector : ComboLinkInspector
{
    public override void OnInspectorGUI()
    {
        DrawParameterList();
    }
}