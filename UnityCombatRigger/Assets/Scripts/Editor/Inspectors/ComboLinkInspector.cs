﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

using AnimParameter = UnityEngine.AnimatorControllerParameter;
using AnimParamType = UnityEngine.AnimatorControllerParameterType;

[CustomEditor(typeof(ComboLink))]
public class ComboLinkInspector : Editor
{
    private class Styles
    {
        public readonly GUIStyle addLockStyle = "AC Button";

        public readonly GUIContent addLockContent = new GUIContent("Add lock");

        public readonly GUIStyle inspectorTitle = "IN Title";

        public readonly GUIContent addButton = EditorGUIUtility.IconContent("Toolbar Plus", "Add parameter");

        public readonly GUIContent removeButton = EditorGUIUtility.IconContent("Toolbar Minus", "Remove Selected parameter");

        public readonly GUIStyle dropButton = "TextFieldDropDown";

        public readonly GUIStyle invisibleButton = "InvisibleButton";
    }

    private ReorderableList m_paramsList;

    private Editor[] m_comboLockInspectors;
    private bool[] m_folded;

    private static Action<ComboLock> s_removeAction;

    private Styles m_styles;

    private Rect m_activatorRect;

    private SerializedProperty m_params;

    private ComboLink m_link;

    private List<AnimParameter> m_notAddedParams;

    private RenameOverlay m_renamer;

    private int m_lastSelectedIndex;
    private bool b_rebuildParams;

    protected virtual void OnEnable()
    {
        m_link = target as ComboLink;

        m_params = serializedObject.FindProperty("m_params");

        m_paramsList = new ReorderableList(serializedObject, m_params);

        m_paramsList.drawElementCallback = OnDrawElement;
        m_paramsList.drawHeaderCallback = OnDrawHeader;
        m_paramsList.onSelectCallback = OnSelect;

        m_paramsList.displayAdd = false;
        m_paramsList.displayRemove = false;
        m_paramsList.footerHeight = 2;

        Undo.undoRedoPerformed += OnUndoAndRedo;
        s_removeAction += RemoveLock;

        SetupLocks();

        m_renamer = new RenameOverlay();
        b_rebuildParams = true;
    }

    protected virtual void OnDestroy()
    {
        Undo.undoRedoPerformed -= OnUndoAndRedo;
        s_removeAction -= RemoveLock;
    }

    private void OnUndoAndRedo()
    {
        SetupLocks();
        b_rebuildParams = true;
        SetupAnimParams();
        m_renamer.EndRename(false);
        Repaint();
    }

    private void SetupAnimParams()
    {
        if(!b_rebuildParams && m_notAddedParams != null)
        {
            return;
        }
              
        m_notAddedParams = AnimControllerLookup.BuildLookup(m_link.Params);

        b_rebuildParams = false;
    }

    private void OnRemove(int index)
    {
        if(index < 0 || index > m_params.arraySize - 1)
        {
            return;
        }

        serializedObject.Update();

        m_params.DeleteArrayElementAtIndex(index);

        if(index >= m_params.arraySize - 1)
        {
            m_paramsList.index = m_params.arraySize - 1;
        }

        serializedObject.ApplyModifiedProperties();
        b_rebuildParams = true;        
    }

    private void OnDrawHeader(Rect position)
    {
        Rect titlePos = new Rect(position.x, position.y, 200, position.height);
        
        Rect deletePos = new Rect(position.xMax - 24, position.y, 20, position.height);

        Rect buttonPos = new Rect(deletePos.x - 24, position.y, 20, position.height);
        
        EditorGUI.LabelField(titlePos, new GUIContent("Parameters", "Parameter settings for this link"));

        if(GUI.Button(deletePos, m_styles.removeButton, m_styles.invisibleButton))
        {
            OnRemove(m_paramsList.index);
        }

        if(GUI.Button(buttonPos, m_styles.addButton, m_styles.invisibleButton))
        {
            OnAddButtonPressed(buttonPos);
        }
    }

    private void OnAddButtonPressed(Rect position)
    {
        GenericMenu menu = new GenericMenu();       
        
        BuildAddMenu(menu);

        menu.DropDown(position);
    }

    private void BuildAddMenu(GenericMenu menu)
    {        
        for(int i = 0; i < m_notAddedParams.Count; i++)
        {
            menu.AddItem(new GUIContent(m_notAddedParams[i].name), false, (object data) => { OnAdd((AnimParameter)data); } , m_notAddedParams[i]);
        }

        AnimParameter param = new AnimParameter();
        param.name = "New param";
        param.type = AnimParamType.Trigger;

        menu.AddItem(new GUIContent("Add new"), false, (object data) => 
        {
            OnAdd((AnimParameter)data);
        }, param);
    }

    private void OnAdd(AnimParameter parameter)
    {
        serializedObject.Update();

        m_params.arraySize++;
        m_paramsList.index = m_params.arraySize - 1;

        SerializedProperty newParam = m_params.GetArrayElementAtIndex(m_params.arraySize - 1);

        newParam.FindPropertyRelative("m_name").stringValue = parameter.name;

        SerializedProperty typeParam = newParam.FindPropertyRelative("m_type");

        switch (parameter.type)
        {
            case AnimParamType.Float:
                typeParam.enumValueIndex = 0;
                break;
            case AnimParamType.Int:
                typeParam.enumValueIndex = 1;
                break;
            case AnimParamType.Bool:
                typeParam.enumValueIndex = 2;
                break;
            case AnimParamType.Trigger:
                typeParam.enumValueIndex = 3;
                break;
            default:
                break;
        }

        serializedObject.ApplyModifiedProperties();

        b_rebuildParams = true;   
    }

    private void OnDrawElement(Rect rect, int index, bool selected, bool focused)
    {
        SerializedProperty prop = m_params.GetArrayElementAtIndex(index);

        DrawParameter(prop, rect, selected);

        Event current = Event.current;
        
        if(current.InRect(rect) && current.type == EventType.ContextClick)
        {
            GenericMenu menu = new GenericMenu();

            menu.AddItem(new GUIContent("Delete"), false, () => { OnRemove(index); });
            menu.ShowAsContext();
            current.Use();
        }       
    }

    private void DrawParameter(SerializedProperty property, Rect position, bool selected)
    {
        SerializedProperty type = property.FindPropertyRelative("m_type");

        int theType = type.enumValueIndex;

        SerializedProperty name = property.FindPropertyRelative("m_name");

        SerializedProperty value = null;

        AnimParamType actualType;

        switch (theType)
        {
            case 0:
                value = property.FindPropertyRelative("m_defaultFloat");
                actualType = AnimParamType.Float;
                break;
            case 1:
                value = property.FindPropertyRelative("m_defaultInt");
                actualType = AnimParamType.Int;
                break;
            case 2:
                value = property.FindPropertyRelative("b_defaultBool");
                actualType = AnimParamType.Bool;
                break;
            case 3:
                actualType = AnimParamType.Trigger;
                break;
            default:
                actualType = AnimParamType.Trigger;
                break;
        }

        position.height -= 3;
        position.y += 1.5f;

        Rect namePos;
        Rect typePos;
        Rect valuePos = new Rect();

        if (value != null)
        {
            namePos = new Rect(position.x + 5, position.y, position.width / 3 - 10, position.height - 2);

            typePos = new Rect(namePos.xMax + 10, position.y, position.width / 3 - 10, position.height - 2);

            float width = position.width / 3 - 10;

            valuePos = new Rect(position.xMax - width - 6, position.y, width, position.height - 2);                                    
        }
        else
        {            
            namePos = new Rect(position.x + 5, position.y, position.width / 2 - 10, position.height - 2);

            float width = position.width / 2 - 10;

            typePos = new Rect(position.xMax - width + 2, position.y, width, position.height - 2);
        }
               
        string nameString = name.stringValue;
                
        //bool containsParam = AnimControllerLookup.ContainsParameter(nameString, actualType);

        //if (!containsParam)
        //{
        //    nameString += "!";

        //    namePos.width -= 20;

        //    //typePos.width -= 20;
        //    typePos.x -= 20;

        //    if(value != null)
        //    {
        //        //valuePos.width -= 20;
        //        valuePos.x -= 20;
        //    }                       
        //}

        List<string> names = new List<string>();

        for (int i = 0; i < m_notAddedParams.Count; i++)
        {
            names.Add(m_notAddedParams[i].name);
        }

        EditorGUI.BeginChangeCheck();

        GUIHelpers.TextFieldWithDropDown(namePos, name, nameString, names, false);

        if(EditorGUI.EndChangeCheck())
        {
            b_rebuildParams = true;
        }

        EditorGUI.PropertyField(typePos, type, GUIContent.none);

        if (value != null)
        {
            EditorGUI.PropertyField(valuePos, value, GUIContent.none);
        }

        //if(!containsParam)
        //{
        //    Rect warningPos = position;

        //    warningPos.width = 20;
        //    warningPos.x = position.xMax - 20;

        //    EditorGUI.HelpBox(warningPos, "", MessageType.Warning);

        //    string message = "This parameter isn't part of the assigned controller";

        //    if(!AnimControllerLookup.HasController())
        //    {
        //        message = "You haven't assigned an animator controller to the combo box";
        //    }

        //    GUI.Label(warningPos, new GUIContent("", message));
        //}
    }      

    private void OnSelect(ReorderableList list)
    {
        Event current = Event.current;

        int index = list.index;
        
        //if(current.button == 0 && Event.current.clickCount == 2 && m_lastSelectedIndex == index)
        //{
        //    SerializedProperty nameProp = m_params.GetArrayElementAtIndex(index);

        //    string name = nameProp.FindPropertyRelative("m_name").stringValue;

        //    m_renamer.BeginRename(name);
        //    m_lastSelectedIndex = -1;
        //    m_list.ReleaseKeyboardFocus();
        //}

        m_lastSelectedIndex = index;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        serializedObject.Update();

        InitStyles();       

        OnSpecialistGUI();
        
        DrawParameterList();

        DrawLocks();

        serializedObject.ApplyModifiedProperties();
    }

    protected virtual void OnSpecialistGUI()
    {
       
    }

    protected void DrawAddLockButton()
    {
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();

        GUILayout.FlexibleSpace();
        
        if(GUILayout.Button(m_styles.addLockContent, m_styles.addLockStyle))
        {
            LockPopup popup = new LockPopup();

            popup.Setup(target as ComboLink, UpdateLocks);

            PopupWindow.Show(m_activatorRect, popup);
        }

        if(Event.current.type == EventType.Repaint)
        {
            m_activatorRect = GUILayoutUtility.GetLastRect();
        }

        Rect rect = GUILayoutUtility.GetLastRect();

        if (Event.current.type == EventType.Repaint)
        {
            DrawSplitLine(rect.yMin - 11.0f);
        }

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndHorizontal();
    }

    protected void DrawParameterList()
    {
        InitStyles();

        SetupAnimParams();

        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical();
                
        m_paramsList.DoLayoutList();
                
        EditorGUILayout.EndVertical();

        Rect checkRect = GUILayoutUtility.GetLastRect();

        Event current = Event.current;

        if(!current.InRect(checkRect) && current.type == EventType.MouseDown)
        {
            m_paramsList.ReleaseKeyboardFocus();
            m_paramsList.index = -1;
        }
    }

    private void DrawSplitLine(float y)
    {
        Rect position = new Rect(0.0f, y, EditorGUIUtility.currentViewWidth + 1.0f, 1.0f);

        Rect texCoords = new Rect(0.0f, 1.0f, 1.0f, 1.0f);
        
        GUI.DrawTextureWithTexCoords(position, m_styles.inspectorTitle.normal.background, texCoords);
    }

    protected void DrawLocks()
    {
        InitStyles();

        for (int i = 0; i < m_comboLockInspectors.Length; i++)
        {
            ComboLock current = m_link[i];

            EditorGUILayout.BeginHorizontal();
                               
            m_folded[i] = EditorGUILayout.InspectorTitlebar(m_folded[i], current);
                        
            EditorGUILayout.EndHorizontal();

            if (m_folded[i])
            {
                m_comboLockInspectors[i].OnInspectorGUI();
            }
        }
        
        DrawAddLockButton();
    }    

    private void SetupLocks()
    {
        ComboLink link = target as ComboLink;

        m_comboLockInspectors = new Editor[link.Locks.Length];

        for (int i = 0; i < link.Locks.Length; i++)
        {
            m_comboLockInspectors[i] = CreateEditor(link[i]);
        }

        m_folded = new bool[m_comboLockInspectors.Length];

        for(int i = 0; i < m_folded.Length; i++)
        {
            m_folded[i] = true;
        }
    }   
    
    private void RemoveLock(ComboLock theLock)
    {
        if(m_link.IsOwner(theLock))
        {
            int index = ArrayUtility.IndexOf(m_link.Locks, theLock);

            m_link.RemoveLock(index);
        }

        SetupLocks();
    }

    [MenuItem("CONTEXT/ComboLock/Remove Lock")]
    private static void RemoveLock(MenuCommand command)
    {
        ComboLock theLock = (ComboLock)command.context;

        s_removeAction(theLock);                        
    }

    private void UpdateLocks()
    {
        SetupLocks();
        Repaint();
    }

    protected internal void InitStyles()
    {
        if(m_styles == null)
        {
            m_styles = new Styles();
        }
    }    
}