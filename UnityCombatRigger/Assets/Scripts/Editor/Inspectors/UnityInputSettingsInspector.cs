﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(UnityInputSettings))]
public class UnityInputSettingsInspector : Editor
{    
    private List<string> m_names = new List<string>();

    private SerializedProperty m_settingsArray;

    private ReorderableList m_inputSettingsList;

    private void OnEnable()
    {
        Object inputSettings = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

        SerializedObject inputManager = new SerializedObject(inputSettings);

        SerializedProperty axis = inputManager.FindProperty("m_Axes");

        m_names.Capacity = axis.arraySize;

        m_names.Clear();

        for(int i = 0; i < axis.arraySize; i++)
        {
            m_names.Add(axis.GetArrayElementAtIndex(i).displayName);
        }

        m_settingsArray = serializedObject.FindProperty("m_inputSettings");

        m_inputSettingsList = new ReorderableList(serializedObject, m_settingsArray);
        m_inputSettingsList.drawElementCallback = OnDrawElement;
        m_inputSettingsList.drawHeaderCallback = OnDrawHeader;
        m_inputSettingsList.onAddCallback = OnAdd;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.LabelField("Input settings");
                
        m_inputSettingsList.DoLayoutList();

        serializedObject.ApplyModifiedProperties();
    }

    private void DrawInputSetting(SerializedProperty prop)
    {
        SerializedProperty buttonName = prop.FindPropertyRelative("name");
        SerializedProperty unityName = prop.FindPropertyRelative("unityInput");

        EditorGUILayout.BeginHorizontal();
                
        EditorGUILayout.DelayedTextField(buttonName, GUIContent.none);

        Rect rect = EditorGUILayout.GetControlRect(false);

        GUIHelpers.TextFieldWithDropDown(rect, unityName, m_names, true);

        EditorGUILayout.EndHorizontal();
    }

    private void OnDrawHeader(Rect position)
    {
        Rect namePos = position;
        namePos.width /= 2;
        namePos.xMin += 13;

        Rect unityNamePos = position;
        unityNamePos.width /= 2;
        unityNamePos.x += unityNamePos.width + 7;

        EditorGUI.LabelField(namePos, "Button Name");

        EditorGUI.LabelField(unityNamePos, "Unity Name");
    }

    private void OnDrawElement(Rect position, int index, bool selected, bool focused)
    {
        position.y += 1.5f;
        position.height -= 3.0f;

        Rect namePos = position;
        namePos.width /= 2;
        namePos.xMax -= 5;
        namePos.height -= 2;

        Rect unityNamePos = namePos;

        unityNamePos.x += unityNamePos.width + 5;
        unityNamePos.xMax += 5;

        SerializedProperty currentProp = m_settingsArray.GetArrayElementAtIndex(index);

        SerializedProperty nameProp = currentProp.FindPropertyRelative("name");
        SerializedProperty unityName = currentProp.FindPropertyRelative("unityInput");

        EditorGUI.DelayedTextField(namePos, nameProp, GUIContent.none);

        GUIHelpers.TextFieldWithDropDown(unityNamePos, unityName, m_names, true);
    }

    private void OnAdd(ReorderableList list)
    {
        serializedObject.Update();

        m_settingsArray.arraySize++;

        SerializedProperty currentProp = m_settingsArray.GetArrayElementAtIndex(m_settingsArray.arraySize - 1);

        SerializedProperty nameProp = currentProp.FindPropertyRelative("name");
        SerializedProperty unityNameProp = currentProp.FindPropertyRelative("unityInput");

        nameProp.stringValue = "New input";
        unityNameProp.stringValue = m_names[0];

        serializedObject.ApplyModifiedProperties();
    }
}