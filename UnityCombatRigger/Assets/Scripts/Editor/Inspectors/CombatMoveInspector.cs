﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditorInternal;

[CustomEditor(inspectedType : typeof(CombatMove), editorForChildClasses : true)]
public class CombatMoveInspector : Editor
{
    private class Styles
    {
        public readonly GUIStyle addModButton = "AC Button";

        public readonly GUIContent addModContent = new GUIContent("Add modifier");
    }

    private static CombatMoveInspector s_instance;

    internal static event System.Action onModsChange;

    internal static event System.Action<CombatMove> onCombatMoveChange;

    internal event System.Action onRepaint;

    private Editor[] m_moveModifiers;
    private CombatMove m_move;
    
    private bool b_added = false;      
    private Rect m_buttonRect;

    private AnimBool m_damageFoldout;

    private float m_sampleRate = 60.0f;

    private Styles m_styles;

    private Undo.UndoRedoCallback m_undoRedo;  
        
    private SerializedProperty m_animationClip;
    private SerializedProperty m_frameCount;

    private SerializedProperty m_activationStart;
    private SerializedProperty m_activationEnd;

    private SerializedProperty m_continuous;
    private SerializedProperty m_damageInterval;

    private SerializedProperty m_damageBoxList;

    private SerializedProperty m_description;   
     
    private GameObject m_animationObjectToPreview;
    private int m_framePos = 0;
    private float m_time = 0;
    private bool m_folded;

    private DamageBoxInspector m_damageBoxGUI;

    internal bool m_openedInWindow;

    protected virtual void OnEnable()
    {
        m_move = target as CombatMove;
        
        m_animationClip = serializedObject.FindProperty("m_clip");
        m_frameCount = serializedObject.FindProperty("m_frameCount");

        m_description = serializedObject.FindProperty("m_description");

        m_damageBoxList = serializedObject.FindProperty("m_hitboxes");

        m_damageInterval = serializedObject.FindProperty("m_damageInterval");

        m_activationStart = serializedObject.FindProperty("m_activationPeriod.start");
        m_activationEnd = serializedObject.FindProperty("m_activationPeriod.end");

        m_continuous = serializedObject.FindProperty("b_continuous");

        m_damageBoxGUI = new DamageBoxInspector(serializedObject, this);

        UpdateModifiers();
        m_undoRedo = UndoAndRedo;
        Undo.undoRedoPerformed += m_undoRedo;

        m_damageFoldout = new AnimBool(m_continuous.boolValue);
        m_damageFoldout.valueChanged.AddListener(RepaintAll);

        onRepaint += Repaint;

        onModsChange += UpdateModifiers;

        if (s_instance == null)
        {           
            s_instance = this;
        }
    }

    private void OnDisable()
    {
        onModsChange -= UpdateModifiers;
        Undo.undoRedoPerformed -= m_undoRedo;
    }
        
    public void RepaintAll()
    {
        if(onRepaint != null)
        {
            onRepaint();
        }
    }

    public void OnMoveDirty()
    {
        if(onCombatMoveChange != null)
        {
            onCombatMoveChange(m_move);
        }
    }
    
    protected virtual void UndoAndRedo()
    {       
        UpdateModifiers();
        RepaintAll();
    }

    protected virtual void OnRepaint()
    {
        UpdateModifiers();
        RepaintAll();
    }   

    public override void OnInspectorGUI()
    {        
        SetupStyles();

        serializedObject.Update();
                
        DrawInfo();
        
        EditorGUILayout.Space();
                
        DrawDamageSettings();

        m_damageBoxGUI.OnGUI();
        
        if (serializedObject.isEditingMultipleObjects)
        {
            serializedObject.ApplyModifiedProperties();
            return;
        }

        EditorGUILayout.Space();

        if(GUILayout.Button("Open previewer"))
        {
            PreviewerWindow.OpenWindow();

            PreviewerWindow.Move = target as CombatMove;
        }

        EditorGUILayout.Space();

        DrawModifierSection();       

        serializedObject.ApplyModifiedProperties();
    }    

    public void DrawInfo()
    {
        EditorGUILayout.PropertyField(m_description);

        EditorGUILayout.PropertyField(m_animationClip);

        if (m_animationClip.objectReferenceValue == null)
        {
            EditorGUILayout.HelpBox("Please enter the frame count for the animation you may be using", MessageType.Info);

            EditorGUILayout.PropertyField(m_frameCount);
        }
    }
    
    public void DrawDamageSettings()
    {
        EditorGUILayout.LabelField("Damage settings");

        DrawPeriod(m_activationStart, m_activationEnd, m_frameCount.intValue, "Activation Period", "The activation Period for the hitboxes");

        EditorGUI.BeginChangeCheck();
                
        bool continuous = EditorGUILayout.Toggle(new GUIContent("Continuous", "Should the move apply damage for the duration of contact"), m_continuous.boolValue);

        if(EditorGUI.EndChangeCheck())
        {
            m_continuous.boolValue = continuous;
            m_damageFoldout.target = continuous;
        }

        if(EditorGUILayout.BeginFadeGroup(m_damageFoldout.faded))
        {
            EditorGUILayout.PropertyField(m_damageInterval);
        }

        EditorGUILayout.EndFadeGroup();
    }

    private void DrawPeriod(SerializedProperty startProp, SerializedProperty endProp, int frameCount, string label, string toolTip)
    {
        float end = endProp.floatValue;
        float start = startProp.floatValue;

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(new GUIContent(label, toolTip), GUILayout.Width(100));

        start = EditorGUILayout.DelayedFloatField(start, GUILayout.MaxWidth(50));

        GUILayout.Space(2);

        end = EditorGUILayout.DelayedFloatField(end, GUILayout.MaxWidth(50));

        GUILayout.Space(2);

        EditorGUILayout.MinMaxSlider(ref start, ref end, 0, frameCount);

        EditorGUILayout.EndHorizontal();

        if (EditorGUI.EndChangeCheck())
        {
            end = Mathf.Round(end);
            start = Mathf.Round(start);

            end = Mathf.Clamp(end, 0, frameCount);
            start = Mathf.Clamp(start, 0, frameCount);

            end = Mathf.Max(start, end);

            if (float.IsNaN(end))
            {
                end = 0;
            }

            if (float.IsNaN(start))
            {
                start = 0;
            }

            startProp.floatValue = start;
            endProp.floatValue = end;
        }
    }
        
    public void DrawModifierSection()
    {                   
        for (int i = 0; i < m_moveModifiers.Length; i++)
        {            
            bool isExpanded = InternalEditorUtility.GetIsInspectorExpanded(m_move.Modifiers[i]);
                       
            bool opened = EditorGUILayout.InspectorTitlebar(isExpanded, m_move.Modifiers[i]);
            
            if(isExpanded != opened)
            {
                InternalEditorUtility.SetIsInspectorExpanded(m_move.Modifiers[i], opened);
            }

            if (opened && m_moveModifiers[i] != null)
            {
                m_moveModifiers[i].OnInspectorGUI();                
            }

            GUILayout.Space(3);
        }

        EditorGUILayout.BeginHorizontal();

        GUILayout.FlexibleSpace();

        Rect rect = GUILayoutUtility.GetRect(m_styles.addModContent, m_styles.addModButton);

        if (Event.current.type == EventType.Repaint)
        {
            GUIHelpers.DrawSplitLine(rect.y - 11.0f, EditorGUIUtility.currentViewWidth + 1.0f);
        }

        if (GUI.Button(rect, m_styles.addModContent, m_styles.addModButton))
        {
            ModifierPopup popup = new ModifierPopup();

            popup.Setup(m_move);
            popup.RegisterRepaint(OnRepaint);

            PopupWindow.Show(m_buttonRect, popup);
        }

        if (Event.current.type == EventType.Repaint)
        {
            m_buttonRect = GUILayoutUtility.GetLastRect();
        }

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndHorizontal();
    }     

    [MenuItem("CONTEXT/MoveModifier/Delete")]
    private static void DeleteModifier(MenuCommand command)
    {
        MoveModifier mod = (MoveModifier)command.context;

        mod.Move.RemoveModifier(mod);

        onModsChange();
    }

    private void DeleteModifier(int index)
    {
        m_move.RemoveModifier(index);

        UpdateModifiers();
    }    

    private void UpdateModifiers()
    {
        if(m_moveModifiers != null)
        {
            for(int i = 0; i < m_moveModifiers.Length; i++)
            {
                DestroyImmediate(m_moveModifiers[i]);
            }
        }

        m_moveModifiers = new Editor[m_move.Modifiers.Length];

        for (int i = 0; i < m_moveModifiers.Length; i++)
        {
            m_moveModifiers[i] = CreateEditor(m_move.Modifiers[i]);
        }
    }

    private void SetupStyles()
    {
        if(m_styles == null)
        {
            m_styles = new Styles();
            m_damageBoxGUI.InitContent();
        }
    }    
}