﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class DamageBoxPreviewer
{
    private Transform[] m_transforms;

    private HitboxInformation[] m_information;

    private MovePreviewer m_previewer;

    private Color m_disabledBoxColour = Color.blue;

    private Color m_boxEnabledColour = new Color(1.0f, 0, 0, 0.5f);

    private CombatMove m_move;
    
    public DamageBoxPreviewer()
    {
        m_previewer = MovePreviewer.instance;
    }

    public void UpdateBoxes()
    {
        if (m_move != null)
        {
            HitboxInformation[] info = m_move.HitboxInfo;

            m_transforms = m_previewer.GetTransforms(info);

            //Debug.Log(m_transforms.Length);

            m_information = info;
        }
        else
        {
            m_transforms = null;
        }
    }

    public void OnSceneGUI()
    {
        if(m_move == null)
        {
            return;
        }

        bool hitboxesOpen = HitboxesOpen();

        Color handlesColour = Handles.color;

        for (int i = 0; i < m_transforms.Length; i++)
        {
            Transform current = m_transforms[i];

            if (current == null)
            {
                continue;
            }
            //Debug.Log(current.position, current);
            HitboxInformation info = m_information[i];

            if (info.Type == HitboxInformation.HitboxType.Box)
            {
                DrawBox(current, info, hitboxesOpen);
            }
            else if (info.Type == HitboxInformation.HitboxType.Sphere)
            {
                DrawSphere(current, info, hitboxesOpen);
            }
            //Handles.SphereCap(i, current.position, Quaternion.identity, 20f);
        }

        Handles.color = handlesColour;
    }

    private void DrawBox(Transform current, HitboxInformation info, bool hitBoxesOpen)
    {
        Matrix4x4 transformMatrix = Matrix4x4.TRS(current.position, current.rotation, Vector3.one);

        Matrix4x4 originalMatrix = Handles.matrix;
        Handles.matrix = transformMatrix;

        Vector3 size = info.Dimensions;
        size.Scale(current.lossyScale);

        Vector3 centre = info.Centre;
        centre = current.TransformPoint(centre);
        centre = transformMatrix.inverse.MultiplyPoint(centre);

        if (hitBoxesOpen && AnimationMode.InAnimationMode())
        {
            Handles.color = m_boxEnabledColour;
            //Gizmos.color = m_boxEnabledColour;

            //Gizmos.DrawCube(centre, size);

            DrawSolidCube(centre, size);

            //Handles.CubeCap(0, current.position + info.Centre, )
        }
        else
        {
            Handles.color = m_disabledBoxColour;

            Handles.DrawWireCube(centre, size);
        }

        Handles.matrix = originalMatrix;
    }

    private void DrawSolidCube(Vector3 position, Vector3 size)
    {
        //    Vector3 topLeftBack = new Vector3(size.x / 2, size.y / 2, size.z / 2);
        //    Vector3 topLeftFront = new Vector3(size.x / 2, size.y / 2, - size.z / 2);
        //    Vector3 topRightBack = new Vector3(- size.x / 2, size.y / 2, size.z / 2);
        //    Vector3 topRightFront = new Vector3( - size.x / 2, size.y / 2, - size.z / 2);

        //    Vector3 bottomLeftBack = new Vector3(size.x / 2, - size.y / 2, size.z / 2);
        //    Vector3 bottomLeftFront = new Vector3(size.x / 2, - size.y / 2, - size.z / 2);
        //    Vector3 bottomRightBack = new Vector3(- size.x / 2, - size.y / 2, size.z / 2);
        //    Vector3 bottomRightFront = new Vector3(- size.x / 2, - size.y / 2, - size.z / 2);

        Vector3 topLeftBack = new Vector3(position.x + size.x / 2, position.y + size.y / 2, position.z + size.z / 2);
        Vector3 topLeftFront = new Vector3(position.x + size.x / 2, position.y + size.y / 2, position.z - size.z / 2);
        Vector3 topRightBack = new Vector3(position.x - size.x / 2, position.y + size.y / 2, position.z + size.z / 2);
        Vector3 topRightFront = new Vector3(position.x - size.x / 2, position.y + size.y / 2, position.z - size.z / 2);

        Vector3 bottomLeftBack = new Vector3(position.x + size.x / 2, position.y - size.y / 2, position.z + size.z / 2);
        Vector3 bottomLeftFront = new Vector3(position.x + size.x / 2, position.y - size.y / 2, position.z - size.z / 2);
        Vector3 bottomRightBack = new Vector3(position.x - size.x / 2, position.y - size.y / 2, position.z + size.z / 2);
        Vector3 bottomRightFront = new Vector3(position.x - size.x / 2, position.y - size.y / 2, position.z - size.z / 2);

        //top face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { topRightFront, topRightBack, topLeftBack, topLeftFront }, m_boxEnabledColour, m_boxEnabledColour);

        //bottom face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { bottomRightFront, bottomRightBack, bottomLeftBack, bottomLeftFront }, m_boxEnabledColour, m_boxEnabledColour);

        //left face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { topLeftFront, topLeftBack, bottomLeftBack, bottomLeftFront }, m_boxEnabledColour, m_boxEnabledColour);

        //right face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { topRightFront, topRightBack, bottomRightBack, bottomRightFront }, m_boxEnabledColour, m_boxEnabledColour);

        //front face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { topRightFront, topLeftFront, bottomLeftFront, bottomRightFront }, m_boxEnabledColour, m_boxEnabledColour);

        //back face
        Handles.DrawSolidRectangleWithOutline(new Vector3[] { topRightBack, bottomRightBack, bottomLeftBack, topLeftBack }, m_boxEnabledColour, m_boxEnabledColour);

        //Handles.color = Color.black;

        //Handles.CubeCap(0, topRightFront, Quaternion.identity, 0.05f);

        //Handles.CubeCap(0, bottomRightFront, Quaternion.identity, 0.05f);

        //Handles.color = Color.green;

        //Handles.CubeCap(0, topRightBack, Quaternion.identity, 0.05f);

        //Handles.CubeCap(0, bottomRightBack, Quaternion.identity, 0.05f);

        //Handles.color = Color.red;

        //Handles.CubeCap(0, topLeftBack, Quaternion.identity, 0.05f);

        //Handles.CubeCap(0, bottomLeftBack, Quaternion.identity, 0.05f);

        //Handles.color = Color.blue;

        //Handles.CubeCap(0, topLeftFront, Quaternion.identity, 0.05f);

        //Handles.CubeCap(0, bottomLeftFront, Quaternion.identity, 0.05f);
    }

    private void DrawSphere(Transform current, HitboxInformation info, bool hitboxesOpen)
    {
        //Matrix4x4 transformMatrix = Matrix4x4.TRS(current.position, current.rotation, Vector3.one);

        //Matrix4x4 matrix = Handles.matrix;
        //Handles.matrix = transformMatrix;

        Vector3 centre = info.Centre;
        centre = current.TransformPoint(centre);

        float radius = Mathf.Abs(info.SphereRadius);

        Vector3 lossyScale = current.lossyScale;
        float absX = Mathf.Abs(lossyScale.x);
        float absY = Mathf.Abs(lossyScale.y);
        float absZ = Mathf.Abs(lossyScale.z);

        float scale = Mathf.Max(absX, absY, absZ);

        radius *= scale;

        radius = Mathf.Max(radius, Mathf.Epsilon);

        if (hitboxesOpen && AnimationMode.InAnimationMode())
        {
            Handles.color = m_boxEnabledColour;

            Handles.SphereCap(0, centre, current.rotation, radius * 2);
        }
        else
        {
            Handles.color = m_disabledBoxColour;

            Handles.DrawWireDisc(centre, current.up, radius);

            Handles.DrawWireDisc(centre, current.right, radius);

            Handles.DrawWireDisc(centre, current.forward, radius);
        }

        //Handles.matrix = matrix;
    }

    private bool HitboxesOpen()
    {
        float currentTime = MovePreviewer.instance.CurrentTime;

        int currentFrame = 0;

        if (m_move.Clip != null)
        {
            currentFrame = Mathf.RoundToInt(currentTime * m_move.Clip.frameRate);
        }
        else
        {
            currentFrame = Mathf.RoundToInt(currentTime * 60);
        }

        return m_move.HitboxesOpen(currentFrame);
    }

    public CombatMove Move
    {
        set
        {
            m_move = value;
            UpdateBoxes();
        }
    }
}