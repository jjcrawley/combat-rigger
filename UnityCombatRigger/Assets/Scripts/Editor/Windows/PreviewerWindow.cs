﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PreviewerWindow : EditorWindow
{
    private static PreviewerWindow s_instance;

    [SerializeField]
    private MovePreviewer m_previewer;

    private GUIContent m_headerContent;
        
    private void OnEnable()
    {
        if(s_instance == null)
        {
            s_instance = this;
        }

        if (m_previewer == null)
        {
            m_previewer = MovePreviewer.instance;
            m_previewer.OnOpen();
        }

        titleContent = new GUIContent("Move Previewer");

        m_headerContent = new GUIContent("Preview");
        
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
        
    private void OnDisable()
    {
        //s_instance = null;
        m_previewer.OnDisable();
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.RepaintAll();
    }

    private void OnSelectionChange()
    {
        m_previewer.OnSelectionChange();
        Repaint();
    }

    private void OnSceneGUI(SceneView view)
    {
        if (!EditorApplication.isPlaying)
        {
            m_previewer.OnSceneGUI();
        }
    }
    
    private void OnGUI()
    {       
        EditorGUILayout.LabelField(m_headerContent, EditorStyles.boldLabel);

        m_previewer.OnGUI();
    }

    public static void OpenWindow()
    {
        if(s_instance == null)
        {
            //PreviewerWindow window = CreateInstance<PreviewerWindow>();
            PreviewerWindow window = GetWindow<PreviewerWindow>();

            s_instance = window;

            window.ShowUtility();

            window.m_previewer.OnSelectionChange();
        }    
        else
        {
            s_instance.Focus();
        }    
    }

    public static CombatMove Move
    {
        set
        {
            s_instance.m_previewer.Move = value;
        }
    }
}