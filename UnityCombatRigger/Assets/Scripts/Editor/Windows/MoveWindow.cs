﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

public class MoveWindow : EditorWindow
{
    private static MoveWindow s_instance;

    [SerializeField]
    private CombatMove m_inspectingMove;

    private CombatMoveInspector m_activeEditor;

    private MovePreviewer m_previewer;

    private float m_previewTime;

    private bool m_animating;

    [SerializeField]
    private Vector2 m_editorScroll;

    //private GUISplitter m_splitter;

    private void InitWindow()
    {
        if(m_activeEditor != null)
        {
            m_activeEditor.onRepaint -= Repaint;
            DestroyImmediate(m_activeEditor);
            MovePreviewer.instance.Move = null;
        }

        if (m_inspectingMove != null)
        {
            m_activeEditor = Editor.CreateEditor(m_inspectingMove) as CombatMoveInspector;
            m_activeEditor.m_openedInWindow = true;
            m_activeEditor.onRepaint += Repaint;
            MovePreviewer.instance.Move = m_inspectingMove;
        }
    }

    private void OnEnable()
    {
        MoveLookup.Init();
        m_previewer = MovePreviewer.instance;
        //m_previewer.OnOpen();
        InitWindow();

        //SceneView.onSceneGUIDelegate -= OnSceneGUI;

        //SceneView.onSceneGUIDelegate += OnSceneGUI;

        minSize = new Vector2(500, 300);
        //m_splitter = new GUISplitter();
        //m_splitter.Window = this;
        //m_splitter.MinSplit = 200;
        //m_splitter.Width = 200;
    }   

    private void OnDisable()
    {
        m_activeEditor.onRepaint -= Repaint;

        //SceneView.onSceneGUIDelegate -= OnSceneGUI;

        //SceneView.RepaintAll();

        //m_previewer.OnDisable();

        //Debug.Log("disabled");

        if(m_activeEditor != null)
        {
            DestroyImmediate(m_activeEditor);
        }
    }

    private void OnSelectionChange()
    {
        //Debug.Log("Hello");
        CombatMove selection = Selection.activeObject as CombatMove;

        //Debug.Log(selection.name);

        if(selection != null)
        {
            m_inspectingMove = selection;
            InitWindow();
            Repaint();
        }
    }

    [MenuItem("Window/Move Window")]
    private static void OpenWindow()
    {
        s_instance = GetWindow<MoveWindow>();        
    }

    private void OnGUI()
    {
        if(m_activeEditor != null)
        {
            m_activeEditor.DrawHeader();

            m_editorScroll = EditorGUILayout.BeginScrollView(m_editorScroll);

            EditorGUIUtility.wideMode = true;
            EditorGUI.indentLevel = 0;
            EditorGUIUtility.hierarchyMode = true;
            m_activeEditor.OnInspectorGUI();

            EditorGUIUtility.wideMode = false;
            EditorGUIUtility.hierarchyMode = false;
            EditorGUI.indentLevel = 0;
            EditorGUILayout.EndScrollView();

            //GUILayout.FlexibleSpace();

            //HandlePreview();
        }
    }    

    private void HandlePreview()
    {
        if(m_inspectingMove.Clip != null)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.LabelField("Preview Settings", EditorStyles.boldLabel);
            
            m_previewer.OnGUI();
            
            EditorGUILayout.EndVertical();
        }               
    }

    //private void Update()
    //{
    //    if (focusedWindow == this)
    //    {
    //        m_previewer.Update();
    //    }
    //}   
    
    public static void OpenWindow(CombatMove move)
    {
        OpenWindow();
        s_instance.m_inspectingMove = move;
        s_instance.InitWindow();
    }

    [OnOpenAsset()]
    private static bool ProcessAsset(int instanceID, int line)
    {
        string assetPath = AssetDatabase.GetAssetPath(instanceID);

        CombatMove asset = AssetDatabase.LoadAssetAtPath<CombatMove>(assetPath);
        
        if(asset != null)
        {
            OpenWindow(asset);
            return true;
        }

        return false;             
    }
}