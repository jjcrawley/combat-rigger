﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditorInternal;

using AnimController = UnityEditor.Animations.AnimatorController;

public class ComboBoxWindow : EditorWindow
{
    private class Styles
    {
        public readonly GUIContent m_addButton;

        public readonly GUIContent m_removeButton = new GUIContent(EditorGUIUtility.IconContent("Toolbar Minus", "Remove a combo tree from the combo box"));

        public readonly GUIStyle invisibleButton = "InvisibleButton";

        public readonly GUIContent visibilityOff = EditorGUIUtility.IconContent("animationvisibilitytoggleoff");

        public readonly GUIContent visibilityOn = EditorGUIUtility.IconContent("animationvisibilitytoggleon");
        
        public Styles()
        {
            m_addButton = new GUIContent(EditorGUIUtility.IconContent("Toolbar Plus"));
            m_addButton.tooltip = "Add a combo tree to the combo box";
        }     
    }

    private static ComboBoxWindow s_instance;

    internal static event System.Action onComboBoxDirty;

    [SerializeField]
    private ComboBox m_comboBox;

    [SerializeField]
    private ComboTree m_currentTree;

    private ComboState m_destination;
    private ComboLink m_link;
    
    private ReorderableList m_list;

    private GUISplitter m_splitter = new GUISplitter();

    private Vector2 m_scrollPos = Vector2.zero;
    private Vector2 m_stateScroll = Vector2.zero;

    private Styles m_styles;

    private Undo.UndoRedoCallback m_undoRedo;
    
    private RenameOverlay m_renamer = new RenameOverlay();
    private RenameOverlay m_comboBoxRenamer = new RenameOverlay();
        
    private int m_lastSelectedIndex = -1;

    private ComboBoxInspector m_comboxBoxInspector;

    [SerializeField]
    private bool m_hidden = false;
    
    [SerializeField]
    private ComboTreeGraphGUI m_graphGUI;
        
    [MenuItem("Window/Combo Box")]
    private static void OpenWindow()
    {
        s_instance = GetWindow<ComboBoxWindow>();
        s_instance.titleContent = new GUIContent("Combo Box");
        s_instance.minSize = new Vector2(300, 0);
    }

    private void OnEnable()
    {
        if (s_instance == null)
        {
            s_instance = this;
        }

        SetupWindow();

        if (m_comboBox != null)
        {
            AnimControllerLookup.SetController(m_comboBox.AnimController);
            ButtonLookup.InitLookup(m_comboBox.InputSettings);
        }

        if (m_graphGUI == null)
        {
            m_graphGUI = CreateInstance<ComboTreeGraphGUI>();
            m_graphGUI.hideFlags = HideFlags.HideAndDontSave;
        }

        if (m_comboBox != null)
        {
            m_comboxBoxInspector = Editor.CreateEditor(m_comboBox) as ComboBoxInspector;
        }

        m_undoRedo = new Undo.UndoRedoCallback(UndoAndRedo);
        Undo.undoRedoPerformed += m_undoRedo;

        onComboBoxDirty += OnDirty;

        m_graphGUI.Tree = m_currentTree;

        MoveLookup.Init();

        m_splitter.Space = 5;
        m_splitter.Width = 200;
        m_splitter.MinSplit = 100;
        m_splitter.MaxSplit = 500;
        m_splitter.Window = this;        
    }

    private void OnDisable()
    {
        Undo.undoRedoPerformed -= m_undoRedo;
        onComboBoxDirty -= OnDirty;

        if (m_comboxBoxInspector != null)
        {
            DestroyImmediate(m_comboxBoxInspector);
        }
    }

    private void SetupWindow(ComboBox box)
    {        
        m_comboBox = box;

        if (m_comboxBoxInspector != null)
        {
            DestroyImmediate(m_comboxBoxInspector);
        }

        if (m_comboBox != null)
        {
            m_comboxBoxInspector = Editor.CreateEditor(m_comboBox) as ComboBoxInspector;
        }

        SetupWindow();
    }

    private void SetupWindow()
    {
        SetupList();
    }

    private void SetTree(ComboTree tree)
    {
        if (m_currentTree == tree)
        {
            return;
        }

        m_graphGUI.Tree = tree;

        m_currentTree = tree;

        int index = ArrayUtility.IndexOf(m_comboBox.Trees, tree);

        if (index != -1)
        {
            m_list.index = index;
        }       
    }

    private void SetupList()
    {         
        if (m_list == null)
        {
            m_list = new ReorderableList(null, typeof(ComboTree));

            m_list.drawElementCallback = DrawElement;
            m_list.onSelectCallback = OnSelect;
            m_list.onReorderCallback = OnReorderParameter;
            m_list.drawHeaderCallback = OnListHeader;

            m_list.footerHeight = 0.0f;
            m_list.displayAdd = false;
            m_list.displayRemove = false;
        }

        if (m_comboBox == null)
        {
            return;
        }

        ComboTree[] trees = new ComboTree[m_comboBox.Trees.Length];

        System.Array.Copy(m_comboBox.Trees, trees, m_comboBox.Trees.Length);

        m_list.list = trees;

        if (trees.Length > 0)
        {
            SetTree(trees[0]);
        }
        else
        {
            m_currentTree = null;
        }
    }        

    private void UndoAndRedo()
    {
        SetupList();
        Repaint();
        m_renamer.EndRename(false);
        m_comboBoxRenamer.EndRename(false);
    }

    internal static void OnComboBoxDirty()
    {      
        if (onComboBoxDirty != null)
        {
            onComboBoxDirty();            
        }
    }

    private void OnDirty()
    {
        m_graphGUI.Tree = m_currentTree;
        Repaint();
    }
       
    private void OnSelectionChange()
    {
        m_graphGUI.SyncWithSelection();

        SetupButtonLookup();
                        
        Repaint();
    }
    
    private void SetupButtonLookup()
    {
        GameObject active = Selection.activeGameObject;

        if(active == null)
        {
            return;
        }

        ComboController controller = active.GetComponent<ComboController>();

        if(controller != null)
        {
            IInputSettings handler = controller.InputSettings;

            if (handler != null)
            {
                ButtonLookup.InitLookup(handler);
            }
            else
            {
                handler = active.GetComponent<IInputSettings>();

                if (handler != null)
                {
                    ButtonLookup.InitLookup(handler);
                }
            }
        }
    }

    private void OnProjectChange()
    {
        MoveLookup.Init();
    }

    private void OnGUI()
    {        
        SetupStyles();
                
        EditorGUILayout.BeginHorizontal();

        DoSideView();
                
        DoGraphGUI();

        EditorGUILayout.EndHorizontal();

        if ((Event.current.LeftClick()))
        {
            m_renamer.EndRename(false);
            m_comboBoxRenamer.EndRename(false);
            m_list.ReleaseKeyboardFocus();

            m_graphGUI.ClearSelection();
            m_graphGUI.DoBackgroundClickAction();
            Event.current.Use();
        }
    }

    private void DoGraphGUI()
    {
        if (!m_hidden)
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(position.width - m_splitter.Width));
        }
        else
        {
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true), GUILayout.Width(position.width));
        }

        DoGraphToolbar();

        EditorGUILayout.BeginVertical();

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndVertical();

        Rect graphArea = GUILayoutUtility.GetLastRect();

        EditorGUILayout.EndVertical();
                
        m_graphGUI.BeginDraw(this, graphArea);

        if (m_currentTree != null)
        {
            m_graphGUI.DoGUI();
        }

        m_graphGUI.EndDraw();

        //if(m_comboBox == null)
        //{
        //    Rect buttonPos = new Rect();

        //    Vector2 widthHeight = GUI.skin.button.CalcSize(new GUIContent("Create a Combo box"));

        //    buttonPos.width = widthHeight.x;
        //    buttonPos.height = widthHeight.y;

        //    buttonPos.x = graphArea.center.x - buttonPos.width / 2;
        //    buttonPos.y = graphArea.center.y - buttonPos.height / 2;

        //    if(GUI.Button(buttonPos, "Create a combo box"))
        //    {

        //    }
        //}
        //else if (m_currentTree == null)
        //{
        //    Rect buttonPos = new Rect();

        //    Vector2 widthHeight = GUI.skin.button.CalcSize(new GUIContent("Create a Combo tree"));

        //    buttonPos.width = widthHeight.x;
        //    buttonPos.height = widthHeight.y;

        //    buttonPos.x = graphArea.center.x - buttonPos.width / 2;
        //    buttonPos.y = graphArea.center.y - buttonPos.height / 2;

        //    if (GUI.Button(buttonPos, "Create a Combo Tree"))
        //    {
        //        OnAdd();
        //    }
        //}
    }

    private void DoGraphToolbar()
    {       
        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.ExpandWidth(false), GUILayout.Height(20f), GUILayout.ExpandHeight(false));
                
        if(m_hidden)
        {            
            if(GUILayout.Button(m_styles.visibilityOff, m_styles.invisibleButton))
            {
                m_hidden = false;
            }

            GUILayout.Space(10);
        }

        if(m_currentTree != null)
        {
            EditorGUILayout.LabelField(m_currentTree.name, EditorStyles.miniLabel);
        }

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndHorizontal();
    }

    private void DoSideView()
    {
        if (!m_hidden)
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(m_splitter.Width), GUILayout.ExpandWidth(false));
                        
            DoSideViewToolbar();
                        
            DoComboBoxGUI();

            DoTreeList();

            DoPreviewGUI();

            EditorGUILayout.EndVertical();

            Event current = Event.current;

            if (current.type != EventType.Layout)
            {
                Rect area = GUILayoutUtility.GetLastRect();

                Rect resizeArea = new Rect(area.xMax - 2, area.yMin, 0, area.height);

                m_splitter.Width = area.width;

                m_splitter.DoSplit(resizeArea);
            }
        }               
    }

    private void DoSideViewToolbar()
    {        
        GUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.Height(18f), GUILayout.ExpandWidth(false));
                
        GUILayout.FlexibleSpace();

        if(GUILayout.Button(m_styles.visibilityOn, m_styles.invisibleButton))
        {
            m_hidden = true;
        }        
                        
        GUILayout.EndHorizontal();
    }

    private void DoComboBoxGUI()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);

        if (m_comboxBoxInspector != null)
        {
            Rect position = GUILayoutUtility.GetRect(0, 18f, GUILayout.ExpandWidth(true));

            if(!m_comboBoxRenamer.RenamingGUI(position))
            {
                if (m_comboBoxRenamer.Accepted)
                {
                    Undo.RecordObject(m_comboBox, "Renamed Box");
                    
                    string path = AssetDatabase.GetAssetPath(m_comboBox);

                    AssetDatabase.RenameAsset(path, m_comboBoxRenamer.EditedString);
                }
            }           
            else if(!m_comboBoxRenamer.Renaming)
            {
                EditorGUI.LabelField(position, m_comboBox.name);

                Rect clickPos = GUILayoutUtility.GetLastRect();

                Event current = Event.current;
                                
                if(current.clickCount == 2 && current.InRect(clickPos) && current.type == EventType.MouseDown && current.button == 0)
                {
                    m_comboBoxRenamer.BeginRename(m_comboBox.name);
                    m_renamer.EndRename(false);
                    current.Use();
                }
            }
            
            float labelWidth = EditorGUIUtility.labelWidth;

            EditorGUIUtility.labelWidth = 100;

            m_comboxBoxInspector.OnInspectorGUI();

            EditorGUIUtility.labelWidth = labelWidth;
        }
        else
        {
            EditorGUILayout.LabelField("No combo box");

            if(GUILayout.Button("Create new Combo box"))
            {
                string path = EditorUtility.SaveFilePanelInProject("Create Combo Box", "Combo Box", "asset", "Create a new combo box");

                if (!string.IsNullOrEmpty(path))
                {
                    ComboBox box = ComboBox.CreateComboBoxAtPath(path);

                    SetupWindow(box);
                }
            }

            GUILayout.Space(20);
        }

        EditorGUILayout.EndVertical();            
    }

    private void DoTreeList()
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);        

        m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);

        if (m_comboBox != null)
        {
            m_list.DoLayoutList();
        }

        EditorGUILayout.EndScrollView();

        GUILayout.FlexibleSpace();

        EditorGUILayout.EndVertical();

        Event current = Event.current;

        if (m_list.HasKeyboardControl())
        {
            if (current.type == EventType.KeyDown && m_lastSelectedIndex != -1)
            {
                if (current.keyCode == KeyCode.Delete)
                {
                    OnRemove(m_lastSelectedIndex);
                    current.Use();
                }
            }
        }
    }

    private void DoPreviewGUI()
    {
        if(GUILayout.Button("Open previewer"))
        {
            PreviewerWindow.OpenWindow();
        }

        GUILayout.Space(4);
    }  
    
    #region DebugStuff
    private void DoStates()
    {       
        EditorGUILayout.BeginVertical();

        if(m_currentTree == null)
        {            
            EditorGUILayout.EndVertical();
            return;
        }

        ComboState[] states = m_currentTree.States;

        DrawStart();

        EditorGUILayout.LabelField("States");

        m_stateScroll = EditorGUILayout.BeginScrollView(m_stateScroll, EditorStyles.helpBox);

        ComboState currentState;

        for(int i = 0; i < states.Length; i++)
        {
            currentState = states[i];

            if (currentState == null)
            {
                continue;
            }
            
            EditorGUILayout.BeginVertical(GUI.skin.box);
                        
            EditorGUILayout.BeginVertical(m_destination == currentState ? EditorStyles.helpBox : EditorStyles.label);

            if (currentState.Move == null)
            {
                EditorGUILayout.LabelField("No move");
            }
            else
            {
                EditorGUILayout.LabelField(currentState.Move.name);
            }

            EditorGUILayout.EndVertical();

            Rect checkRect = GUILayoutUtility.GetLastRect();

            //Debug.Log(i);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add link"))
            {
                if (m_destination != null)
                {
                    currentState.AddPlayerLink(m_destination);
                    onComboBoxDirty();
                }
            }

            if (GUILayout.Button("Delete State"))
            {
                Selection.activeObject = null;

                m_currentTree.RemoveState(i);
                onComboBoxDirty();
            }            

            EditorGUILayout.EndHorizontal();

            DrawLinks(currentState);            
            
            if(Event.current.InRect(checkRect) && Event.current.LeftClick())
            {
                Selection.activeObject = currentState;
                m_destination = currentState;
                //Debug.Log("clicked");
            }        

            EditorGUILayout.Space();

            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndScrollView();  

        if(GUILayout.Button(new GUIContent("Add state")))
        {
            ComboState state = m_currentTree.AddState();

            Selection.activeObject = state;
            onComboBoxDirty();                          
        }

        EditorGUILayout.EndVertical();
    }

    private void DrawLinks(ComboState state)
    {
        ComboLink[] links = state.Links;

        for(int i = 0; i < links.Length; i++)
        {
            ComboLink link = links[i];
            
            EditorGUILayout.BeginHorizontal();

            if(link == null)
            {
                EditorGUILayout.EndHorizontal();
                Debug.Log("Null links: " + links.Length);
                return;
            }
            
            if(link.Destination == null)
            {
                EditorGUILayout.LabelField("Empty move");
            }
            else
            {
                GUIStyle style = m_link == link ? EditorStyles.helpBox : EditorStyles.label;
     
                if(link.Destination.Move != null)
                {
                    EditorGUILayout.LabelField("Link destination: " + link.Destination.Move.name, style);
                }
                else
                {
                    EditorGUILayout.LabelField("No move");
                }
            }

            Rect lastRect = GUILayoutUtility.GetLastRect();

            if(Event.current.InRect(lastRect) && Event.current.LeftClick())
            {
                Selection.activeObject = link;
                m_link = link;
            }            

            if(GUILayout.Button("Delete link"))
            {
                state.RemoveLink(i);
            }

            EditorGUILayout.EndHorizontal();
        }       
    }    

    private void DrawStart()
    {        
        EditorGUILayout.LabelField(new GUIContent("Starting point"));

        EditorGUILayout.BeginVertical(GUI.skin.box);

        EditorGUILayout.Space();     
          
        if (GUILayout.Button("Add link"))
        {
            if (m_destination != null)
            {
                m_currentTree.AddEntryLink(m_destination);
                onComboBoxDirty();
            }
        }

        ComboLink[] links = m_currentTree.EntryLinks;

        //EditorGUILayout.BeginVertical();

        for (int i = 0; i < links.Length; i++)
        {
            EditorGUILayout.BeginHorizontal();

            if (links[i] == null)
            {
                EditorGUILayout.EndHorizontal();
                continue;
            }

            GUIStyle style = m_link == links[i] ? EditorStyles.helpBox : EditorStyles.label;

            if (links[i].Destination.Move != null)
            {
                EditorGUILayout.LabelField(links[i].Destination.Move.name, style);
            }
            else
            {
                EditorGUILayout.LabelField("empty");
            }
            Rect label = GUILayoutUtility.GetLastRect();

            Event current = Event.current;

            if(current.InRect(label) && current.LeftClick())
            {
                Selection.activeObject = links[i];
                m_link = links[i];
            }

            if (GUILayout.Button("Delete link"))
            {
                m_currentTree.RemoveEntryLink(i);
                onComboBoxDirty();
            }

            EditorGUILayout.EndHorizontal();
        }

        //EditorGUILayout.EndVertical();

        //if (GUILayout.Button("Clear links"))
        //{
        //    for (int i = m_currentTree.EntryLinks.Length - 1; i >= 0; i--)
        //    {
        //        m_currentTree.RemoveEntryLink(i);
        //    }
        //}

        EditorGUILayout.Space();

        EditorGUILayout.EndVertical();
    }
    #endregion

    private void OnReorderParameter(ReorderableList list)
    {        
        ComboTree[] otherTrees = list.list as ComboTree[];        

        m_comboBox.ReorderTrees(otherTrees);        
    }

    private void DrawElement(Rect rect, int index, bool selected, bool focused)
    {
        ComboTree tree = m_comboBox.Trees[index];        
        
        if (m_renamer.Renaming && index == m_lastSelectedIndex)
        {
            if (!m_renamer.RenamingGUI(rect))
            {
                Undo.RecordObject(tree, "Tree name changed");

                tree.name = m_renamer.EditedString;
                tree.name = m_comboBox.GetUniqueTreeName(tree);
                //m_renamer.ApplyRenameWithUndo(tree, "Tree name changed");
            }

            if(Event.current.type == EventType.ScrollWheel)
            {
                Event.current.Use();
            }
        }
        else
        {
            EditorGUI.LabelField(rect, new GUIContent(tree.name));
        }

        Event current = Event.current;
        
        if (current.type == EventType.ContextClick && current.InRect(rect))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Remove"), false, () => { OnRemove(index); });
            menu.ShowAsContext();
            Event.current.Use();                                    
        }                                            
    }

    private void OnSelect(ReorderableList list)
    {
        int index = list.index;
        
        ComboTree current = list.list[index] as ComboTree;

        Selection.activeObject = current;

        SetTree(current);
                
        if (Event.current.button == 0 && Event.current.clickCount == 2 && m_lastSelectedIndex == index)
        {
            m_renamer.BeginRename(current.name);
            m_comboBoxRenamer.EndRename(false);
            m_lastSelectedIndex = -1;
            m_list.ReleaseKeyboardFocus();
        }
        
        m_lastSelectedIndex = index;         
    }

    private void OnAdd()
    {
        if (m_comboBox != null)
        {
            ComboTree tree = m_comboBox.AddTree();
            //Selection.activeObject = tree;
            SetupList();

            SetTree(tree);
        }
    }

    private void OnRemove(int index)
    {        
        //Selection.activeObject = null;
        m_comboBox.RemoveTree(index);

        SetupList();
        
        m_list.ReleaseKeyboardFocus();
        m_list.index = -1;

        if(m_list.count != 0)
        {
            SetTree(m_comboBox[0]);
        }
        else
        {
            SetTree(null);
        }

        Repaint();
    }

    private void OnListHeader(Rect rect)
    {
        Rect titlePos = rect;
        titlePos.width = 200;
        titlePos.x += 10;

        EditorGUI.LabelField(titlePos, "Combo Trees");
                
        Rect addPos = rect;

        addPos.width = 20;
        addPos.x = rect.xMax - addPos.width;

        if (GUI.Button(addPos, m_styles.m_addButton, m_styles.invisibleButton))
        {
            OnAdd();
        }               
    }    

    private void SetupStyles()
    {
        if(m_styles == null)
        {
            m_styles = new Styles();
            m_styles.m_addButton.tooltip = "Add a new tree to the combo box";
        }
    }
    
    [OnOpenAsset()]
    private static bool OpenAsset(int instanceID, int line)
    {
        string path = AssetDatabase.GetAssetPath(instanceID);

        ComboBox current = AssetDatabase.LoadAssetAtPath<ComboBox>(path);

        if(current != null)
        {
            OpenWindow();
            s_instance.SetupWindow(current);
            AnimControllerLookup.SetController(current.AnimController);
            ButtonLookup.InitLookup(current.InputSettings);

            return true;
        }

        return false;                        
    }  

    internal static ComboState[] States
    {
        get
        {
            ComboState[] states = new ComboState[s_instance.m_currentTree.States.Length];

            System.Array.Copy(s_instance.m_currentTree.States, states, states.Length);

            return states;
        }
    }

    internal static AnimController SyncController
    {
        get
        {
            return s_instance.m_comboBox.AnimController;
        }
    }
    
    internal static ComboBox EditingComboBox
    {
        get
        {
            return s_instance.m_comboBox;
        }
    }      

    internal static ComboTree EditingComboTree
    {
        get
        {
            return s_instance.m_currentTree;
        }
    }
}