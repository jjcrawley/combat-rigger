﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class AnimatorParam
{
    [SerializeField]
    private string m_name;

    [SerializeField]
    private float m_defaultFloat;

    [SerializeField]
    private int m_defaultInt;

    [SerializeField]
    private bool b_defaultBool;
    
    [SerializeField]
    private AnimatorControllerParameterType m_type;
       
    private int m_nameHash;

    internal void Setup()
    {
        m_nameHash = Animator.StringToHash(m_name);
    }

    internal void Setup(Animator animator)
    {
        //Debug.Log(m_nameHash);
        //Debug.Log(m_name);
        switch (m_type)
        {
            case AnimatorControllerParameterType.Float:
                animator.SetFloat(m_nameHash, m_defaultFloat);
                break;
            case AnimatorControllerParameterType.Int:
                animator.SetInteger(m_nameHash, m_defaultInt);
                break;
            case AnimatorControllerParameterType.Bool:
                animator.SetBool(m_nameHash, b_defaultBool);
                break;
            case AnimatorControllerParameterType.Trigger:
                animator.SetTrigger(m_nameHash);
                break;
            default:
                break;
        }
    }
           
    public string ParameterName
    {
        set
        {
            m_name = value;
            m_nameHash = Animator.StringToHash(m_name);
        }
        get
        {
            return m_name;
        }
    }       

    public float FloatValue
    {
        set
        {
            m_defaultFloat = value;
        }
        get
        {
            return m_defaultFloat;
        }
    }

    public int IntValue
    {
        set
        {
            m_defaultInt = value;
        }
        get
        {
            return m_defaultInt;
        }
    }

    public AnimatorControllerParameterType Type
    {
        set
        {
            m_type = value;
        }
        get
        {
            return m_type;
        }
    }

    public int NameHash
    {
        get
        {
            return m_nameHash;
        }
    }
}