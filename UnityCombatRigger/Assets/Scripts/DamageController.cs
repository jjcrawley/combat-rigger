﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

internal class DamageController
{
    private List<DamageFireData> m_updateTimers = new List<DamageFireData>();

    private Dictionary<ICombatEntity, DamageFireData> m_updateLookup = new Dictionary<ICombatEntity, DamageFireData>();

    private HitboxController m_hitboxController;

    private ComboController m_comboController;

    private Coroutine m_updateRoutine;

    public DamageController(ComboController controller, HitboxController hitboxController)
    {
        m_comboController = controller;
        m_hitboxController = hitboxController;
    }

    public IEnumerator UpdateTimers()
    {
        yield return null;

        while (m_updateTimers.Count != 0)
        {
            for (int i = m_updateTimers.Count - 1; i > -1; i--)
            {
                m_updateTimers[i].Update();
            }

            yield return null;
        }
    }     

    public void ClearTimers()
    {
        m_updateLookup.Clear();
        m_updateTimers.Clear();

        if(m_updateRoutine != null)
        {
            m_comboController.StopCoroutine(m_updateRoutine);
        }
    }

    public void RegisterFire(float interval, HitInfo info)
    {
        DamageFireData data = new DamageFireData(interval, info, this);
        
        if (m_updateTimers.Count == 0)
        {
            //Debug.Log("Starting");
            m_updateRoutine = m_comboController.StartCoroutine(UpdateTimers());
        }

        m_updateTimers.Add(data);
        m_updateLookup.Add(info.hitEntity, data);
    }

    private class DamageFireData
    {
        private float m_reactivateTime;
        private float m_currentTime;

        private HitInfo m_info;

        private DamageController m_controller;

        public DamageFireData(float damageInterval, HitInfo info, DamageController controller)
        {
            m_reactivateTime = damageInterval;
            m_info = info;
            m_controller = controller;
        }

        public void Update()
        {
            m_currentTime += Time.deltaTime;

            if (m_controller.m_hitboxController.StillInContact(m_info.hitEntity))
            {
                if (m_currentTime > m_reactivateTime)
                {
                    //Debug.Log("hit a thing again " + (m_info.hitEntity as Component).gameObject.name + " " + Time.frameCount, (m_info.hitEntity as Component).gameObject);
                    m_info.senderController.OnContinuousHit(m_info);
                    m_currentTime = 0;
                }
            }
            else
            {
                //Debug.Log("Removing timer");
                m_controller.m_updateTimers.Remove(this);
                m_controller.m_updateLookup.Remove(m_info.hitEntity);
            }
        }
    }
}