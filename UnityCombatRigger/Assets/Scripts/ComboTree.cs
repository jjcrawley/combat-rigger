﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public sealed class ComboTree : ScriptableObject
{
    [SerializeField, HideInInspector]
    private ComboState[] m_states;
            
    private ComboState m_startState;

    [SerializeField, HideInInspector, Tooltip("The reset transition for the tree, when the controller triggers a reset, this is the transition that is used")]
    private ComboLink m_resetTransition;

    [SerializeField, HideInInspector]
    private ComboLink[] m_entryLinks;   

    private Interop.SimpleCallback m_resetCallback;
    
    private void OnEnable()
    {
        if(m_states == null)
        {
            m_states = new ComboState[0];
        }

        if (m_startState == null)
        {
            m_startState = CreateInstance<EntryState>();
        }

        if(m_entryLinks == null)
        {
            m_entryLinks = new ComboLink[0];
        }
    }

#if UNITY_EDITOR
    internal void Setup()
    {
        if (m_resetTransition == null)
        {
            m_resetTransition = CreateInstance<ResetLink>();
            m_resetTransition.hideFlags = HideFlags.HideInHierarchy;
            m_resetTransition.Destination = m_startState;

            m_resetTransition.AddParam(AnimatorControllerParameterType.Trigger, "Reset");

            Undo.RegisterCompleteObjectUndo(this, "Added tree");
            Undo.RegisterCreatedObjectUndo(m_resetTransition, "Added tree");
        }

        if (AssetDatabase.Contains(this) && !AssetDatabase.Contains(m_resetTransition))
        {
            AssetDatabase.AddObjectToAsset(m_resetTransition, this);
        }
    }
#endif

    public IntPtr BuildBackend(IntPtr comboBox)
    {
        IntPtr tree = Interop.CreateComboTree(comboBox);

        IntPtr resetLink = Interop.GetResetTransition(tree);

        if(m_resetCallback == null)
        {
            m_resetCallback = new Interop.SimpleCallback(ApplyReset);
        }
                    
        Interop.SetLinkCallback(resetLink, m_resetCallback);

        Interop.ObjectOnCreate(resetLink);

        for(int i = 0; i < m_states.Length; i++)
        {
            IntPtr statePointer = m_states[i].GetBackend(tree);

            if (statePointer == IntPtr.Zero)
            {
                continue;
            }            
        }
      
        for(int i = 0; i < m_states.Length; i++)
        {
            m_states[i].CreateLinkBackend();
        }

        for (int i = 0; i < m_entryLinks.Length; i++)
        {
            IntPtr connected = m_entryLinks[i].Destination.tempPointer;

            if(connected == IntPtr.Zero)
            {
                continue;
            }
                  
            IntPtr link = Interop.AddEntryTransition(tree, connected);

            m_entryLinks[i].CreateBackend(link);
        }

        Interop.ObjectOnCreate(tree);
       
        return tree;
    }

    internal MoveModifier[] AllMods
    {
        get
        {
            List<MoveModifier> mods = new List<MoveModifier>();

            //for(int i = 0; i < m_states.Length; i++)
            //{
            //    mods.AddRange(m_states[i].GetMods());
            //}

            return mods.ToArray(); 
        }
    }

    internal ComboLock[] AllLocks
    {
        get
        {
            List<ComboLock> locks = new List<ComboLock>();

            for(int i = 0; i < m_states.Length; i++)
            {
                locks.AddRange(m_states[i].GetLocks());                
            }

            for (int i = 0; i < m_entryLinks.Length; i++)
            {
                locks.AddRange(m_entryLinks[i].CreateLockBackend());
            }
            
            return locks.ToArray();
        }
    }

    public ComboState[] States
    {
        get
        {
            return m_states;
        }
    }

    public ComboLink ResetTransition
    {
        get
        {
            return m_resetTransition;
        }
    }

    public ComboState Start
    {
        get
        {
#if UNITY_EDITOR
            SetupStart();
#endif
            return m_startState;
        }
    }

    public void Init(ComboController controller)
    {
        for (int i = 0; i < m_states.Length; i++)
        {
            m_states[i].Init(controller);
        }
    }

    public void ApplyReset()
    {
        m_resetTransition.UpdateParams(ComboController.s_lastAnimator);
    }

    public ComboState ApplyReset(Animator animator)
    {
        m_resetTransition.UpdateParams(animator);
        //Debug.Log("applying transition");
        return m_startState;
    }

#if UNITY_EDITOR

    [SerializeField, HideInInspector]
    public ComboState.EditorData entryNodeData = new ComboState.EditorData();

    public ComboState AddMove(CombatMove move)
    {
        ComboState moveState = AddState();
        
        Undo.RecordObject(moveState, "Added state");

        moveState.Move = move;
        moveState.ComboName = move.name;

        return moveState;
    }

    public ComboState AddState()
    {
        ComboState state = CreateInstance<ComboState>();
        state.hideFlags = HideFlags.HideInHierarchy;
        state.ComboName = "New combo";

        Undo.RegisterCreatedObjectUndo(state, "Added state");

        AddState(state);

        return state;
    }
    
    private void AddState(ComboState state)
    {
        Undo.RegisterCompleteObjectUndo(this, "Added state");

        if (AssetDatabase.Contains(this))
        {            
            AssetDatabase.AddObjectToAsset(state, this);            
        }

        EditorUtility.SetDirty(this);

        if(!ArrayUtility.Contains(m_states, state))
        {
            ArrayUtility.Add(ref m_states, state);           
        }
    }   
    
    public void RemoveState(int index)
    {
        RemoveComboState(m_states[index]);        
    } 

    private void RemoveComboState(ComboState state)
    {
        EditorUtility.SetDirty(this);
        Undo.RegisterCompleteObjectUndo(this, "Removed state");

        if(ArrayUtility.Contains(m_states, state))
        {
            ArrayUtility.Remove(ref m_states, state);
        }

        for(int i = 0; i < m_states.Length; i++)
        {
            m_states[i].RemoveLinks(state);
        }

        RemoveEntryLink(state);       

        state.DestroyState();

        Undo.DestroyObjectImmediate(state);
    }  
    
    public void DestroyTree()
    {
        //Undo.RegisterCompleteObjectUndo(this, "Removed Tree");

        for(int i = 0; i < m_entryLinks.Length; i++)
        {
            m_entryLinks[i].DestroyLink();

            Undo.DestroyObjectImmediate(m_entryLinks[i]);
        }

        for (int i = 0; i < m_states.Length; i++)
        {                       
            m_states[i].DestroyState();

            Undo.DestroyObjectImmediate(m_states[i]);
        }

        Undo.DestroyObjectImmediate(m_resetTransition);
    }

    public void RemoveEntryLink(ComboState state)
    {
        for (int i = 0; i < m_entryLinks.Length; i++)
        {
            if (m_entryLinks[i].Destination == state)
            {
                RemoveEntryLink(m_entryLinks[i]);
                break;
            }
        }        
    }
        
    public void RemoveEntryLink(int index)
    {
        ComboLink link = m_entryLinks[index];

        RemoveEntryLink(link);                
    }
    
    private void RemoveEntryLink(ComboLink link)
    {
        EditorUtility.SetDirty(this);
        Undo.RegisterCompleteObjectUndo(this, "Remove Entry link");
        //Undo.RecordObject(link, "Remove entry link");

        ArrayUtility.Remove(ref m_entryLinks, link);

        Undo.DestroyObjectImmediate(link);

        link.DestroyLink();        
    }

    public ComboLink AddEntryLink(ComboState state)
    {
        if(IsConnected(state))
        {
            return null;
        }

        ComboLink link = CreateInstance<BasePlayerLink>();

        link.hideFlags = HideFlags.HideInHierarchy;
        link.Destination = state;
        //link.name = "Entry link";

        if(AssetDatabase.Contains(this))
        {
            EditorUtility.SetDirty(this);
            Undo.RegisterCreatedObjectUndo(link, "Add entry link");
            Undo.RegisterCompleteObjectUndo(this, "Add entry link");           

            AssetDatabase.AddObjectToAsset(link, this);

            ArrayUtility.Add(ref m_entryLinks, link);
        }

        if (m_startState != null)
        {
            m_startState.AddLink(link);
        }

        return link;        
    }

    private bool IsConnected(ComboState destination)
    {
        if(destination == null)
        {
            return true;
        }

        for(int i = 0; i < m_entryLinks.Length; i++)
        {
            if(m_entryLinks[i].Destination == destination)
            {
                return true;
            }
        }

        return false;
    }

    public void ReorderEntryLinks(ComboLink[] links)
    {
        Undo.RegisterCompleteObjectUndo(this, "Reordered entry links");

        Array.Copy(links, m_entryLinks, links.Length);
    }

    private bool ConnectedToStart(ComboState state)
    {
        for (int i = 0; i < m_entryLinks.Length; i++)
        {
            if (m_entryLinks[i].Destination == state)
            {
                return true;
            }
        }

        return false;
    }

    private void SetupStart()
    {
        if (m_startState == null)
        {
            m_startState = CreateInstance<EntryState>();
            m_startState.name = "Start";
        }

        EntryState state = m_startState as EntryState;

        state.Tree = this;
        state.ResetLinks(m_entryLinks);
    }

#endif    

    public ComboLink[] EntryLinks
    {
        get
        {
            return m_entryLinks;
        }
    }

    public ComboState this[int index]
    {
        get
        {
            return m_states[index];
        }
    }
}