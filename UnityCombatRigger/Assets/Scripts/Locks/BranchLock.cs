﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchLock : ComboLock
{
    public bool unlocked = false;

    public override bool Unlock(float normal, ComboController handler)
    {
        return unlocked;
    }
}