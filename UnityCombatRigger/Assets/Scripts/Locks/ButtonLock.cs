﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLock : ComboLock
{
    [SerializeField]
    private string m_buttonName;

    public override bool Unlock(float normal, ComboController handler)
    {
        if (!string.IsNullOrEmpty(m_buttonName))
        {                           
            return handler.CurrentInputHandler.ButtonDown(m_buttonName);
        }

        return true;
    }

    public string ButtonName
    {
        set
        {
            m_buttonName = value;
        }
        get
        {
            return m_buttonName;
        }
    }
}