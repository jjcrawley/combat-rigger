﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitLock : ComboLock
{
    private Period m_waitPeriod;

    public override bool Unlock(float normal, ComboController handler)
    {
        //Debug.Log("Quick");
        return m_waitPeriod.InRange(normal);
    }

    public Period WaitPeriod
    {
        set
        {
            m_waitPeriod = value;            
        }
        get
        {
            return m_waitPeriod;
        }
    }
}