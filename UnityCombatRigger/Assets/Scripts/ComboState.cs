﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ComboState : ScriptableObject
{
    [SerializeField, Tooltip("The move information for the combo state, required")]
    private CombatMove m_moveInfo;       

    private IInputHandler m_handler = null;

    [SerializeField]
    protected ComboLink[] m_links;
    //private List<ComboLink> m_links;

    [SerializeField, Tooltip("The name of the combo")]
    private string m_comboName;

    [SerializeField, Tooltip("The reset frame for the state, after this frame is reached the controller will reset, provided a move wasn't triggered")]
    private float m_resetFrame = 0;

    private float m_resetFrameNormal;
        
    private bool b_hitboxesActive;
        
    private List<BaseHitbox> m_damageBoxes;

    private bool b_usedHitboxes;

    internal IntPtr tempPointer;

    private StateInfo m_info;
             
    private void OnEnable()
    {
        if (m_links == null)
        {
            m_links = new ComboLink[0];
        }              

        name = m_comboName;

        //UpdateMods();     
    }

    internal virtual void Init(ComboController controller)
    {
        //CombatMoveLookup.AddMove(m_moveInfo);

        for (int i = 0; i < m_links.Length; i++)
        {
            m_links[i].Init();
        }

        if (controller.HitboxRig != null)
        {
            m_damageBoxes = m_moveInfo.BuildHitboxSetup(controller.HitboxRig, controller);
        }

        m_resetFrameNormal = m_resetFrame / m_moveInfo.FrameCount;
                
        m_info = new StateInfo();

        if (m_moveInfo != null)
        {
            m_info.moveID = m_moveInfo.GetInstanceID();
            m_moveInfo.Init(controller);
        }

        //Debug.Log(m_moveInfo.name + ", " + m_resetFrameNormal);
    }

    internal virtual IntPtr GetBackend(IntPtr tree)
    {        
        if(m_moveInfo == null)
        {
            return IntPtr.Zero;
        }

        tempPointer = Interop.CreateCombatState(tree);

        //Debug.Log(m_moveInfo.name);

        Interop.AssignComboMove(tempPointer, m_moveInfo.CreateBackend());

        Interop.SetResetFrame(tempPointer, m_resetFrame);

        Interop.ObjectOnCreate(tempPointer);

        return tempPointer;
    }

    internal virtual void CreateLinkBackend()
    {
        for(int i = 0; i < m_links.Length; i++)
        {
            m_links[i].CreateBackend(tempPointer, m_links[i].Destination.tempPointer);
        }                
    }

    internal MoveModifier[] GetMods()
    {
        if(m_moveInfo == null)
        {
            return new MoveModifier[0];
        }

        MoveModifier[] mods = m_moveInfo.GetMods();
        
        //for(int i = 0; i < mods.Length; i++)
        //{
        //    mods[i].GetBackend(tempPointer);            
        //}

        return mods;
    }

    internal List<ComboLock> GetLocks()
    {
        List<ComboLock> locks = new List<ComboLock>();
        
        if (m_moveInfo == null)
        {
            return locks;
        }

        for (int i = 0; i < m_links.Length; i++)
        {
            locks.AddRange(m_links[i].CreateLockBackend());
        }

        return locks;     
    }
        
    const string DebugName = "Rising_P";

    public virtual void UpdateState(float normalTime, ComboController controller)
    {        
        UpdateMods(normalTime);       
         
        if(!b_usedHitboxes)
        {
            //if (m_moveInfo != null && m_moveInfo.name == DebugName)
            //{
            //    Debug.Log("Normal time " + normalTime);
            //}

            ProcessHitboxes(normalTime, controller.HitboxRig);
        }

        //m_moveInfo.HandleHitboxes(normalTime, controller.HitboxRig);

        if(controller.TransitionPending)
        {
            //Debug.Log("transition is pending");
            return;
        }

        if (normalTime >= m_resetFrameNormal)
        {
            //Debug.Log(m_moveInfo.name + ", " + normalTime);
            controller.ResetController();            
        }
        else
        {
            EvaluateTransitions(normalTime, controller);
        }
    } 

    public void UpdateMods(float normalTime)
    {
        m_moveInfo.OnUpdate(normalTime, ComboController.s_modifierInfo);
    }

    protected void EvaluateTransitions(float normalTime, ComboController controller)
    {
        ComboLink link = null;

        for (int i = 0; i < m_links.Length; i++)
        {
            link = m_links[i];

            if (link.Transition(normalTime, controller))
            {
                link.BeginTransition(controller);
                break;
            }
        }
    }   

    public virtual void OnEnter()
    {
        m_moveInfo.OnEnter(ComboController.s_modifierInfo);

        b_usedHitboxes = false;
        //b_fired = false;
    }

    public virtual void OnExit()
    {
        m_moveInfo.OnExit(ComboController.s_modifierInfo);       
    }

    public virtual void ProcessHit(HitInfo info)
    {
        //Debug.Log("I hit a thing in combo state");

        m_moveInfo.ProcessHit(info);
        
        //Debug.Log(this.GetType().FullName);

        //m_moveInfo.ProcessDamage(info);

        //info.currentMove = m_moveInfo;

        if(info.move != null)
        {
            info.move.ProcessHit(info);
        }

        HitboxController rig = info.senderController.HitboxRig;

        if (rig != null)
        {
            if(m_moveInfo.Continuous)
            {
                info.senderController.damageController.RegisterFire(m_moveInfo.DamageInterval, info);
                //b_hitboxesActive = false;
                //rig.DisableDamageBoxes(m_moveInfo);
                //Debug.Log("deactivated move");
                //m_currentHitboxMethod = ProcessEmpty;
            }
            else
            {
                //rig.DisableDamageBoxes(m_moveInfo);  
            }
        }
    }

    internal virtual void ProcessHit(out StateInfo moveInfo)
    {
        moveInfo = new StateInfo();

        if (m_moveInfo != null)
        {
            moveInfo.moveID = m_moveInfo.GetInstanceID();
        }
        else
        {
            moveInfo.moveID = 0;
        }
    }
    
    public virtual void ProcessContinuousHit(HitInfo info)
    {
        info.move.ProcessHit(info);
        //Debug.Log("hit a thing again " + (info.hitEntity as Component).gameObject.name, (info.hitEntity as Component).gameObject);
        //m_moveInfo.ProcessDamage(info);
    }   

    private void ProcessHitboxes(float normalTime, HitboxController rig)
    {
        bool hitBoxesOpen = m_moveInfo.HitboxesOpen(normalTime);

        if(hitBoxesOpen && !b_hitboxesActive)
        {
            b_hitboxesActive = true;
            rig.ActivateDamageBoxes(m_moveInfo);
        }
        else if(!hitBoxesOpen && b_hitboxesActive)
        {            
            b_hitboxesActive = false;
            b_usedHitboxes = true;
            rig.DisableDamageBoxes(m_moveInfo);
        }   
    }
    
    public StateInfo StateInfo
    {
        get
        {
            return m_info;
        }
    }

#if UNITY_EDITOR

    [HideInInspector]
    public EditorData data = new EditorData();   
       
    public PlayerComboLink AddPlayerLink(ComboState destination)
    {
        if(IsConnected(destination))
        {
            return null;
        }

        PlayerComboLink link = CreateInstance<PlayerComboLink>();

        AddLink(link, destination);

        return link;                
    } 
    
    private void AddLink(ComboLink link, ComboState destination)
    {
        link.Destination = destination;
        link.hideFlags = HideFlags.HideInHierarchy;
        link.Origin = this;

        Undo.RegisterCreatedObjectUndo(link, "Added link");
        Undo.RegisterCompleteObjectUndo(this, "Added link");

        if (AssetDatabase.Contains(this))
        {
            AssetDatabase.AddObjectToAsset(link, this);
        }

        AddLink(link);
    }

    public void AddLink(ComboLink link)
    {
        if (!ArrayUtility.Contains(m_links, link))
        {
            ArrayUtility.Add(ref m_links, link);

            link.Origin = this;
        }
    }

    public ComboLink AddLink(ComboState destination)
    {
        if (IsConnected(destination))
        {
            return null;
        }

        ComboLink link = CreateInstance<ComboLink>();

        AddLink(link, destination);

        return link;
    }

    public void RemoveLink(int index)
    {
        ComboLink link = m_links[index];

        RemoveLink(link);
    }

    private void RemoveLink(ComboLink link)
    {
        EditorUtility.SetDirty(this);

        Undo.RegisterCompleteObjectUndo(this, "Removed link");

        if (ArrayUtility.Contains(m_links, link))
        {
            ArrayUtility.Remove(ref m_links, link);
        }

        link.DestroyLink();

        Undo.DestroyObjectImmediate(link);               
    }

    public void RemoveLinks(ComboState state)
    {
        EditorUtility.SetDirty(this);

        Undo.RegisterCompleteObjectUndo(this, "Removed State");        
        
        for (int i = 0; i < m_links.Length; i++)
        {
            if(m_links[i].Destination == state)
            {
                RemoveLink(i);
            }
        }
    }

    public bool IsConnected(ComboState state)
    {
        if(state == this || state == null)
        {
            return true;
        }

        for(int i = 0; i < m_links.Length; i++)
        {
            if(m_links[i].Destination == state)
            {
                return true;
            }
        }

        return false;     
    }

    public virtual void ReorderLinks(ComboLink[] orderedLinks)
    {
        Undo.RegisterCompleteObjectUndo(this, "Reorder links");

        Array.Copy(orderedLinks, m_links, m_links.Length);        
    }

    public void DestroyState()
    {
        Undo.RegisterCompleteObjectUndo(this, "Deleted State");

        for(int i = 0; i < m_links.Length; i++)
        {
            Undo.DestroyObjectImmediate(m_links[i]);

            m_links[i].DestroyLink();
        }
    }

    public IInputHandler InputHandler
    {
        set
        {
            m_handler = value;
        }
    }

  
       
    [Serializable]
    public class EditorData
    {
        public Vector2 position;
    }
#endif

    public CombatMove Move
    {
        internal set
        {
            m_moveInfo = value;
        }
        get
        {
            return m_moveInfo;
        }
    }

    public ComboLink[] Links
    {
        get
        {
            return m_links;
        }
    }

    public string ComboName
    {
        set
        {
            m_comboName = value;
        }
        get
        {
            return m_comboName;
        }
    }
}