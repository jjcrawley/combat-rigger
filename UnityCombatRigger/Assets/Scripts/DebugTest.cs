﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class DebugTest : StateMachineBehaviour
{    
    [SerializeField]
    private string m_value;

    [SerializeField]
    private bool pause;

    public bool debug;
        
    private ComboController m_controller;

#if UNITY_EDITOR

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (pause)
        {
            EditorApplication.isPaused = true;
        }

        if (debug)
        {
            if (animator.IsInTransition(layerIndex))
            {
                Debug.Log("State Enter Animator in transition " + m_value);
            }
            else
            {
                Debug.Log("State Enter Animator " + m_value);
            }
        }
        //m_firedMove = false;        
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (debug)
        {
            if (animator.IsInTransition(layerIndex))
            {
                Debug.Log("State Update Animator is transitioning " + m_value);
            }
            else
            {
                Debug.Log("State Update Animator " + m_value);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (debug)
        {
            if (animator.IsInTransition(layerIndex))
            {
                Debug.Log("State Exit Animator in transition " + m_value);
            }
            else
            {
                Debug.Log("State Exit Animator " + m_value);
            }
        }
    }

    public ComboController Controller
    {
        set
        {
            m_controller = value;       
        }
    }
#endif
    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}