InteropCPlusPlus contains all the code used in the C++ test DLL.

InteropTests contains all the C# code that uses the C++ test DLL.
To view the C++ and C# projects side by side, use the InteropTests sln.

InteropTestUnity contains a test Unity project that was used to test the C++ DLL, after it passed all the 
regular C# tests. 

