﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace InteropTests
{
    [StructLayout(LayoutKind.Sequential)]
    struct AStruct
    {
        public int a;
        public int b;
        public double c;
        [MarshalAs(UnmanagedType.U1)]
        public bool aBool;

        public override string ToString()
        {
            return "a = " + a.ToString() + ", b = " + b.ToString() + ", c =" + c.ToString() + ", aBool = " + aBool.ToString();
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    struct AnotherStruct
    {       
        public string aString;
        public string anotherString;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct BStruct
    {
        public int aNumber;
        public string aString;
    }

    class Program
    {
        const string DLLName = "InteropCPlusPlus";

        [DllImport(DLLName)]
        private static extern void PrintThing();

        [DllImport(DLLName)]
        private static extern int GetNumber();

        [DllImport(DLLName)]
        private static extern int AddNumbers(int numberOne, int numberTwo);

        [DllImport(DLLName)]
        private static extern float AddFloats(float numberOne, float numberTwo);

        [DllImport(DLLName)]
        private static extern void GetString(StringBuilder builder);

        [DllImport(DLLName)]
        private static extern void PrintString(string theString);

        [DllImport(DLLName)]
        [return: MarshalAs(UnmanagedType.U1)]
        private static extern bool GetBool();

        [DllImport(DLLName)]
        private static extern void GetAMessage(bool aBool, string message);

        [DllImport(DLLName)]
        private static extern void ReleaseArray(double[] array);

        [DllImport(DLLName)]
        private static extern void PrintPassedArray(int[] array, int numberItems);

        [DllImport(DLLName)]
        private static extern void CreateArray(double[] array, out int numberItems);

        [DllImport(DLLName)]
        private static extern void CreateDoubleArray(out IntPtr array, out int numberItems);

        [DllImport(DLLName)]
        private static extern void TakesOtherStruct(AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void PopulateOtherStruct(OtherStringDelegate callback, out AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void PopulateOtherStructTest(out AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void DoVoidCallback(PrintStuff stuff);

        private delegate void PrintStuff();

        [DllImport(DLLName)]
        private static extern void DoPrintCallBack(PrintMessage stuff, string message);
        private delegate int AddStuff(int number, int numberTwo);

        [DllImport(DLLName)]
        private static extern int AddCallback(AddStuff stuff, int number, int number2);
        private delegate void PrintMessage(string message);

        [DllImport(DLLName)]
        private static extern string GetAString(StringDelegate aString);
        private delegate string StringDelegate();

        [DllImport(DLLName)]
        private static extern string GetStringOtherCallback(OtherStringDelegate del);
        private delegate string OtherStringDelegate(string aString);

        [DllImport(DLLName)]
        private static extern void TakesAStruct(AStruct niceGuy);

        [DllImport(DLLName)]
        private static extern void PopulateStruct(out AStruct aStruct);

        [DllImport(DLLName)]
        private static extern IntPtr GetAStruct();

        [DllImport(DLLName)]
        private static extern IntPtr GetAnotherStruct();

        [DllImport(DLLName)]
        private static extern void TakesBStructArray(BStruct[] array, int number);

        [DllImport(DLLName)]
        private static extern void TakesStringArray([In, Out] string[] array, int numberItems);

        [DllImport(DLLName)]
        private static extern void ModifiesBStructs([In, Out] BStruct[] array, int numberOfItems);

        [DllImport(DLLName)]
        private static extern void GrabBStructArray(out IntPtr array, out int numberItems);
                
        [DllImport(DLLName)]
        private static extern void PrintAStructM(PrintStruc method);
        private delegate void PrintStruc(AStruct struc);

        public static void Main(string[] args)
        {
            Console.WriteLine("Begin tests");

            PrintThing();

            Console.WriteLine(GetNumber());

            Console.WriteLine(AddNumbers(20, 30));

            Console.WriteLine(AddFloats(2.5f, 7.2f));

            StringBuilder builder = new StringBuilder(256);

            GetString(builder);

            Console.WriteLine(builder.ToString());
                        
            PrintString("Hello from C#");

            Console.WriteLine(GetBool());

            GetAMessage(true, "Testing message");

            GetAMessage(false, "Testing message");

            Console.WriteLine("\nTesting callbacks");

            DoVoidCallback(PrintSomeStuff);

            int added = AddCallback(AddSomeNumbers, 20, 45);

            Console.WriteLine(added);

            DoPrintCallBack(PrintSomeMessages, "printing a message");
                        
            Console.WriteLine(GetStringOtherCallback(GetMessageOther));

            Console.WriteLine("\nTesting array stuff");

            double[] array = new double[5];
            int numberOfItems = 0;

            CreateArray(array, out numberOfItems);
            
            Console.WriteLine(numberOfItems);

            Console.WriteLine("\nHello from C#");
            for (int i = 0; i < numberOfItems; i++)
            {
                Console.Write(array[i] + ", ");
            }
            
            TestDoubleArray();
            
            Console.WriteLine();

            int[] anotherArray = new int[10];

            for(int i = 0; i < 10; i++)
            {
                anotherArray[i] = i;
            }

            PrintPassedArray(anotherArray, 10);

            Console.WriteLine("\nTesting struct stuff");

            AStruct aStruct = new AStruct();
            aStruct.a = 300;
            aStruct.b = 1;
            aStruct.c = 100.1;
            aStruct.aBool = true;

            TakesAStruct(aStruct);

            AStruct anotherStruct = new AStruct();

            PopulateStruct(out anotherStruct);

            Console.WriteLine(anotherStruct.ToString());
                        
            TestStructReturns();
                       
            TestStringStruct();
            
            Console.WriteLine("Structs with strings");

            AnotherStruct testStruct = new AnotherStruct();

            testStruct.anotherString = "Hello I'm a C# thing";
            testStruct.aString = "I'm a string in C#";

            TakesOtherStruct(testStruct);

            Console.WriteLine("Populating a struct");
            PopulateOtherStruct(GetMessageOther, out testStruct);

            Console.WriteLine(testStruct.aString + " " + testStruct.anotherString);

            PopulateOtherStructTest(out testStruct);

            Console.WriteLine(testStruct.aString + " " + testStruct.anotherString);
            
            TestReturnStruct();
            
            Console.WriteLine();

            Console.WriteLine("Testing array of strings");

            string[] stringArray = new string[3];

            stringArray[0] = "Hello one";
            stringArray[1] = "Hello two";
            stringArray[2] = "Bye three";

            TakesStringArray(stringArray, stringArray.Length);

            Console.WriteLine("In C#");

            for(int i = 0; i < stringArray.Length; i++)
            {
                Console.WriteLine(stringArray[i]);
            }

            Console.WriteLine("Testing array of structs");
                        
            TestStructArray();

            Console.WriteLine("Testing struct array retrieval");
                        
            TestStructArrayRetrieve();
            
            Console.WriteLine();

            PrintAStructM(PrintStructM);

            Console.WriteLine("\nFinished");

            Console.Read();
        } 

        private static void PrintStructM(AStruct struc)
        {
            Console.WriteLine("printing a struct " + struc.ToString());
        }

        private static void TestStructArrayRetrieve()
        {
            int size;

            BStruct[] bStructTest;
            IntPtr bArrayPointer;

            GrabBStructArray(out bArrayPointer, out size);

            IntPtr current = bArrayPointer;

            bStructTest = new BStruct[size];

            for (int i = 0; i < size; i++)
            {
                bStructTest[i] = new BStruct();

                bStructTest[i] = (BStruct)Marshal.PtrToStructure(current, typeof(BStruct));

                Marshal.DestroyStructure(current, typeof(BStruct));

                current = (IntPtr)((long)current + Marshal.SizeOf(bStructTest[i]));
            }

            Marshal.FreeCoTaskMem(bArrayPointer);    
            
            for(int i = 0; i < size; i++)
            {
                Console.WriteLine(bStructTest[i].aNumber + ", " + bStructTest[i].aString);
            }       
        }

        private static void TestStructArray()
        {
            BStruct[] bStructs = { new BStruct(), new BStruct(), new BStruct() };

            bStructs[0].aNumber = 30;
            bStructs[1].aNumber = 100;
            bStructs[2].aNumber = 55555;

            bStructs[0].aString = "I'm another string";
            bStructs[1].aString = "This is Sparda";
            bStructs[2].aString = "This is a bStruct";

            TakesBStructArray(bStructs, bStructs.Length);

            ModifiesBStructs(bStructs, bStructs.Length);
            
            Console.WriteLine("\nModified the bStructs");

            foreach (BStruct current in bStructs)
            {
                Console.WriteLine(current.aNumber + ", " + current.aString);
            }
        }
        
        private static void TestReturnStruct()
        {
            IntPtr pointer = GetAnotherStruct();

            AnotherStruct theStruct = (AnotherStruct)Marshal.PtrToStructure(pointer, typeof(AnotherStruct));

            Marshal.FreeCoTaskMem(pointer);

            Console.WriteLine("Test the return struct");

            Console.WriteLine(theStruct.anotherString + " " + theStruct.aString);
        } 

        private static void TestStringStruct()
        {
            AnotherStruct testStruct = new AnotherStruct();

            testStruct.anotherString = "Hello I'm a C# thing";
            testStruct.aString = "I'm a string in C#";

            TakesOtherStruct(testStruct);

            Console.WriteLine("Populating a struct");
            PopulateOtherStruct(GetMessageOther, out testStruct);

            Console.WriteLine(testStruct.aString + " " + testStruct.anotherString);

            PopulateOtherStructTest(out testStruct);

            Console.WriteLine(testStruct.aString + " " + testStruct.anotherString);
        }

        private static void TestDoubleArray()
        {
            int size = 10;

            IntPtr buffer = Marshal.AllocCoTaskMem(Marshal.SizeOf(size) * 10);
            //Marshal.Copy(array2, 0, buffer, array2.Length);

            CreateDoubleArray(out buffer, out size);

            Console.WriteLine("\ntesting copy");

            Console.WriteLine(size);

            if (size > 0)
            {
                double[] otherOne = new double[size];
                Marshal.Copy(buffer, otherOne, 0, size);
                Marshal.FreeCoTaskMem(buffer);
                foreach (double i in otherOne)
                {
                    Console.Write(i + ", ");
                }
            }
        }

        private static void TestStructReturns()
        {
            IntPtr structPointer = GetAStruct();

            AStruct returnedStruct = new AStruct();

            returnedStruct = (AStruct)Marshal.PtrToStructure(structPointer, typeof(AStruct));

            Marshal.FreeCoTaskMem(structPointer);

            Console.WriteLine(returnedStruct.ToString());
        }

        private static double[] MakeArray(int size)
        {
            Console.WriteLine("I was called");

            double[] array = new double[size];            

            return array;
        }
                
        private static void PrintSomeStuff()
        {
            Console.WriteLine("printing some stuff");
        }    

        private static void PrintSomeMessages(string message)
        {
            Console.WriteLine(message);
        }

        private static int AddSomeNumbers(int number, int number2)
        {
            return number + number2;            
        }

        private static void TestString()
        {
            //GetAString(GetMessageDefault);
            GetStringOtherCallback(GetMessageOther);       
        }

        private static string GetMessageDefault()
        {
            return "I'm a string";
        }

        private static string GetMessageOther(string message)
        {
            return message;
        }       
    }   
}