﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Text;

public class TestScript : MonoBehaviour
{
    void Awake()
    {
        Program.Main(null);
    }


    [StructLayout(LayoutKind.Sequential)]
    struct AStruct
    {
        public int a;
        public int b;
        public double c;
        [MarshalAs(UnmanagedType.U1)]
        public bool aBool;

        public override string ToString()
        {
            return "a = " + a.ToString() + ", b = " + b.ToString() + ", c =" + c.ToString() + ", aBool = " + aBool.ToString();
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    struct AnotherStruct
    {
        public string aString;
        public string anotherString;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct BStruct
    {
        public int aNumber;
        public string aString;
    }

    class Program
    {
        const string DLLName = "InteropCPlusPlus";

        [DllImport(DLLName)]
        private static extern void PrintThing();

        [DllImport(DLLName)]
        private static extern int GetNumber();

        [DllImport(DLLName)]
        private static extern int AddNumbers(int numberOne, int numberTwo);

        [DllImport(DLLName)]
        private static extern float AddFloats(float numberOne, float numberTwo);

        [DllImport(DLLName)]
        private static extern void GetString(StringBuilder builder);

        [DllImport(DLLName)]
        private static extern void PrintString(string theString);

        [DllImport(DLLName)]
        [return: MarshalAs(UnmanagedType.U1)]
        private static extern bool GetBool();

        [DllImport(DLLName)]
        private static extern void GetAMessage(bool aBool, string message);

        [DllImport(DLLName)]
        private static extern void ReleaseArray(double[] array);

        [DllImport(DLLName)]
        private static extern void PrintPassedArray(int[] array, int numberItems);

        [DllImport(DLLName)]
        private static extern void CreateArray(double[] array, out int numberItems);

        [DllImport(DLLName)]
        private static extern void CreateDoubleArray(out IntPtr array, out int numberItems);

        [DllImport(DLLName)]
        private static extern void TakesOtherStruct(AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void PopulateOtherStruct(OtherStringDelegate callback, out AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void PopulateOtherStructTest(out AnotherStruct anotherStruct);

        [DllImport(DLLName)]
        private static extern void DoVoidCallback(PrintStuff stuff);

        private delegate void PrintStuff();

        [DllImport(DLLName)]
        private static extern void DoPrintCallBack(PrintMessage stuff, string message);
        private delegate int AddStuff(int number, int numberTwo);

        [DllImport(DLLName)]
        private static extern int AddCallback(AddStuff stuff, int number, int number2);
        private delegate void PrintMessage(string message);

        [DllImport(DLLName)]
        private static extern string GetAString(StringDelegate aString);
        private delegate string StringDelegate();

        [DllImport(DLLName)]
        private static extern string GetStringOtherCallback(OtherStringDelegate del);
        private delegate string OtherStringDelegate(string aString);

        [DllImport(DLLName)]
        private static extern void TakesAStruct(AStruct niceGuy);

        [DllImport(DLLName)]
        private static extern void PopulateStruct(out AStruct aStruct);

        [DllImport(DLLName)]
        private static extern IntPtr GetAStruct();

        [DllImport(DLLName)]
        private static extern IntPtr GetAnotherStruct();

        [DllImport(DLLName)]
        private static extern void TakesBStructArray(BStruct[] array, int number);

        [DllImport(DLLName)]
        private static extern void TakesStringArray([In, Out] string[] array, int numberItems);

        [DllImport(DLLName)]
        private static extern void ModifiesBStructs([In, Out] BStruct[] array, int numberOfItems);

        [DllImport(DLLName)]
        private static extern void GrabBStructArray(out IntPtr array, out int numberItems);

        public static void Main(string[] args)
        {
            Debug.Log("Begin tests");

            PrintThing();

            Debug.Log(GetNumber());

            Debug.Log(AddNumbers(20, 30));

            Debug.Log(AddFloats(2.5f, 7.2f));

            StringBuilder builder = new StringBuilder(256);

            GetString(builder);

            Debug.Log(builder.ToString());

            PrintString("Hello from C#");

            Debug.Log(GetBool());

            GetAMessage(true, "Testing message");

            GetAMessage(false, "Testing message");

            Debug.Log("\nTesting callbacks");

            DoVoidCallback(PrintSomeStuff);

            int added = AddCallback(AddSomeNumbers, 20, 45);

            Debug.Log(added);

            DoPrintCallBack(PrintSomeMessages, "printing a message");

            Debug.Log(GetStringOtherCallback(GetMessageOther));

            Debug.Log("Testing array stuff");

            double[] array = new double[5];
            int numberOfItems = 0;

            CreateArray(array, out numberOfItems);

            Debug.Log(numberOfItems);

            string tempMessage = "";
            Console.WriteLine("\nHello from C#");
            for (int i = 0; i < numberOfItems; i++)
            {
                tempMessage += (array[i] + ", ");
            }

            Debug.Log(tempMessage);

            TestDoubleArray();

            //Console.WriteLine();

            int[] anotherArray = new int[10];

            for (int i = 0; i < 10; i++)
            {
                anotherArray[i] = i;
            }

            PrintPassedArray(anotherArray, 10);

            Debug.Log("Testing struct stuff");

            AStruct aStruct = new AStruct();
            aStruct.a = 300;
            aStruct.b = 1;
            aStruct.c = 100.1;
            aStruct.aBool = true;

            TakesAStruct(aStruct);

            AStruct anotherStruct = new AStruct();

            PopulateStruct(out anotherStruct);

            Debug.Log(anotherStruct.ToString());

            TestStructReturns();

            //I'm a buggy piece of code and hi, will always throw an exception
            //try
            //{
            //    AStruct theStruct = GetStruct();

            //    Console.WriteLine(theStruct.ToString());
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine("\nI'm an exception and hi");
            //    Console.WriteLine(ex.ToString());
            //}

            TestStringStruct();

            Debug.Log("Structs with strings");

            AnotherStruct testStruct = new AnotherStruct();

            testStruct.anotherString = "Hello I'm a C# thing";
            testStruct.aString = "I'm a string in C#";

            TakesOtherStruct(testStruct);

            Debug.Log("Populating a struct");
            PopulateOtherStruct(GetMessageOther, out testStruct);

            Debug.Log(testStruct.aString + " " + testStruct.anotherString);

            PopulateOtherStructTest(out testStruct);

            Debug.Log(testStruct.aString + " " + testStruct.anotherString);

            TestReturnStruct();

            //Console.WriteLine();

            Debug.Log("Testing array of strings");

            //Buggy, not sure why tested in windows application worked fine.
            //Not an issue though, as this was more for practice sake than actual use.
            string[] stringArray = new string[3];

            stringArray[0] = "Hello one";
            stringArray[1] = "Hello two";
            stringArray[2] = "Bye three";

            TakesStringArray(stringArray, stringArray.Length);

            Debug.Log("In C#");

            for (int i = 0; i < stringArray.Length; i++)
            {
                Debug.Log(stringArray[i]);
            }

            Debug.Log("Testing array of structs");

            TestStructArray();

            Debug.Log("Testing struct array retrieval");

            TestStructArrayRetrieve();

            //Console.WriteLine();

            Debug.Log("Finished");

            Console.Read();
        }

        static void TestStructArrayRetrieve()
        {
            int size;

            BStruct[] bStructTest;
            IntPtr bArrayPointer;

            GrabBStructArray(out bArrayPointer, out size);

            IntPtr current = bArrayPointer;

            bStructTest = new BStruct[size];

            for (int i = 0; i < size; i++)
            {
                bStructTest[i] = new BStruct();

                bStructTest[i] = (BStruct)Marshal.PtrToStructure(current, typeof(BStruct));

                Marshal.DestroyStructure(current, typeof(BStruct));

                current = (IntPtr)((long)current + Marshal.SizeOf(bStructTest[i]));
            }

            Marshal.FreeCoTaskMem(bArrayPointer);

            for (int i = 0; i < size; i++)
            {
                Debug.Log(bStructTest[i].aNumber + ", " + bStructTest[i].aString);
            }
        }

        static void TestStructArray()
        {
            BStruct[] bStructs = { new BStruct(), new BStruct(), new BStruct() };

            bStructs[0].aNumber = 30;
            bStructs[1].aNumber = 100;
            bStructs[2].aNumber = 55555;

            bStructs[0].aString = "I'm another string";
            bStructs[1].aString = "This is Sparda";
            bStructs[2].aString = "This is a bStruct";

            TakesBStructArray(bStructs, bStructs.Length);

            ModifiesBStructs(bStructs, bStructs.Length);

            Debug.Log("Modified the bStructs");

            foreach (BStruct current in bStructs)
            {
                Debug.Log(current.aNumber + ", " + current.aString);
            }
        }

        static void TestReturnStruct()
        {
            IntPtr pointer = GetAnotherStruct();

            AnotherStruct theStruct = (AnotherStruct)Marshal.PtrToStructure(pointer, typeof(AnotherStruct));

            Marshal.FreeCoTaskMem(pointer);

            Debug.Log("Test the return struct");

            Debug.Log(theStruct.anotherString + " " + theStruct.aString);
        }

        static void TestStringStruct()
        {
            AnotherStruct testStruct = new AnotherStruct();

            testStruct.anotherString = "Hello I'm a C# thing";
            testStruct.aString = "I'm a string in C#";

            TakesOtherStruct(testStruct);

            Debug.Log("Populating a struct");
            PopulateOtherStruct(GetMessageOther, out testStruct);

            Debug.Log(testStruct.aString + " " + testStruct.anotherString);

            PopulateOtherStructTest(out testStruct);

            Debug.Log(testStruct.aString + " " + testStruct.anotherString);
        }

        static void TestDoubleArray()
        {
            int size = 10;

            IntPtr buffer = Marshal.AllocCoTaskMem(Marshal.SizeOf(size) * 10);
            //Marshal.Copy(array2, 0, buffer, array2.Length);

            CreateDoubleArray(out buffer, out size);

            Debug.Log("testing copy");

            //Console.WriteLine(size);
            string message = "doubles tests";
            if (size > 0)
            {               
                double[] otherOne = new double[size];
                Marshal.Copy(buffer, otherOne, 0, size);
                Marshal.FreeCoTaskMem(buffer);
                foreach (double i in otherOne)
                {
                    message += (i + ", ");
                }
            }

            Debug.Log(message);
        }

        static void TestStructReturns()
        {
            IntPtr structPointer = GetAStruct();

            AStruct returnedStruct = new AStruct();

            returnedStruct = (AStruct)Marshal.PtrToStructure(structPointer, typeof(AStruct));

            Marshal.FreeCoTaskMem(structPointer);

            Debug.Log(returnedStruct.ToString());
        }

        static double[] MakeArray(int size)
        {
            Console.WriteLine("I was called");

            double[] array = new double[size];

            return array;
        }

        static void PrintSomeStuff()
        {
            Debug.Log("printing some stuff");
        }

        static void PrintSomeMessages(string message)
        {
            Debug.Log(message);
        }

        static int AddSomeNumbers(int number, int number2)
        {
            return number + number2;
        }

        static void TestString()
        {
            //GetAString(GetMessageDefault);
            GetStringOtherCallback(GetMessageOther);
        }

        static string GetMessageDefault()
        {
            return "I'm a string";
        }

        static string GetMessageOther(string message)
        {
            return message;
        }
    }
}