#pragma once

#include "Standard.h"
#include <stdio.h>
#include <array>
#include <objbase.h>
#include <iostream>
#include <strsafe.h>

using namespace std;

struct AStruct
{
	int a;
	int b;
	double c;
	bool aBool;
};


struct AnotherStruct
{
	char* aString;
	char* anotherString;
};

struct BStruct
{
	int aNumber;
	char* aString;
};

extern "C"
{
	EXPORT void CALL PrintThing();


	double doubleArray[] = { 2, 3.0, 20, 30.5, 50.05};

	typedef int(CALL *Callback)(int, int);
	EXPORT int AddCallback(Callback callback, int num, int num2);

	typedef void(CALL *VoidCallback)();
	EXPORT void DoVoidCallback(VoidCallback thing);

	typedef void(CALL *PrintCallback)(char* astring);
	EXPORT void CALL DoPrintCallBack(PrintCallback callback, char* theString);

	typedef char*(CALL *StringCallback)();
	EXPORT char* GetAString(StringCallback callback);

	typedef char*(CALL *GetStringCallback)(char* aString);
	EXPORT char* CALL GetStringOtherCallback(GetStringCallback callback);

	typedef double*(CALL *CreateArrayCallback)(int size);
	EXPORT void CALL MakeArrayWithCallback(CreateArrayCallback callback, double** theArray);

	EXPORT int CALL GetNumber();
	EXPORT int CALL AddNumbers(int number, int numberTwo);
	EXPORT float CALL AddFloats(float number, float numberTwo);
	EXPORT void CALL GetString(char* aString);
	EXPORT void CALL PrintString(char* aString);
	EXPORT bool CALL GetBool();
	EXPORT void CALL GetAMessage(bool aBool, char* aString);
	EXPORT void CALL PrintPassedArray(int* anArray, int numberItems);
	EXPORT void CALL CreateArray(double* anArray, int* numberItems);
	EXPORT void CALL CreateDoubleArray(double** anArray, int* number);
	EXPORT void CALL TakesAStruct(AStruct aStruct);
	EXPORT void CALL PopulateStruct(AStruct* aStruct);
	EXPORT AStruct* CALL GetAStruct();
	EXPORT AStruct CALL GetStruct();
	EXPORT void CALL TakesOtherStruct(AnotherStruct anotherStruct);
	EXPORT void CALL PopulateOtherStruct(GetStringCallback callback, AnotherStruct * anotherStruct);
	EXPORT void CALL PopulateOtherStructTest(AnotherStruct* anotherStruct);
	EXPORT void CALL TakesStringArray(char* aStringArray[], int numberItems);
	EXPORT AnotherStruct* CALL GetAnotherStruct();	
	EXPORT void CALL TakesBStructArray(BStruct* structArray, int numberItems);
	EXPORT void CALL ModifiesBStructs(BStruct* anArray, int numberOfItems);
	EXPORT void CALL GrabBStructArray(BStruct** anArray, int* numberItems);

	typedef void(CALL *PrintAStruct)(AStruct struc);
	EXPORT void CALL PrintAStructM(PrintAStruct print);
}