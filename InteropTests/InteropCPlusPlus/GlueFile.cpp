#include "GlueFile.h"

void CALL PrintThing()
{
	cout << "Hello" << endl;
}

int CALL GetNumber()
{
	return 20;
}

int CALL AddNumbers(int number, int numberTwo)
{
	return number + numberTwo;
}

float CALL AddFloats(float number, float numberTwo)
{
	return number + numberTwo;
}

void CALL GetString(char * aString)
{
	strcpy_s(aString, sizeof("Hello from C++"), "Hello from C++");
}

void CALL PrintString(char * aString)
{
	cout << aString << endl;
}

bool CALL GetBool()
{
	return false;
}

void CALL GetAMessage(bool aBool, char * aString)
{
	if (aBool)
	{
		cout << aString << endl;
	}
	else
	{
		cout << "Nope" << endl;
	}
}

void CALL PrintPassedArray(int * anArray, int numberItems)
{
	for (int i = 0; i < numberItems; i++)
	{
		cout << anArray[i] << ", ";
	}

	cout << endl;
}


void CALL CreateArray(double* anArray, int* numberItems)
{
	//anArray = new double[5];

	/*anArray = new double[5];

	for (int i = 0; i < 5; i++)
	{
		anArray[i] = doubleArray[i];
	}*/
	
	*numberItems = 5;	
/*
	anArray[0] = 2;
	anArray[1] = 3;
	anArray[2] = 20;
	anArray[3] = 30.5;
	anArray[4] = 75.4;
*/
	//anArray = doubleArray;
	
	cout << "Hello from C++" << endl;

	for (int i = 0; i < *numberItems; i++)
	{
		anArray[i] = doubleArray[i];
		cout << anArray[i] << ", ";
	}

//	cout << endl;
}

void CALL CreateDoubleArray(double ** anArray, int * number)
{
	double* newArray = (double*) CoTaskMemAlloc(sizeof(double) * 5);
	
	for (int i = 0; i < 5; i++)
	{
		newArray[i] = i + 0.5;
	}

	CoTaskMemFree(*anArray);
	*anArray = newArray;

	/*for (int i = 0; i < 5; i++)
	{
		cout << (*anArray)[i] << ", ";
	}*/

	//cout << endl;

	*number = 5;
}

void CALL TakesAStruct(AStruct aStruct)
{
	
	cout << endl;

	cout << "The struct I received has these things: " << endl;
	cout << "A and B are " << aStruct.a << " and " << aStruct.b << " respectively" << endl;
	cout << "The C double is: " << aStruct.c << endl;
	cout << "The bool provided is: " << aStruct.aBool << endl;
	cout << "That's all folks" << endl;
}

void CALL PopulateStruct(AStruct* aStruct)
{
	aStruct->a = 20;
	aStruct->b = 30;
	aStruct->c = 20.4;
	aStruct->aBool = true;
}

AStruct* CALL GetAStruct()
{
	AStruct* pointer = (AStruct*) CoTaskMemAlloc(sizeof(AStruct));

	pointer->a = 20;
	pointer->b = 300;
	pointer->c = 10.5;
	pointer->aBool = true;

	return pointer;
}

AStruct CALL GetStruct()
{
	return AStruct{400, 1000, 20.1122, false};
}

void CALL TakesOtherStruct(AnotherStruct anotherStruct)
{
	cout << "Received a struct and it says: " << endl;
	cout << anotherStruct.anotherString << " and ";
	cout << anotherStruct.aString << endl;
}

void CALL PopulateOtherStruct(GetStringCallback callback, AnotherStruct * anotherStruct)
{
	anotherStruct->anotherString = callback("Hello from anotherString");
	anotherStruct->aString = callback("Yay, it didn't explode");
}

void CALL PopulateOtherStructTest(AnotherStruct * anotherStruct)
{
	SIZE_T sizeOne = sizeof("Test twenty five");

	char* pointer = (char*)CoTaskMemAlloc(sizeOne);

	strcpy_s(pointer, sizeOne, "Test twenty five");

	char* otherString = (char*) CoTaskMemAlloc(sizeof("Another string"));
	
	//cout << sizeof(otherString);

	sizeOne = sizeof("Another string");

	strcpy_s(otherString, sizeOne, "Another string");

	anotherStruct->anotherString = pointer;
	anotherStruct->aString = otherString;
}

void CALL TakesStringArray(char * aStringArray[], int numberItems)
{
	cout << "Hello from C++" << endl;

	for (int i = 0; i < numberItems; i++)
	{
		cout << aStringArray[i] << endl;
	}

	cout << "Modifying values" << endl;

	for (int i = 0; i < numberItems; i++)
	{		
		char* temp = (char*) CoTaskMemAlloc(sizeof(char) * 10);

		strcpy_s(temp, sizeof("Hello"), "Hello");

		CoTaskMemFree(aStringArray[i]);
		
		aStringArray[i] = temp;
	}

	for (int i = 0; i < numberItems; i++)
	{
		cout << aStringArray[i] << endl;
	}
}

AnotherStruct *CALL GetAnotherStruct()
{
	AnotherStruct* anotherStruct;

	anotherStruct = (AnotherStruct*) CoTaskMemAlloc(sizeof(AnotherStruct));

	anotherStruct->anotherString = "Hello again";
	anotherStruct->aString = "Greetings";

	//anotherStruct->anotherString = (char*) CoTaskMemAlloc(sizeof(char) * 20);
	//strcpy_s(anotherStruct->anotherString, sizeof("Hello again"), "Hello again");

	//anotherStruct->aString = (char*) CoTaskMemAlloc(sizeof(char) * 30);
	//strcpy_s(anotherStruct->aString, sizeof(char) * 30, "Once again, greetings");
	
	return anotherStruct;
}

void CALL TakesBStructArray(BStruct * structArray, int numberItems)
{
	BStruct* pointer = structArray;

	cout << "Hello from C++, again";

	for (int i = 0; i < numberItems; i++)
	{
		cout << pointer->aNumber << ", " << pointer->aString << endl;
		pointer++;
	}
}

void CALL ModifiesBStructs(BStruct * anArray, int numberOfItems)
{
	for (int i = 0; i < numberOfItems; i++)
	{
		anArray[i].aNumber = anArray[i].aNumber + i * 10;
		
		char* message = "Hello";
		char* pointer = (char*) CoTaskMemAlloc(sizeof(message));
		
		CoTaskMemFree(anArray[i].aString);

		strcpy_s(pointer, sizeof(message), message);

		anArray[i].aString = pointer;
	}

	//cout << "Was called" << endl;
}

void CALL GrabBStructArray(BStruct ** anArray, int * numberItems)
{
	*numberItems = 3;

	*anArray = (BStruct*) CoTaskMemAlloc(*numberItems * sizeof(BStruct));

	BStruct* current = *anArray;
	
	for (int i = 0; i < 3; i++, current++)
	{
		char* aMessage = "Hello from C++";

		current->aNumber = i * 100;
		char* buffer = (char*) CoTaskMemAlloc(sizeof(char) * 20);

		//cout << sizeof("Hello from C++");
		//cout << sizeof(buffer) << endl;
		//cout << sizeof(aMessage) << endl;

		//cout << "Problems with string";

		//cout << strlen(aMessage);

		strcpy_s(buffer, strlen(aMessage) + 1, aMessage);

		current->aString = buffer;
	}
}

void CALL PrintAStructM(PrintAStruct print)
{
	AStruct struc;
	struc.a = 20;
	struc.b = 50;
	struc.aBool = false;
	struc.c = 20.303030303;

	print(struc);
}

int AddCallback(Callback callback, int num, int num2)
{
	return callback(num, num2);
}

void DoVoidCallback(VoidCallback thing)
{
	thing();
}

void CALL DoPrintCallBack(PrintCallback callback, char * theString)
{
	callback(theString);
}

char * GetAString(StringCallback callback)
{
	return callback();
}

char *CALL GetStringOtherCallback(GetStringCallback callback)
{
	return callback("Hello from other callback");
}

void CALL MakeArrayWithCallback(CreateArrayCallback callback, double** theArray)
{
	cout << "Hi there" << endl;

	double* createdArray = callback(10);

	cout << "Created an array in C#" << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << createdArray[i] << ", ";
	}

	cout << endl;

	*theArray = createdArray;
}