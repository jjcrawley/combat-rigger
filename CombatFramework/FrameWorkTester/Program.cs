﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace FrameWorkTester
{
    class Program
    {
        private static IntPtr m_moveOneBack;
        private static IntPtr m_moveTwoBack;

        private static IntPtr m_pointer;

        private static Interop.SimpleCallback m_callback;
        private static Interop.SimpleCallback m_linkCallback;
        private static Interop.SimpleCallback m_entryCallback;
        private static Interop.SimpleCallback m_paramsCallback;
        private static Interop.Debugger m_debugCallback;

        private static void Main(string[] args)
        {
            m_debugCallback = new Interop.Debugger(PrintMessage);

            Interop.SetDebugCallback(m_debugCallback);

            BuildBackendTest();

            Interop.UpdateController(m_pointer, 0);          

            Console.ReadKey();
        }

        private static void PrintMessage(string message)
        {
            Console.WriteLine(message);
        }

        private static void BuildBackendTest()
        {
            m_paramsCallback = new Interop.SimpleCallback(UpdateParamsController);

            m_pointer = Interop.CreateComboController();
            //m_handler.SetupBackend();
            //Debug.Log(m_pointer.ToString());
            Interop.SetParamsCallback(m_pointer, m_paramsCallback);
            //Interop.AssignInputHandler(m_pointer, m_handler.InternalSetup);

            //create and assign box
            IntPtr box = Interop.CreateComboBox();

            Interop.AssignComboBox(m_pointer, box);

            //create and assign tree
            IntPtr tree = Interop.CreateComboTree(box);

            //setup reset link
            IntPtr resetLink = Interop.GetResetTransition(tree);

            m_callback = new Interop.SimpleCallback(UpdateParams);
            m_linkCallback = new Interop.SimpleCallback(HelloFromLink);
            m_entryCallback = new Interop.SimpleCallback(EntryHello);

            Interop.SetLinkCallback(resetLink, m_callback);

            //create combo states
            IntPtr stateOne = Interop.CreateCombatState(tree);
            IntPtr stateTwo = Interop.CreateCombatState(tree);

            m_moveOneBack = Interop.CreateCombatMoveInfo();
            m_moveTwoBack = Interop.CreateCombatMoveInfo();            

            Interop.AssignComboMove(stateOne, m_moveOneBack);
            Interop.AssignComboMove(stateTwo, m_moveTwoBack);

            //setup entry links
            IntPtr entryLink = Interop.AddEntryTransition(tree, stateOne);

            Interop.SetLinkCallback(entryLink, m_entryCallback);

            //link state one to state two
            IntPtr linkOne = Interop.CreateAndAddPlayerLink(stateOne, stateTwo);

            Interop.SetPlayerLinkCallback(linkOne, m_linkCallback);
            Interop.SetButtonName(linkOne, "null");
            //Interop.SetInputPeriod(linkOne, 20.0f, 30.0f);

            Interop.OnCreate(m_pointer);
        }

        private static void UpdateParams()
        {
            Console.WriteLine("Hello from reset link");
            //m_animator.SetTrigger("Reset");
        }

        private static void HelloFromLink()
        {
            Console.WriteLine("Hello from regular link");
            //m_animator.SetTrigger("Move");
        }

        private static void EntryHello()
        {
            Console.WriteLine("Hello from entry");
            //m_animator.SetTrigger("Move");
        }

        private static void UpdateParamsController()
        {
            Console.WriteLine("updating controller params");
            //StartCoroutine(FrameTiming());
        }
    }

    internal class Interop
    {
        public delegate void SimpleCallback();
        public delegate void UpdateCallback(float normal);
        public delegate bool InputCallback(float normal);
        public delegate bool ButtonCallback(string name);
        public delegate float AxisCallback(string name);
        public delegate void Debugger(string message);

    //const string Dllname = "CombatFramework 1";
        const string Dllname = "CombatFramework";


        #region ComboController
        [DllImport(Dllname)]
        public static extern IntPtr CreateComboController();
        [DllImport(Dllname)]
        public static extern void ReleaseComboController(IntPtr controller);
        [DllImport(Dllname)]
        public static extern void OnCreate(IntPtr controller);
        [DllImport(Dllname)]
        public static extern void UpdateController(IntPtr controller, float normal);
        [DllImport(Dllname)]
        public static extern void AssignComboBox(IntPtr controller, IntPtr comboBox);
        [DllImport(Dllname)]
        public static extern void SetComboTree(IntPtr controller, string name);
        [DllImport(Dllname)]
        public static extern void SetSingleComboTree(IntPtr controller, IntPtr tree);
        [DllImport(Dllname)]
        public static extern void AssignInputHandler(IntPtr controller, IntPtr handler);
        [DllImport(Dllname)]
        public static extern void EndTransition(IntPtr controller);
        [DllImport(Dllname)]
        public static extern void SetParamsCallback(IntPtr controller, SimpleCallback callback);
        #endregion

        #region ComboTree
        [DllImport(Dllname)]
        public static extern IntPtr CreateAComboTree();
        [DllImport(Dllname)]
        public static extern IntPtr CreateComboTree(IntPtr comboBox);
        [DllImport(Dllname)]
        public static extern void SetTreeName(IntPtr tree, string name);
        [DllImport(Dllname)]
        public static extern IntPtr GetResetTransition(IntPtr tree);
        [DllImport(Dllname)]
        public static extern IntPtr AddEntryTransition(IntPtr tree, IntPtr state);
        #endregion

        #region ComboBox
        [DllImport(Dllname)]
        public static extern IntPtr CreateComboBox();
        [DllImport(Dllname)]
        public static extern void SetName(IntPtr comboBox, string name);
        [DllImport(Dllname)]
        public static extern void AddComboTree(IntPtr comboBox, IntPtr tree);
        [DllImport(Dllname)]
        public static extern void ReleaseComboBox(IntPtr box);
        #endregion

        #region ComboState
        [DllImport(Dllname)]
        public static extern IntPtr CreateCombatState(IntPtr tree);
        [DllImport(Dllname)]
        public static extern void AssignComboMove(IntPtr state, IntPtr move);
        [DllImport(Dllname)]
        public static extern void AddCombatModifier(IntPtr state, IntPtr mod);
        [DllImport(Dllname)]
        public static extern void ReleaseComboState(IntPtr state);
        #endregion

        #region CombatModifier
        [DllImport(Dllname)]
        public static extern IntPtr CreateCombatModifier(IntPtr move);
        [DllImport(Dllname)]
        public static extern void SetModOnHit(IntPtr modifier, SimpleCallback callback);
        [DllImport(Dllname)]
        public static extern void SetModOnEnter(IntPtr modifier, SimpleCallback callback);
        [DllImport(Dllname)]
        public static extern void SetModOnExit(IntPtr modifier, SimpleCallback callback);
        [DllImport(Dllname)]
        public static extern void SetModUpdate(IntPtr modifier, UpdateCallback callback);
        [DllImport(Dllname)]
        public static extern void ReleaseModifier(IntPtr modifier);
        #endregion

        #region CombatMove
        [DllImport(Dllname)]
        public static extern IntPtr CreateCombatMoveInfo();
        [DllImport(Dllname)]
        public static extern void CombatMoveOnCreate(IntPtr move);
        [DllImport(Dllname)]
        public static extern void SetMoveInputWindow(IntPtr info, float start, float end);
        [DllImport(Dllname)]
        public static extern void SetMoveName(IntPtr move, string name);
        [DllImport(Dllname)]
        public static extern void SetMoveLength(IntPtr move, int frameCount);
        [DllImport(Dllname)]
        public static extern void SetResetPeriod(IntPtr move, float start, float end);
        [DllImport(Dllname)]
        public static extern void ReleaseMoveInfo(IntPtr move);
        #endregion

        #region ComboLink
        [DllImport(Dllname)]
        public static extern IntPtr CreateAndAddLink(IntPtr startState, IntPtr destination);
        [DllImport(Dllname)]
        public static extern IntPtr CreateAndAddPlayerLink(IntPtr startState, IntPtr destination);
        [DllImport(Dllname)]
        public static extern void SetPlayerLinkCallback(IntPtr link, SimpleCallback callback);
        [DllImport(Dllname)]
        public static extern void SetLinkCallback(IntPtr link, SimpleCallback callback);
        [DllImport(Dllname)]
        public static extern void SetButtonName(IntPtr link, string name);
        [DllImport(Dllname)]
        public static extern void SetInputPeriod(IntPtr link, float start, float end);
        [DllImport(Dllname)]
        public static extern void SetFirePeriod(IntPtr link, float start, float end);
        [DllImport(Dllname)]
        public static extern void SetInstantFire(IntPtr link, bool instant);
        [DllImport(Dllname)]
        public static extern void ReleaseLink(IntPtr link);
        #endregion

        #region LinkLock
        [DllImport(Dllname)]
        public static extern IntPtr CreateAndAddLinkLock(IntPtr link);
        [DllImport(Dllname)]
        public static extern void SetLinkLockInputCallback(IntPtr link, InputCallback callback);
        [DllImport(Dllname)]
        public static extern void ReleaseLinkLock(IntPtr linkLock);
        #endregion

        #region InputHandler
        [DllImport(Dllname)]
        public static extern IntPtr CreateInputHandler();
        [DllImport(Dllname)]
        public static extern void RegisterInputButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld, IntPtr handler);
        [DllImport(Dllname)]
        public static extern void RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis, IntPtr handler);
        [DllImport(Dllname)]
        public static extern void ReleaseInputHandler(IntPtr handler);
        #endregion

        #region Helper
        [DllImport(Dllname)]
        public static extern void SetDebugCallback(Debugger callback);
        #endregion
    }  
}