#pragma once
#include "CombatMove.h"

class InteropCombatMove : public CombatMove
{
public:
	InteropCombatMove();
	~InteropCombatMove();
	void OnEnter(const ComboController* controller) override;
	void OnExit(const ComboController* controller) override;
	void OnUpdate(float normalTime, const ComboController* controller) override;
	void SetCallbacks(SimpleCallback enter, SimpleCallback exit, UpdateCallback update);
private:
	SimpleCallback m_exitCallback = nullptr;
	SimpleCallback m_enterCallback = nullptr;
	UpdateCallback m_updateCallback = nullptr;
};