#pragma once

#include <string>

#include "Standard.h"
#include "HitboxRig.h"
#include "IInputHandler.h"
#include "ComboBox.h"
#include "ComboLink.h"
#include "ComboState.h"
#include "BaseHitBoxController.h"

class ComboController : public IObject
{
public:
	ComboController();
	~ComboController();
	virtual void Update(float normalTime);
	void AssignInputHandler(IInputHandler* handler);
	void AssignComboBox(ComboBox* box);	
	void SetComboTree(std::string treeName);
	void SetComboTree(ComboTree* tree);
	void SetHitboxController(BaseHitBoxController* hitboxController);
	ComboBox* GetComboBox();
	BaseHitBoxController* GetHitboxController();
	IInputHandler* GetInputHandler();
	virtual void ResetController();
	virtual void BeginTransition(ComboLink* link, bool immediate = false);
	virtual void FireTransition();
	StateInfo GetStateInfo();
	void ProcessHitInfo();
	void ProcessContinuousHit();
	bool TransitionPending();
	virtual void EndTransition();
	virtual void Release() override;
	virtual void OnCreate() override;
protected:
	IInputHandler* m_handler = nullptr;
	ComboBox* m_comboBox = nullptr;	
	BaseHitBoxController* m_hitboxController = nullptr;
	ComboTree* m_currentTree = nullptr;
	ComboState* m_currentState = nullptr;
	ComboState* m_nextState = nullptr;
	ComboLink* m_currentLink = nullptr;
	bool b_inTransition = false;
	bool b_pendingTransition = false;
};