#pragma once

#include <vector>
#include <algorithm>
#include <cmath>
#include <string>
#include "Debugger.h"

#include "Standard.h"

struct Period
{
	float startTime = 0;
	float endTime = 0;
	Period();
	Period(float start, float end);
	Period(const Period& period);
public:
	static Period Normalise(Period period, int frameCount);
	bool InRange(float normal);
};

class MoveModifier;

class Move : public IObject
{
public:
	Move();
	~Move();
	void SetName(const char* name);
	const char* GetName();
	void SetDescription(const char* description);
	const char* GetDescription();
	void SetFrameCount(int frameCount);
	int GetFrameCount();
	int GetID();
	void SetID(int id);
	void AddModifier(MoveModifier* modifier);
	void RemoveModifier(MoveModifier* modifier);
	const std::vector<MoveModifier*>& GetMoveMods();		
	virtual void Release() override;
	virtual void OnCreate() override;
protected:
	std::vector<MoveModifier*> m_moveModifiers = std::vector<MoveModifier*>();
private:
	std::string m_name = "";
	std::string m_description = "";	
	int m_frameCount = 60;
	int m_moveID = 0;
};