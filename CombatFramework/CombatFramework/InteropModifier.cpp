#include "InteropModifier.h"

InteropModifier::InteropModifier()
{
}

InteropModifier::~InteropModifier()
{
}

void InteropModifier::Update(float normal)
{
	m_onUpdate(normal);
}

void InteropModifier::OnHit()
{
	m_onHit();
}

void InteropModifier::OnEnter()
{
	m_onEnter();
}

void InteropModifier::OnExit()
{
	m_onExit();
}

void InteropModifier::RegisterOnHit(SimpleCallback callback)
{
	m_onHit = callback;
}

void InteropModifier::RegisterOnEnter(SimpleCallback callback)
{
	m_onEnter = callback;
}

void InteropModifier::RegisterOnExit(SimpleCallback callback)
{
	m_onExit = callback;
}

void InteropModifier::RegisterOnUpdate(UpdateCallback callback)
{
	m_onUpdate = callback;
}