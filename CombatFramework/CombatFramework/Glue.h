#pragma once
#include "Standard.h"

#include "ComboController.h"
#include "ComboState.h"
#include "CombatLinkLock.h"
#include "CombatMove.h"
#include "CombatModifier.h"
#include "ComboLink.h"
#include "InputInterop.h"
#include "InteropModifier.h"
#include "InteropController.h"
#include "InteropLink.h"
#include "InteropLock.h"
#include "Debugger.h"
#include "InteropPlayerLink.h"
#include "InteropHitboxController.h"
#include "InteropCombatMove.h"

extern "C"
{
#pragma region IObject
	EXPORT void CALL ObjectOnCreate(void* obj);
	EXPORT void CALL ObjectOnRelease(void* obj);
#pragma endregion
#pragma region ComboController
	EXPORT InteropController* CALL CreateComboController();
	EXPORT void CALL ReleaseComboController(InteropController* controller);
	EXPORT void CALL OnCreate(InteropController* controller);
	EXPORT void CALL UpdateController(InteropController* controller, float normal);
	EXPORT void CALL UpdateInteropController(InteropController* controller, float normal, float secondState);
	EXPORT void CALL AssignComboBox(InteropController* controller, ComboBox* comboBox);
	EXPORT void CALL SetComboTree(InteropController* controller, const char* name);
	EXPORT void CALL SetSingleComboTree(InteropController* controller, ComboTree* tree);
	EXPORT void CALL AssignInputHandler(InteropController* controller, IInputHandler* handler);	
	EXPORT void CALL GetStateInfo(InteropController* controller, StateInfo* info);
	EXPORT void CALL TriggerEnter(InteropController* controller);
	EXPORT void CALL EndTransition(InteropController* controller);
	EXPORT void CALL ProcessHit(InteropController* controller);
	EXPORT void CALL SetParamsCallback(InteropController* controller, SimpleCallback callback);
	EXPORT void CALL SetHitboxController(InteropController* controller, BaseHitBoxController* hitboxController);
#pragma endregion
#pragma region ComboTree
	EXPORT ComboTree* CALL CreateAComboTree();
	EXPORT ComboTree* CALL CreateComboTree(ComboBox* box);
	EXPORT void CALL SetTreeName(ComboTree* tree, const char* name);
	EXPORT ComboLink* CALL GetResetTransition(ComboTree* tree);
	EXPORT ComboLink* CALL AddEntryTransition(ComboTree* tree, ComboState* state);
#pragma endregion
#pragma region ComboBox
	EXPORT ComboBox* CALL CreateComboBox();
	EXPORT void CALL SetName(ComboBox* box, char* name);
	EXPORT void CALL AddComboTree(ComboBox* box, ComboTree* tree);
	EXPORT void CALL ReleaseComboBox(ComboBox* box);
#pragma endregion
#pragma region ComboState
	EXPORT ComboState* CALL CreateCombatState(ComboTree* tree);
	EXPORT void CALL AssignComboMove(ComboState* state, InteropCombatMove* move);
	EXPORT void CALL AddCombatModifier(ComboState* state, InteropModifier* modifier);
	EXPORT void CALL SetResetFrame(ComboState* state, float frame);
	EXPORT void CALL ReleaseComboState(ComboState* state);
#pragma endregion
#pragma region CombatModifier
	EXPORT InteropModifier* CALL CreateCombatModifier(InteropCombatMove* move);
	EXPORT void CALL SetModOnHit(InteropModifier* modifier, SimpleCallback callback);
	EXPORT void CALL SetModOnEnter(InteropModifier* modifier, SimpleCallback callback);
	EXPORT void CALL SetModOnExit(InteropModifier* modifier, SimpleCallback callback);
	EXPORT void CALL SetModUpdate(InteropModifier* modifier, UpdateCallback callback);
	EXPORT void CALL ReleaseModifier(InteropModifier* modifier);
#pragma endregion
#pragma region CombatMove
	EXPORT InteropCombatMove* CALL CreateCombatMoveInfo();	
	EXPORT void CALL CombatMoveOnCreate(InteropCombatMove* move);
	EXPORT void CALL SetMoveID(InteropCombatMove* move, int id);
	EXPORT void CALL SetMoveName(InteropCombatMove* move, const char* name);
	EXPORT void CALL SetMoveLength(InteropCombatMove* move, int frameCount);
	EXPORT void CALL SetResetPeriod(InteropCombatMove* move, float start, float end);
	EXPORT void CALL SetMoveActivationPeriod(InteropCombatMove* move, float start, float end);
	EXPORT void CALL SetMoveContinuous(InteropCombatMove* move, bool continuous);
	EXPORT void CALL SetMoveDamageInterval(InteropCombatMove* move, float interval);
	EXPORT void CALL SetMoveCallbacks(InteropCombatMove* move, SimpleCallback enter, SimpleCallback exit, UpdateCallback update);
	EXPORT void CALL ReleaseMoveInfo(InteropCombatMove* move);
#pragma endregion
#pragma region ComboLink
	EXPORT InteropLink* CALL CreateAndAddLink(ComboState* state, ComboState* destination);
	EXPORT InteropPlayerLink* CALL CreateAndAddPlayerLink(ComboState* state, ComboState* destination);
	EXPORT void CALL SetPlayerLinkCallback(InteropPlayerLink* link, SimpleCallback callback);
	EXPORT void CALL SetLinkCallback(InteropLink* link, SimpleCallback callback);	
	EXPORT void CALL ReleaseLink(ComboLink* link);
	EXPORT void CALL SetButtonName(InteropLink* link, const char* name);
	EXPORT void CALL SetInputPeriod(InteropPlayerLink* link, float start, float end);
	EXPORT void CALL SetFirePeriod(InteropPlayerLink* link, float start, float end);
	EXPORT void CALL SetInstantTransition(InteropPlayerLink* link, bool instant);
#pragma endregion
#pragma region LinkLock
	EXPORT LinkLock* CALL CreateAndAddLinkLock(ComboLink* link);	
	EXPORT void CALL SetLinkLockInputCallback(InteropLock* link, InputCallback callback);
	EXPORT void CALL ReleaseLinkLock(InteropLock* lock);
#pragma endregion
#pragma region InputHandler
	EXPORT InputInterop* CALL CreateInputHandler();
	EXPORT void CALL RegisterInputButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld, InputInterop* handler);
	EXPORT void CALL RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis, InputInterop* handler);
	EXPORT void CALL ReleaseInputHandler(InputInterop* handler);
#pragma endregion	
#pragma region Hitboxes
	EXPORT InteropHitboxController* CALL CreateNewHitboxController();
	EXPORT void SetHitBoxCallbacks(InteropHitboxController* controller, BoxHandler handler, BoxHandler deactivator, FireDataHander fireDataHandler);
	EXPORT void ReleaseHitboxController(InteropHitboxController* controller);
#pragma endregion
#pragma region Helper
	EXPORT void CALL SetDebugCallback(DebugHelper callback);
#pragma endregion
}