#include "BasePlayerLink.h"
#include "ComboController.h"

BasePlayerLink::BasePlayerLink()
{
}

BasePlayerLink::~BasePlayerLink()
{
}

void BasePlayerLink::SetButtonName(std::string name)
{
	m_buttonName = name;
}

std::string BasePlayerLink::GetButtonName()
{
	return m_buttonName;
}

bool BasePlayerLink::Transition(float normalTime, ComboController* controller)
{
	if (controller->GetInputHandler()->GetButtonDown(m_buttonName.c_str()))
	{
		return ComboLink::Transition(normalTime, controller);
	}

	return false;
}