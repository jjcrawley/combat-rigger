#include "ComboLink.h"
#include "ComboController.h"
#include "ComboState.h"
#include "Debugger.h"

using namespace std;

ComboState::ComboState()
{
}

ComboState::~ComboState()
{
}

void ComboState::AssignCombatMove(CombatMove* move)
{
	m_moveInfo = move;
}

CombatMove* ComboState::GetMove()
{
	return m_moveInfo;
}

void ComboState::AssignComboTree(ComboTree* tree)
{
	m_owner = tree;
}

void ComboState::SetComboName(const char* comboName)
{
	m_comboName = std::string(comboName);
}

const char* ComboState::GetComboName()
{
	return m_comboName.c_str();
}

void ComboState::AddComboLink(ComboLink* link)
{
	if (link)
	{
		link->SetOrigin(this);
		m_links.push_back(link);
	}
}

void ComboState::RemoveComboLink(ComboLink* link)
{
	if (link)
	{
		m_links.erase(remove(m_links.begin(), m_links.end(), link));
	}
}

void ComboState::AddMod(CombatModifier* modifier)
{
	if (modifier)
	{
		m_mods.push_back(modifier);
	}
}

void ComboState::SetResetFrame(float frame)
{
	m_resetFrame = frame;

	if (m_moveInfo != nullptr)
	{
		m_resetFrameNormal = frame / m_moveInfo->GetFrameCount();
	}
}

float ComboState::GetResetFrame()
{
	return m_resetFrame;
}

void ComboState::ProcessHit(ComboController* controller)
{
	BaseHitBoxController* rig = controller->GetHitboxController();

	if (rig != nullptr)
	{
		if (m_moveInfo->GetContinuousDamage())
		{
			rig->QueueFireData(m_moveInfo->GetDamageInterval());
		}
	}
}

void ComboState::ProcessContinuousHit(ComboController* controller)
{
		
}

void ComboState::Update(float normalTime, ComboController* controller)
{
	if (m_moveInfo == nullptr)
	{
		return;
	}

	m_moveInfo->OnUpdate(normalTime, controller);

	if (!b_usedHitboxes)
	{
		ProcessHitboxes(normalTime, controller->GetHitboxController());
	}

	if (controller->TransitionPending())
	{
		return;
	}
		
	if (m_resetFrameNormal <= normalTime)
	{
		controller->ResetController();
	}
	else
	{
		ProcessTransitions(normalTime, controller);		
	}
}

void ComboState::OnEnter(const ComboController* controller)
{
	m_moveInfo->OnEnter(controller);

	b_usedHitboxes = false;
}

void ComboState::OnExit(const ComboController* controller)
{
	m_moveInfo->OnExit(controller);
}

void ComboState::ProcessTransitions(float normal, ComboController* controller)
{
	ComboLink* link = nullptr;

	for (int i = 0; i < m_links.size(); i++)
	{
		link = m_links[i];		

		if (link->Transition(normal, controller))
		{
			link->BeginTransition(controller);
			break;
		}
	}
}

void ComboState::ProcessHitboxes(float normal, BaseHitBoxController* controller)
{
	//Debugger::PrintMessage("Current move: " + to_string(m_moveInfo->GetID()) + " " + m_moveInfo->GetName());
	bool hitboxesOpen = m_moveInfo->ActivateHitboxes(normal);

	if (hitboxesOpen && !b_hitboxesActive)
	{
		b_hitboxesActive = true;
		//Debugger::PrintMessage("Starting box " + to_string(m_moveInfo->GetID()));
		controller->ActivateBoxes(m_moveInfo->GetID());
	}
	else if (!hitboxesOpen && b_hitboxesActive)
	{
		b_hitboxesActive = false;
		b_usedHitboxes = true;
		//Debugger::PrintMessage("Closing box " + to_string(m_moveInfo->GetID()));
		controller->DeactivateBoxes(m_moveInfo->GetID());
	}
}

void ComboState::OnCreate()
{
	//Debugger::PrintMessage(to_string(m_links.size()));
	
	/*for (int i = 0; i < m_links.size(); i++)
	{
		if (m_links[i])
		{		
			m_links[i]->OnCreate();
		}
		else
		{
			Debugger::PrintMessage("null link " + to_string(i));
		}		
	}*/

	SetResetFrame(m_resetFrame);
		
	//Setup move modifiers, when sorted
}

void ComboState::Release()
{
	for (int i = 0; i < m_links.size(); i++)
	{
		SAFE_DELETE(m_links[i]);
	}

	for (int i = 0; i < m_mods.size(); i++)
	{
		SAFE_DELETE(m_mods[i]);
	}
}