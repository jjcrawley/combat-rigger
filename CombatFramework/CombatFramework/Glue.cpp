#include "Glue.h"

void CALL ObjectOnCreate(void* obj)
{
	NULLRETURN(obj);

	IObject* object = (IObject*) obj;
	object->OnCreate();
}

void CALL ObjectOnRelease(void* obj)
{
	NULLRETURN(obj);

	IObject* object = (IObject*) obj;

	SAFE_DELETE(object);
}

InteropController* CALL CreateComboController()
{
	InteropController* controller = new InteropController();
	
	return controller;
}

void CALL ReleaseComboController(InteropController* controller)
{
	SAFE_DELETE(controller)
}

void CALL OnCreate(InteropController* controller)
{	
	NULLRETURN(controller);

	controller->OnCreate();	
}

void CALL UpdateController(InteropController* controller, float normal)
{
	NULLRETURN(controller);

	controller->Update(normal);
}

void CALL UpdateInteropController(InteropController* controller, float normal, float secondState)
{
	NULLRETURN(controller);

	controller->Update(normal, secondState);
}

void CALL AssignComboBox(InteropController* controller, ComboBox* comboBox)
{
	NULLRETURN(controller);

	controller->AssignComboBox(comboBox);	
}

void CALL SetComboTree(InteropController* controller, const char* name)
{
	NULLRETURN(controller);

	controller->SetComboTree(std::string(name));	
}

void CALL SetSingleComboTree(InteropController* controller, ComboTree* tree)
{
	NULLRETURN(controller);

	controller->SetComboTree(tree);	
}

void CALL AssignInputHandler(InteropController* controller, IInputHandler* handler)
{
	NULLRETURN(controller);

	controller->AssignInputHandler(handler);	
}

void CALL GetStateInfo(InteropController* controller, StateInfo* info)
{
	NULLRETURN(controller);

	StateInfo currentInfo = controller->GetStateInfo();

	info->moveID = currentInfo.moveID;	
}

void CALL TriggerEnter(InteropController* controller)
{
	NULLRETURN(controller);

	controller->TriggerEnter();
}

void CALL EndTransition(InteropController* controller)
{
	NULLRETURN(controller);

	controller->EndTransition();	
}

void CALL ProcessHit(InteropController* controller)
{
	NULLRETURN(controller);

	controller->ProcessHitInfo();	
}

void CALL SetParamsCallback(InteropController* controller, SimpleCallback callback)
{
	NULLRETURN(controller);
	
	controller->AssignTimerCallback(callback);	
}

void CALL SetHitboxController(InteropController* controller, BaseHitBoxController* hitboxController)
{
	NULLRETURN(controller);

	controller->SetHitboxController(hitboxController);	
}

ComboTree* CALL CreateAComboTree()
{
	return new ComboTree();
}

ComboTree* CALL CreateComboTree(ComboBox* box)
{
	NULLPRETURN(box);

	ComboTree* tree = new ComboTree();

	box->AddComboTree(tree);

	tree->SetResetLink(new InteropLink());

	return tree;
}

void CALL SetTreeName(ComboTree* tree, const char* name)
{
	NULLRETURN(tree);

	tree->SetName(name);	
}

ComboLink* CALL GetResetTransition(ComboTree* tree)
{
	NULLPRETURN(tree);
	
	return tree->GetResetLink();	
}

ComboLink* CALL AddEntryTransition(ComboTree* tree, ComboState* destination)
{
	NULLPRETURN(tree);

	InteropLink* link = new InteropLink();

	link->SetDestinationState(destination);

	tree->AddEntryLink(link);

	return link;
}

ComboBox* CALL CreateComboBox()
{
	ComboBox* box = new ComboBox();

	return box;
}

void CALL SetName(ComboBox* box, char* name)
{
	NULLRETURN(box);
	box->AssignName(name);	
}

void CALL AddComboTree(ComboBox* box, ComboTree* tree)
{
	NULLRETURN(box);

	box->AddComboTree(tree);
}

void CALL ReleaseComboBox(ComboBox* box)
{
	SAFE_DELETE(box);
}

ComboState* CALL CreateCombatState(ComboTree* tree)
{
	NULLPRETURN(tree);

	ComboState* state = new ComboState();

	tree->AddComboState(state);

	return state;
}

void CALL AssignComboMove(ComboState* state, InteropCombatMove* move)
{
	NULLRETURN(state);

	state->AssignCombatMove(move);
	//Debugger::PrintMessage(to_string(move->GetID()) + ", " + move->GetName());
}

void CALL AddCombatModifier(ComboState* state, InteropModifier* modifier)
{
	NULLRETURN(state);

	state->AddMod(modifier);
}

void CALL SetResetFrame(ComboState* state, float frame)
{
	NULLRETURN(state);

	state->SetResetFrame(frame);
}

void CALL ReleaseComboState(ComboState* state)
{
	SAFE_DELETE(state);
}

InteropModifier* CALL CreateCombatModifier(InteropCombatMove* move)
{
	InteropModifier* mod = new InteropModifier();

	if (move != nullptr)
	{
		move->AddModifier(mod);
	}

	return mod;
}

void CALL SetModOnHit(InteropModifier* modifier, SimpleCallback callback)
{
	NULLRETURN(modifier);

	modifier->RegisterOnHit(callback);
}

void CALL SetModOnEnter(InteropModifier* modifier, SimpleCallback callback)
{
	NULLRETURN(modifier);

	modifier->RegisterOnEnter(callback);
}

void CALL SetModOnExit(InteropModifier* modifier, SimpleCallback callback)
{
	NULLRETURN(modifier);

	modifier->RegisterOnExit(callback);
}

void CALL SetModUpdate(InteropModifier* modifier, UpdateCallback callback)
{
	NULLRETURN(modifier);

	modifier->RegisterOnUpdate(callback);
}

void CALL ReleaseModifier(InteropModifier* modifier)
{
	SAFE_DELETE(modifier);
}

InteropCombatMove* CALL CreateCombatMoveInfo()
{
	return new InteropCombatMove();
}

void CALL CombatMoveOnCreate(InteropCombatMove* move)
{
	NULLRETURN(move);

	move->OnCreate();
}

void CALL SetMoveID(InteropCombatMove* move, int id)
{
	NULLRETURN(move);

	move->SetID(id);
	//Debugger::PrintMessage("Assigning ID: " + to_string(id));
}

void CALL AddMoveModifier(CombatModifier* modifier, InteropCombatMove* move)
{
	NULLRETURN(move);

	move->AddModifier(modifier);
}

void CALL SetMoveName(InteropCombatMove* move, const char* name)
{
	NULLRETURN(move);

	move->SetName(name);
}

void CALL SetMoveLength(InteropCombatMove* move, int frameCount)
{
	NULLRETURN(move);

	move->SetFrameCount(frameCount);
}

void CALL SetResetPeriod(InteropCombatMove* move, float start, float end)
{
	NULLRETURN(move);

	move->SetResetPeriod({start, end});
}

void CALL SetMoveActivationPeriod(InteropCombatMove* move, float start, float end)
{
	NULLRETURN(move);

	move->SetActivatePeriod({ start, end });
}

void CALL SetMoveContinuous(InteropCombatMove* move, bool continuous)
{
	NULLRETURN(move);

	move->SetContinuous(continuous);
}

void CALL SetMoveDamageInterval(InteropCombatMove* move, float interval)
{
	NULLRETURN(move);

	move->SetDamageInterval(interval);
}

void CALL SetMoveCallbacks(InteropCombatMove* move, SimpleCallback enter, SimpleCallback exit, UpdateCallback update)
{
	NULLRETURN(move);

	move->SetCallbacks(enter, exit, update);
}

void CALL ReleaseMoveInfo(InteropCombatMove* move)
{
	SAFE_DELETE(move);
}

InteropLink* CALL CreateAndAddLink(ComboState* state, ComboState* destination)
{
	NULLPRETURN(state);

	InteropLink* link = new InteropLink();
	
	link->SetDestinationState(destination);

	state->AddComboLink(link);
	
	return link;
}

InteropPlayerLink* CALL CreateAndAddPlayerLink(ComboState* state, ComboState* destination)
{
	NULLPRETURN(state);

	InteropPlayerLink* link = new InteropPlayerLink();

	link->SetDestinationState(destination);
	/*link->SetInputPeriod({0,60});
	link->SetButtonName("Null");*/

	state->AddComboLink(link);	

	return link;
}

void CALL SetPlayerLinkCallback(InteropPlayerLink* link, SimpleCallback callback)
{
	NULLRETURN(link);

	link->SetUpdateCallback(callback);
}

void CALL SetLinkCallback(InteropLink* link, SimpleCallback callback)
{
	NULLRETURN(link);

	link->SetUpdateCallback(callback);
}

void CALL ReleaseLink(ComboLink* link)
{
	SAFE_DELETE(link);
}

void CALL SetButtonName(InteropLink* link, const char* name)
{
	NULLRETURN(link);

	link->SetButtonName(std::string(name));
}

void CALL SetInputPeriod(InteropPlayerLink* link, float start, float end)
{	
	NULLRETURN(link);

	link->SetInputPeriod({start, end});

	//link->OnCreate();
}

void CALL SetFirePeriod(InteropPlayerLink* link, float start, float end)
{
	NULLRETURN(link);

	link->SetFirePeriod({ start, end });
}

void CALL SetInstantTransition(InteropPlayerLink* link, bool instant)
{
	NULLRETURN(link);

	//Debugger::PrintMessage(BOOL_TO_STRING(instant));

	link->SetInstantFire(instant);
}

LinkLock* CALL CreateAndAddLinkLock(ComboLink* link)
{
	NULLPRETURN(link);

	InteropLock* lock = new InteropLock();

	link->AddLinkLock(lock);

	return lock;
}

void CALL SetLinkLockInputCallback(InteropLock* link, InputCallback callback)
{
	NULLRETURN(link);

	link->RegisterInputCallback(callback);
}

void CALL ReleaseLinkLock(InteropLock* lock)
{
	SAFE_DELETE(lock);
}

InputInterop* CALL CreateInputHandler()
{
	InputInterop* handler = new InputInterop();

	return handler;
}

void CALL RegisterInputButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld, InputInterop* handler)
{
	NULLRETURN(handler);

	handler->RegisterButtonCallbacks(buttonDown, buttonUp, buttonHeld);
}

void CALL RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis, InputInterop* handler)
{
	NULLRETURN(handler);

	handler->RegisterAxisCallbacks(axisRaw, axis);
}

void CALL ReleaseInputHandler(InputInterop* handler)
{
	SAFE_DELETE(handler);
}

InteropHitboxController* CALL CreateNewHitboxController()
{
	return new InteropHitboxController();
}

void SetHitBoxCallbacks(InteropHitboxController* controller, BoxHandler handler, BoxHandler deactivator, FireDataHander fireDataHandler)
{
	NULLRETURN(controller);

	controller->AssignHandlers(handler, deactivator, fireDataHandler);
}

void ReleaseHitboxController(InteropHitboxController * controller)
{
	SAFE_DELETE(controller);
}

void CALL SetDebugCallback(DebugHelper callback)
{
	Debugger::RegisterCallback(callback);
}