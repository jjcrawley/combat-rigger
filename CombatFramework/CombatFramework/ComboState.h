#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "Standard.h"

#include "ComboTree.h"
#include "CombatMove.h"
#include "IInputHandler.h"
#include "CombatModifier.h"
#include "BaseHitBoxController.h"

class ComboLink;
class ComboController;

struct StateInfo
{
	int moveID;
};

class ComboState : public IObject
{
public:
	ComboState();
	~ComboState();
	virtual void AssignCombatMove(CombatMove* move);
	CombatMove* GetMove();
	void AssignComboTree(ComboTree* tree);	
	void SetComboName(const char* comboName);
	const char* GetComboName();
	void AddComboLink(ComboLink* link);
	void RemoveComboLink(ComboLink* link);
	void AddMod(CombatModifier* modifier);
	void SetResetFrame(float frame);
	float GetResetFrame();
	virtual void ProcessHit(ComboController* controller);
	virtual void ProcessContinuousHit(ComboController* controller);
	virtual void Update(float normalTime, ComboController* controller);
	virtual void OnEnter(const ComboController* controller);
	virtual void OnExit(const ComboController* controller);
	virtual void OnCreate() override;
	virtual void Release() override;
protected:	
	CombatMove* m_moveInfo = nullptr;
	void ProcessTransitions(float normal, ComboController* controller);
private:	
	std::vector<ComboLink*> m_links = std::vector<ComboLink*>();	
	std::vector<CombatModifier*> m_mods = std::vector<CombatModifier*>();
	std::string m_comboName = "Unknown";
	void ProcessHitboxes(float normal, BaseHitBoxController* controller);
	bool b_hitboxesActive = false;
	bool b_usedHitboxes = false;
	float m_resetFrame = 0;
	float m_resetFrameNormal = 0;
	ComboTree* m_owner = nullptr;
};