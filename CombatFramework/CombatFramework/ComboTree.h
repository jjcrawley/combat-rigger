#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "Standard.h"
#include "IInputHandler.h"
#include "ComboLink.h"

class ComboBox;
class ComboState;

class ComboTree : public IObject
{
public:
	ComboTree();
	~ComboTree();
	void SetName(const char* name);
	const char* GetName();
	void SetDescription(const char* description);
	const char* GetDescription();	
	void AssignComboBox(ComboBox* comboBox);
	template<class T>
	T* CreateAndAddComboState();
	void AddComboState(ComboState* state);
	void RemoveComboState(ComboState* state);
	void SetResetLink(ComboLink* link);
	void AddEntryLink(ComboLink* link);
	ComboState* ApplyReset();
	ComboState* GetStart();
	ComboLink* GetResetLink();
	virtual void Release() override;
	virtual void OnCreate() override;
private:
	std::string m_name = "Unknown";
	std::string m_description = "";
	std::vector<ComboState*> m_comboStates = std::vector<ComboState*>();		
	ComboBox* m_comboBox = nullptr;
	ComboState* m_startState = nullptr;
	ComboLink* m_resetLink = nullptr;
};