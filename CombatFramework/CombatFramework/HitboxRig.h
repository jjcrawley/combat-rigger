#pragma once

#include <vector>

#include "Standard.h"
#include "Hitbox.h"

class HitboxRig
{
public:
	HitboxRig();
	~HitboxRig();
private:
	std::vector<Hitbox*> m_hitboxes;
};