#pragma once

#include "Standard.h"
#include "IInputHandler.h"

class ComboController;

class LinkLock : public IObject
{
public:
	LinkLock();
	~LinkLock();
	virtual bool AllowAccess(float normal, const ComboController* controller) = 0;
	virtual void OnCreate() override;
	virtual void Release() override;
};