#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "Standard.h"
#include "HitboxRig.h"
#include "IInputHandler.h"
#include "ComboTree.h"

class ComboBox : public IObject
{
public:
	ComboBox();
	~ComboBox();
	void AssignName(char* name);
	const char* GetName();
	void SetDescription(char* description);
	const char* GetDescription();
	void AddComboTree(ComboTree* tree);
	void RemoveComboTree(ComboTree* tree);
	ComboTree* GetComboTree(std::string name);
	std::vector<ComboTree*>* GetComboTrees();
	virtual void OnCreate() override;
	virtual void Release() override;
	ComboTree*& operator[](size_t index);
private:	
	std::vector<ComboTree*> m_comboTrees = std::vector<ComboTree*>();
	std::string m_name = "Unknown";
	std::string m_description = "Unknown";
};