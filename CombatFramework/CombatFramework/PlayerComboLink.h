#pragma once
#include "BasePlayerLink.h"

class PlayerComboLink :	public BasePlayerLink
{
public:
	PlayerComboLink();
	~PlayerComboLink();
	virtual void OnCreate() override;
	void Normalise(int frameCount) override;
	bool Transition(float normalTime, ComboController* controller) override;
	virtual void Update(float normalTime, ComboController* controller) override;
	virtual void BeginTransition(ComboController* controller);
	Period GetInputPeriod();
	Period GetFirePeriod();
	void SetFirePeriod(Period period);
	void SetInstantFire(bool instantFire);
	void SetInputPeriod(Period inputPeriod);
private:	
	Period m_inputPeriod = Period(0.0f, 60.0f);
	Period m_inputNormal = Period(0.0f, 1.0f);	
	bool b_instantTransition = true;
	Period m_firePeriod = Period(0.0f, 60.0f);
	Period m_fireNormal = Period(0.0f, 1.0f);
};