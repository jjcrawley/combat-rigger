#include "BaseHitBoxController.h"
#include "Debugger.h"

BaseHitBoxController::BaseHitBoxController()
{
}

BaseHitBoxController::~BaseHitBoxController()
{
}

void BaseHitBoxController::ActivateBoxes(int id)
{
}

void BaseHitBoxController::DeactivateBoxes(int id)
{
}

void BaseHitBoxController::QueueFireData(float interval)
{
}

void BaseHitBoxController::OnCreate()
{
}

void BaseHitBoxController::Release()
{
	//Debugger::PrintMessage("Releasing hit box controller");
}