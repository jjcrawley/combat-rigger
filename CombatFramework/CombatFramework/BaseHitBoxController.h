#pragma once
#include "Standard.h"

class BaseHitBoxController : public IObject
{
public:
	BaseHitBoxController();
	~BaseHitBoxController();
	virtual void ActivateBoxes(int id);
	virtual void DeactivateBoxes(int id);
	virtual void QueueFireData(float interval);
	virtual void OnCreate() override;
	virtual void Release() override;
};