#pragma once

#include "PlayerComboLink.h"

class InteropPlayerLink : public PlayerComboLink
{
public:
	InteropPlayerLink();
	~InteropPlayerLink();
	void UpdateParams() override;
	void SetUpdateCallback(SimpleCallback callback);
private:
	SimpleCallback m_paramsUpdate = nullptr;
};