#pragma once

#include <vector>
#include <algorithm>

#include "Standard.h"
#include "LinkLock.h"
#include "Move.h"

class ComboState;
class ComboTree;
class ComboController;

class ComboLink : public IObject
{
public:
	ComboLink();
	~ComboLink();
	virtual void Normalise(int frameCount);
	void AddLinkLock(LinkLock* lock);
	void RemoveLinkLock(LinkLock* lock);	
	virtual void Update(float normal, ComboController* controller);
	virtual bool Transition(float frame, ComboController* controller);
	virtual void BeginTransition(ComboController* controller);
	virtual void UpdateParams();
	void SetDestinationState(ComboState* state);
	void SetOrigin(ComboState* origin);
	ComboState* GetOrigin();
	ComboState* GetDestination();	
	virtual void OnCreate() override;
	virtual void Release() override;
private:
	ComboState* m_destination = nullptr;	
	ComboState* m_origin = nullptr;
	std::vector<LinkLock*> m_linkLocks = std::vector<LinkLock*>();			
};