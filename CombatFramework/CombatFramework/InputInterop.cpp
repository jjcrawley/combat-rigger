#include "InputInterop.h"
#include "Debugger.h"

InputInterop::InputInterop()
{
}

InputInterop::~InputInterop()
{
}

float InputInterop::GetAxis(const char* id)
{
	return m_axis(id);
}

float InputInterop::GetAxisRaw(const char* id)
{
	return m_axisRaw(id);
}

bool InputInterop::GetButtonDown(const char* id)
{
	return m_buttonDown(id);
}

bool InputInterop::GetButtonUp(const char* id)
{
	return m_buttonUp(id);
}

bool InputInterop::GetButtonHeld(const char* id)
{
	return m_buttonHeld(id);
}

int InputInterop::GetID(const char* name)
{
	return m_getID(name);
}

void InputInterop::RegisterButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld)
{
	m_buttonDown = buttonDown;
	m_buttonHeld = buttonHeld;
	m_buttonUp = buttonUp;
}

void InputInterop::RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis)
{
	m_axis = axis;
	m_axisRaw = axisRaw;
}

void InputInterop::RegisterIDCallback(IdCallback id)
{
	m_getID = id;
}

void InputInterop::OnCreate()
{
}

void InputInterop::Release()
{
	//Debugger::PrintMessage("releasing input handler");
}