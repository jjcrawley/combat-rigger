#pragma once
#include "ComboLink.h"

class BasePlayerLink : public ComboLink
{
public:
	BasePlayerLink();
	~BasePlayerLink();
	void SetButtonName(std::string name);
	std::string GetButtonName();
	virtual bool Transition(float normalTime, ComboController* controller) override;
protected:
	std::string m_buttonName = "Unknown";
};