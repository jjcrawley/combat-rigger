#pragma once

class IObject
{
public:
	virtual void OnCreate() = 0;
	virtual void Release() = 0;
};