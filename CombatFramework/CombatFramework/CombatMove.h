#pragma once

#include "Standard.h"
#include "Move.h"

class ComboController;

class CombatMove : public Move
{
public:
	CombatMove();
	~CombatMove();
	virtual void OnCreate() override;
	virtual void OnEnter(const ComboController* controller);
	virtual void OnExit(const ComboController* controller);
	virtual void OnUpdate(float normalTime, const ComboController* controller);
	void SetBaseDamage(float baseDamage);
	float GetBaseDamage();
	void SetDamageInterval(float interval);
	float GetDamageInterval();
	void SetResetPeriod(Period period);
	Period GetResetPeriod();
	void SetActivatePeriod(Period period);
	Period GetActivatePeriod();
	void SetContinuous(bool continuous);
	bool GetContinuousDamage();
	bool ShouldReset(float normal);
	bool ActivateHitboxes(float normal);
private:	
	float m_baseDamage = 0;	
	bool b_continuousDamage = false;
	float m_damageInterval = 0;
	Period m_resetPeriod = Period(0, 0);
	Period m_resetNormalise = Period(0, 0);
	Period m_hitboxActivatePeriod = Period(0, 0);
	Period m_hitboxActivatePeriodNormal = Period(0, 0);
};