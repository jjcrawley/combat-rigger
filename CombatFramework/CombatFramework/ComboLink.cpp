#include "ComboState.h"
#include "ComboLink.h"
#include "ComboController.h"

ComboLink::ComboLink()
{
}

ComboLink::~ComboLink()
{
}

void ComboLink::Normalise(int frameCount)
{
	
}

void ComboLink::AddLinkLock(LinkLock* lock)
{
	if (lock)
	{
		m_linkLocks.push_back(lock);
	}
}

void ComboLink::RemoveLinkLock(LinkLock* lock)
{
	if (lock)
	{
		m_linkLocks.erase(std::remove(m_linkLocks.begin(), m_linkLocks.end(), lock));
	}
}

void ComboLink::Update(float normal, ComboController* controller)
{

}

bool ComboLink::Transition(float frame, ComboController* controller)
{		
	for (int i = 0; i < m_linkLocks.size(); i++)
	{
		LinkLock* lock = m_linkLocks[i];

		if (!lock->AllowAccess(frame, controller))
		{
			return false;
		}
	}

	return true;
}

void ComboLink::BeginTransition(ComboController* controller)
{
	controller->BeginTransition(this, true);
}

void ComboLink::UpdateParams()
{
}

void ComboLink::SetDestinationState(ComboState* state)
{
	m_destination = state;
}

void ComboLink::SetOrigin(ComboState* origin)
{
	m_origin = origin;
}

ComboState* ComboLink::GetOrigin()
{
	return m_origin;
}

ComboState* ComboLink::GetDestination()
{
	return m_destination;
}

void ComboLink::OnCreate()
{	
	/*for (int i = 0; i < m_linkLocks.size(); i++)
	{
		m_linkLocks[i]->OnCreate();
	}*/
}

void ComboLink::Release()
{
	for (int i = 0; i < m_linkLocks.size(); i++)
	{
		SAFE_DELETE(m_linkLocks[i]);
	}
}