#include "MoveModifier.h"
#include "Move.h"

using namespace std;

Move::Move()
{
}

Move::~Move()
{
}

void Move::SetName(const char* name)
{
	m_name = std::string(name);
}

const char* Move::GetName()
{
	return m_name.c_str();
}

void Move::SetDescription(const char* description)
{
	m_description = std::string(description);
}

const char* Move::GetDescription()
{
	return m_description.c_str();
}

void Move::SetFrameCount(int frameCount)
{
	m_frameCount = frameCount;
}

int Move::GetFrameCount()
{
	return m_frameCount;
}

int Move::GetID()
{
	return m_moveID;
}

void Move::SetID(int id)
{
	m_moveID = id;
}

void Move::AddModifier(MoveModifier* modifier)
{
	if (modifier != nullptr)
	{
		m_moveModifiers.push_back(modifier);
		modifier->AssignMoveInfo(this);
	}
}

void Move::RemoveModifier(MoveModifier* modifier)
{
	m_moveModifiers.erase(std::remove(m_moveModifiers.begin(), m_moveModifiers.end(), modifier), m_moveModifiers.end());
}

const vector<MoveModifier*>& Move::GetMoveMods()
{
	return m_moveModifiers;
}

void Move::Release()
{
	//Debugger::PrintMessage("Releasing move");

	for (int i = 0; i < m_moveModifiers.size(); i++)
	{
		SAFE_DELETE(m_moveModifiers[i]);
	}
}

void Move::OnCreate()
{
	for (int i = 0; i < m_moveModifiers.size(); i++)
	{
		m_moveModifiers[i]->OnCreate();
	}
}

Period::Period()
{
	startTime = 0.0f;
	endTime = 0.0f;
}

Period::Period(float start, float end)
{
	startTime = start;
	endTime = end;
}

Period::Period(const Period & period)
{
	startTime = period.startTime;
	endTime = period.endTime;
}

Period Period::Normalise(Period period, int frameCount)
{
	period.startTime = (period.startTime / frameCount);
	period.endTime = (period.endTime / frameCount);

	return period;
}

bool Period::InRange(float normal)
{
	return normal <= endTime && normal >= startTime;
}