#include "InteropHitboxController.h"

InteropHitboxController::InteropHitboxController()
{
}

InteropHitboxController::~InteropHitboxController()
{
}

void InteropHitboxController::ActivateBoxes(int id)
{
	m_activator(id);
}

void InteropHitboxController::DeactivateBoxes(int id)
{
	m_deactivator(id);
}

void InteropHitboxController::QueueFireData(float interval)
{
	m_fireDataHandler(interval);
}

void InteropHitboxController::AssignHandlers(BoxHandler activator, BoxHandler deactivator, FireDataHander fireDataHandler)
{
	m_activator = activator;
	m_deactivator = deactivator;
	m_fireDataHandler = fireDataHandler;
}