#pragma once
#include "BasePlayerLink.h"

class InteropLink :	public BasePlayerLink
{
public:
	InteropLink();
	~InteropLink();
	void UpdateParams() override;
	void SetUpdateCallback(SimpleCallback callback);
private:
	SimpleCallback m_paramsCallback = nullptr;
};