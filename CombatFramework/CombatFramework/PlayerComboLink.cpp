#include "PlayerComboLink.h"
#include "ComboState.h"
#include "Debugger.h"
#include "ComboController.h"
#include <string>

PlayerComboLink::PlayerComboLink()
{	
	m_inputPeriod = Period(0.0f, 60.0f);
	m_inputNormal = Period(0.0f, 1.0f);
}

PlayerComboLink::~PlayerComboLink()
{
}

void PlayerComboLink::OnCreate()
{	
	//Debugger::PrintMessage("I was made");

	if (GetOrigin() == nullptr)
	{
		Debugger::PrintMessage("origin is null");
		return;
	}

	if (GetOrigin()->GetMove() == nullptr)
	{
		Debugger::PrintMessage("Move is null");	
		//return;
	}
	
	BasePlayerLink::OnCreate();
}

void PlayerComboLink::Normalise(int frameCount)
{	
	m_inputNormal = Period::Normalise(m_inputPeriod, frameCount);	
}

bool PlayerComboLink::Transition(float normalTime, ComboController* controller)
{
	if (m_inputNormal.InRange(normalTime))
	{
		return BasePlayerLink::Transition(normalTime, controller);
	}

	return false;
}

void PlayerComboLink::Update(float normalTime, ComboController* controller)
{
	if (m_fireNormal.InRange(normalTime))
	{
		controller->FireTransition();
	}
}

void PlayerComboLink::BeginTransition(ComboController* controller)
{	
	controller->BeginTransition(this, b_instantTransition);
}

Period PlayerComboLink::GetInputPeriod()
{
	return m_inputPeriod;
}

Period PlayerComboLink::GetFirePeriod()
{
	return m_firePeriod;
}

void PlayerComboLink::SetFirePeriod(Period period)
{
	m_firePeriod = period;
	m_fireNormal = Period::Normalise(m_firePeriod, GetOrigin()->GetMove()->GetFrameCount());
}

void PlayerComboLink::SetInstantFire(bool instantFire)
{
	b_instantTransition = instantFire;
}

void PlayerComboLink::SetInputPeriod(Period inputPeriod)
{	
	m_inputPeriod = inputPeriod;
	m_inputNormal = Period::Normalise(m_inputPeriod, GetOrigin()->GetMove()->GetFrameCount());
}