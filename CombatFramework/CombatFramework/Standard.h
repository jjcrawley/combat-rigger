#pragma once

#include "IObject.h"
#include <string>

#define EXPORT __declspec(dllexport)
#define CALL __stdcall

#define SAFE_DELETE(p) if(p){ p->Release(); delete p; }

#define NULLRETURN(p) if(p == nullptr) { return; }
#define NULLPRETURN(p) if(p == nullptr) { return nullptr; }

#define BOOL_TO_STRING(b) b ? "True" : "False" 

typedef void(CALL* SimpleCallback)();
typedef void(CALL* UpdateCallback)(float normalTime);
typedef bool(CALL* InputCallback)(float normal);
typedef void(CALL* DebugHelper)(const char* message);