#include "EntryState.h"
#include "ComboController.h"

EntryState::EntryState()
{
}

EntryState::~EntryState()
{
}

void EntryState::Update(float normalTime, ComboController* controller)
{
	if (!controller->TransitionPending())
	{
		ProcessTransitions(normalTime, controller);
	}
}

void EntryState::OnEnter(const ComboController* controller)
{
}

void EntryState::OnExit(const ComboController* controller)
{
}