#include "InteropCombatMove.h"
#include "ComboController.h"

InteropCombatMove::InteropCombatMove()
{
}

InteropCombatMove::~InteropCombatMove()
{
}

void InteropCombatMove::OnEnter(const ComboController* controller)
{
	if (m_enterCallback)
	{
		m_enterCallback();
	}
}

void InteropCombatMove::OnExit(const ComboController* controller)
{	
	if (m_exitCallback)
	{
		m_exitCallback();
	}
}

void InteropCombatMove::OnUpdate(float normalTime, const ComboController* controller)
{
	if (m_updateCallback)
	{
		m_updateCallback(normalTime);
	}
}

void InteropCombatMove::SetCallbacks(SimpleCallback enter, SimpleCallback exit, UpdateCallback update)
{
	m_enterCallback = enter;
	m_exitCallback = exit;
	m_updateCallback = update;
}