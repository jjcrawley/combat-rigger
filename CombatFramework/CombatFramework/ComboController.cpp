#include "ComboController.h"
#include "Debugger.h"

ComboController::ComboController()
{
	//Debugger::PrintMessage("Made a combo controller");
}

ComboController::~ComboController()
{
}

void ComboController::Update(float normalTime)
{	
	if (b_inTransition)
	{
		//Debugger::PrintMessage("Waiting for transition");
		return;
	}
	
	if (m_currentLink != nullptr)
	{		
		//Debugger::PrintMessage("Updating current link");
		m_currentLink->Update(normalTime, this);			
	}
		
	m_currentState->Update(normalTime, this);
}

void ComboController::AssignInputHandler(IInputHandler* handler)
{	
	m_handler = handler;
}

void ComboController::AssignComboBox(ComboBox* comboBox)
{
	m_comboBox = comboBox;
	
	if (m_comboBox->GetComboTrees()->size() > 0)
	{
		m_currentTree = m_comboBox->GetComboTrees()->at(0);
	}

	//OnCreate();	
}

void ComboController::SetComboTree(std::string treeName)
{
	ResetController();
	m_currentTree = m_comboBox->GetComboTree(treeName);	
}

void ComboController::SetComboTree(ComboTree* tree)
{
	m_currentTree = tree;
}

void ComboController::SetHitboxController(BaseHitBoxController* hitboxController)
{
	m_hitboxController = hitboxController;
}

ComboBox* ComboController::GetComboBox()
{
	return m_comboBox;
}

BaseHitBoxController* ComboController::GetHitboxController()
{
	return m_hitboxController;
}

IInputHandler* ComboController::GetInputHandler()
{
	return m_handler;
}

void ComboController::ResetController()
{
	m_nextState = m_currentTree->ApplyReset();
	b_inTransition = true;
	b_pendingTransition = true;
}

void ComboController::BeginTransition(ComboLink* link, bool immediate)
{
	//Debugger::PrintMessage("Beginning a transition");
	//Debugger::PrintMessage("Immediate is: " + immediate ? "True" : "False");	

	m_currentLink = link;
	b_pendingTransition = true;

	if (immediate)
	{
		//Debugger::PrintMessage("Immediate was true");
		FireTransition();
	}

	//Debugger::PrintMessage("started transition");
}

void ComboController::FireTransition()
{
	//Debugger::PrintMessage("Firing transition");
	b_inTransition = true;
	m_currentLink->UpdateParams();
	m_nextState = m_currentLink->GetDestination();
	m_currentLink = nullptr;
}

StateInfo ComboController::GetStateInfo()
{
	StateInfo info;

	if (m_currentState->GetMove() != nullptr)
	{
		info.moveID = m_currentState->GetMove()->GetID();
	}
	else
	{
		info.moveID = 0;
	}

	return info;
}

void ComboController::ProcessHitInfo()
{
	m_currentState->ProcessHit(this);
}

void ComboController::ProcessContinuousHit()
{

}

bool ComboController::TransitionPending()
{
	return b_pendingTransition || b_inTransition;
}

void ComboController::EndTransition()
{
	b_inTransition = false;
	b_pendingTransition = false;
	m_currentState->OnExit(this);

	m_currentState = m_nextState;
	//m_currentState->OnEnter(this);

	m_nextState = nullptr;

	//Debugger::PrintMessage("Ended transition, backend");
}

void ComboController::Release()
{
	//Debugger::PrintMessage("releasing combo controller");

	if (m_comboBox != nullptr)
	{
		SAFE_DELETE(m_comboBox);
	}
	else
	{
		SAFE_DELETE(m_currentTree);
	}
}

void ComboController::OnCreate()
{
	if (m_comboBox != nullptr)
	{
		//m_comboBox->OnCreate();
		m_currentTree = (*m_comboBox)[0];
	}
	else
	{
		//m_currentTree->OnCreate();
	}
			
	m_currentState = m_currentTree->GetStart();
	
	m_currentState->OnEnter(this);	
}