#include "InteropController.h"

InteropController::InteropController()
{
	//Debugger::PrintMessage("Created an interop controller");
}

InteropController::~InteropController()
{
}

void InteropController::AssignTimerCallback(SimpleCallback callback)
{
	m_timerCallback = callback;
}

void InteropController::Update(float normalTime, float secondNormal)
{
	if (!m_currentState)
	{
		Debugger::PrintMessage("Current was null");
	}

	if (b_inTransition)
	{
		m_currentState->Update(normalTime, this);

		if (secondNormal > 0)
		{
			m_nextState->Update(secondNormal, this);
		}
	}
	else
	{
		ComboController::Update(normalTime);
	}
}

void InteropController::ResetController()
{
	ComboController::ResetController();
	//Debugger::PrintMessage("Called reset");
	if (m_timerCallback)
	{
		m_timerCallback();
	}
}

void InteropController::TriggerEnter()
{
	m_nextState->OnEnter(this);
}

void InteropController::EndTransition()
{
	//ComboController::EndTransition();
	b_inTransition = false;
	b_pendingTransition = false;
	
	//Debugger::PrintMessage("before trigger exit");

	m_currentState->OnExit(this);

	//Debugger::PrintMessage("after trigger exit");

	m_currentState = m_nextState;
	m_nextState = nullptr;
}

void InteropController::FireTransition()
{
	ComboController::FireTransition();
	m_timerCallback();
}