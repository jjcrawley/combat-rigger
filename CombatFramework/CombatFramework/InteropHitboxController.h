#pragma once
#include "BaseHitBoxController.h"

typedef void(CALL* BoxHandler)(int id);
typedef void(CALL* FireDataHander)(float interval);

class InteropHitboxController :	public BaseHitBoxController
{
public:
	InteropHitboxController();
	~InteropHitboxController();
	void ActivateBoxes(int id) override;
	void DeactivateBoxes(int id) override;
	void QueueFireData(float interval) override;
	void AssignHandlers(BoxHandler activator, BoxHandler deactivator, FireDataHander fireDataHandler);
private:
	BoxHandler m_activator = nullptr;
	BoxHandler m_deactivator = nullptr;
	FireDataHander m_fireDataHandler = nullptr;
};