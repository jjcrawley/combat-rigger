#pragma once

#include <string>

#include "Standard.h"
#include "IInputHandler.h"

typedef float (CALL* AxisCallback)(const char* id);
typedef bool(CALL* ButtonCallback)(const char* id);
typedef int(CALL* IdCallback)(const char* name);

class InputInterop : public IInputHandler
{
public:
	InputInterop();
	~InputInterop();
	virtual float GetAxis(const char* id) override;
	virtual float GetAxisRaw(const char* id) override;
	virtual bool GetButtonDown(const char* id) override;
	virtual bool GetButtonUp(const char* id) override;
	virtual bool GetButtonHeld(const char* id) override;	
	virtual int GetID(const char* name) override;
	void RegisterButtonCallbacks(ButtonCallback buttonDown, ButtonCallback buttonUp, ButtonCallback buttonHeld);
	void RegisterAxisCallbacks(AxisCallback axisRaw, AxisCallback axis);
	void RegisterIDCallback(IdCallback id);
	virtual void OnCreate() override;
	virtual void Release() override;
private:
	AxisCallback m_axisRaw = nullptr;
	AxisCallback m_axis = nullptr;
	ButtonCallback m_buttonDown = nullptr;
	ButtonCallback m_buttonUp = nullptr;
	ButtonCallback m_buttonHeld = nullptr;
	IdCallback m_getID = nullptr;
};