#pragma once

#include "LinkLock.h"

class CombatLinkLock : public LinkLock
{
public:
	CombatLinkLock();
	~CombatLinkLock();
	virtual bool AllowAccess(float frame, const ComboController* controller) override;	
};