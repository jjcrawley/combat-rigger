#include "InteropLock.h"

InteropLock::InteropLock()
{
}

InteropLock::~InteropLock()
{
}

void InteropLock::RegisterInputCallback(InputCallback callback)
{
	m_callback = callback;
}

bool InteropLock::AllowAccess(float normal, const ComboController* controller)
{
	return m_callback(normal);
}