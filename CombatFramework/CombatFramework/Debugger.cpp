#include "Debugger.h"

DebugHelper Debugger::s_debug = NULL;

Debugger::Debugger()
{
}

Debugger::~Debugger()
{
}

void Debugger::PrintMessage(std::string message)
{
 	Debugger::s_debug(message.c_str());
}

void Debugger::RegisterCallback(DebugHelper helper)
{
	Debugger::s_debug = helper;
}