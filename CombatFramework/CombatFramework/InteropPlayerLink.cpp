#include "InteropPlayerLink.h"

InteropPlayerLink::InteropPlayerLink()
{
}

InteropPlayerLink::~InteropPlayerLink()
{
}

void InteropPlayerLink::UpdateParams()
{
	m_paramsUpdate();
}

void InteropPlayerLink::SetUpdateCallback(SimpleCallback callback)
{
	m_paramsUpdate = callback;
}