#include "ComboBox.h"
#include "ComboState.h"
#include "ComboTree.h"
#include "EntryState.h"

using namespace std;

ComboTree::ComboTree()
{
	m_comboStates = std::vector<ComboState*>(0);

	m_startState = new EntryState();
	m_comboStates.push_back(m_startState);
}

ComboTree::~ComboTree()
{
}

void ComboTree::SetName(const char* name)
{
	m_name = string(name);
}

const char* ComboTree::GetName()
{
	return m_name.c_str();
}

void ComboTree::SetDescription(const char* description)
{
	m_description = string(description);
}

const char* ComboTree::GetDescription()
{	
	return m_description.c_str();
}

void ComboTree::AssignComboBox(ComboBox* comboBox)
{
	m_comboBox = comboBox;
}

template<class T>
T* ComboTree::CreateAndAddComboState()
{
	T* temp = new T();
	
	ComboState* cast = dynamic_cast<ComboState*>(temp);

	if (cast != nullptr)
	{
		m_comboStates.push_back(cast);
	}
	else
	{
		delete temp;
	}

	return cast;
}

void ComboTree::AddComboState(ComboState* state)
{
	if (state)
	{
		m_comboStates.push_back(state);
	}
}

void ComboTree::RemoveComboState(ComboState* state)
{
	if (state)
	{
		m_comboStates.erase(std::remove(m_comboStates.begin(), m_comboStates.end(), state));
	}
}

void ComboTree::SetResetLink(ComboLink* link)
{
	if (link)
	{
		m_resetLink = link;
		link->SetDestinationState(m_startState);
	}
}

void ComboTree::AddEntryLink(ComboLink* link)
{
	if (link)
	{
		m_startState->AddComboLink(link);
	}
}

ComboState* ComboTree::ApplyReset()
{
	m_resetLink->UpdateParams();
	return m_startState;
}

ComboState* ComboTree::GetStart()
{
	return m_startState;
}

ComboLink* ComboTree::GetResetLink()
{
	return m_resetLink;
}

void ComboTree::Release()
{
	for (int i = 0; i < m_comboStates.size(); i++)
	{
		SAFE_DELETE(m_comboStates[i]);
	}
	
	SAFE_DELETE(m_resetLink);
}

void ComboTree::OnCreate()
{	
	/*for (int i = 0; i < m_comboStates.size(); i++)
	{
		m_comboStates[i]->OnCreate();
	}*/
}