#pragma once
#include "ComboController.h"

class InteropController : public ComboController
{
public:
	using ComboController::Update;
	InteropController();
	~InteropController();
	void AssignTimerCallback(SimpleCallback callback);
	void Update(float normalTime, float secondNormal);
	void ResetController() override;	
	void TriggerEnter();
	void EndTransition() override;
	void FireTransition() override;
private:
	SimpleCallback m_timerCallback = nullptr;
};