#include "ComboBox.h"

using namespace std;

ComboBox::ComboBox()
{
	m_comboTrees = std::vector<ComboTree*>(0);
}

ComboBox::~ComboBox()
{
}

void ComboBox::AssignName(char* name)
{
	m_name = string(name);
}

const char* ComboBox::GetName()
{
	return m_name.c_str();
}

void ComboBox::SetDescription(char* description)
{
	m_description = string(description);
}

const char* ComboBox::GetDescription()
{
	return m_description.c_str();
}

void ComboBox::AddComboTree(ComboTree* tree)
{
	if (tree)
	{
		m_comboTrees.push_back(tree);
		tree->AssignComboBox(this);
	}
}

void ComboBox::RemoveComboTree(ComboTree* tree)
{
	if (tree)
	{
		m_comboTrees.erase(remove(m_comboTrees.begin(), m_comboTrees.end(), tree));
	}
}

ComboTree* ComboBox::GetComboTree(string name)
{
	for (int i = 0; i < m_comboTrees.size(); i++)
	{
		if (m_comboTrees[i]->GetName() == name)
		{
			return m_comboTrees[i];
		}
	}

	return nullptr;
}

vector<ComboTree*>* ComboBox::GetComboTrees()
{
	return &m_comboTrees;
}

ComboTree*& ComboBox::operator[](size_t index)
{
	return m_comboTrees[index];
}

void ComboBox::OnCreate()
{
	/*for (int i = 0; i < m_comboTrees.size(); i++)
	{
		m_comboTrees[i]->OnCreate();
	}*/
}

void ComboBox::Release()
{
	for (int i = 0; i < m_comboTrees.size(); i++)
	{
		SAFE_DELETE(m_comboTrees[i]);
	}
}