#pragma once
#include "CombatModifier.h"

class InteropModifier :	public CombatModifier
{
public:
	InteropModifier();
	~InteropModifier();
	void Update(float normal) override;
	void OnHit() override;
	void OnEnter() override;
	void OnExit() override;
	void RegisterOnHit(SimpleCallback callback);
	void RegisterOnEnter(SimpleCallback callback);
	void RegisterOnExit(SimpleCallback callback);
	void RegisterOnUpdate(UpdateCallback callback);
private:
	SimpleCallback m_onHit = nullptr;
	SimpleCallback m_onEnter = nullptr;
	SimpleCallback m_onExit = nullptr;
	UpdateCallback m_onUpdate = nullptr;
};