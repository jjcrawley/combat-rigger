#pragma once

#include "Standard.h"

class Move;

class MoveModifier : public IObject
{
public:
	MoveModifier();
	~MoveModifier();
	virtual void Update(float frame);
	virtual void OnEnter();
	virtual void OnExit();
	void AssignMoveInfo(Move* moveInfo);
	Move* GetMoveInfo();
	virtual void OnCreate() override;
	virtual void Release() override;
protected:
	Move* m_moveInfo = nullptr;	
};