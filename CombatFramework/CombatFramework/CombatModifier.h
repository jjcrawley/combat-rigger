#pragma once

#include "Standard.h"
#include "MoveModifier.h"

class CombatModifier : public MoveModifier
{
public:
	CombatModifier();
	~CombatModifier();
	void Update(float normal) override;	
	virtual void OnHit();
	virtual void OnEnter();
	virtual void OnExit();
	//virtual int ProcessDamage();
	//virtual void OnStagger();
};