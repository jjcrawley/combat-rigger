#pragma once

#include "Standard.h"

class IInputHandler : public IObject
{
public:
	virtual bool GetButtonDown(const char* id) = 0;
	virtual bool GetButtonHeld(const char* id) = 0;
	virtual bool GetButtonUp(const char* id) = 0;
	virtual float GetAxis(const char* id) = 0;
	virtual float GetAxisRaw(const char* id) = 0;
	virtual int GetID(const char* name) = 0;
};