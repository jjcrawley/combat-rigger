#pragma once
#include "ComboState.h"

class EntryState :	public ComboState
{
public:
	EntryState();
	~EntryState();
	void Update(float normalTime, ComboController* controller) override;
	void OnEnter(const ComboController* controller) override;
	void OnExit(const ComboController* controller) override;
};