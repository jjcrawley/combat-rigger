#include "Move.h"
#include "MoveModifier.h"

MoveModifier::MoveModifier()
{
}

MoveModifier::~MoveModifier()
{
}

void MoveModifier::Update(float frame)
{
}

void MoveModifier::OnEnter()
{
}

void MoveModifier::OnExit()
{
}

void MoveModifier::AssignMoveInfo(Move* moveInfo)
{
	m_moveInfo = moveInfo;
}

Move* MoveModifier::GetMoveInfo()
{
	return m_moveInfo;
}

void MoveModifier::OnCreate()
{
}

void MoveModifier::Release()
{
}