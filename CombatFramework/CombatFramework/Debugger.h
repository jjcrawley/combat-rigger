#pragma once

#include "Standard.h"

class Debugger
{
public:
	Debugger();
	~Debugger();
	static void PrintMessage(std::string message);
	static void RegisterCallback(DebugHelper helper);
private:
	static DebugHelper s_debug;	
};