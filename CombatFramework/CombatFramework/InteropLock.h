#pragma once
#include "CombatLinkLock.h"

class InteropLock :	public CombatLinkLock
{
public:
	InteropLock();
	~InteropLock();
	void RegisterInputCallback(InputCallback callback);
	bool AllowAccess(float normal, const ComboController* controller) override;
private:
	InputCallback m_callback = nullptr;
};