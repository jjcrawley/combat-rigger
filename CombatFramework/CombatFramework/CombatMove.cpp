#include "CombatMove.h"
#include "Debugger.h"
#include "MoveModifier.h"
#include <string>
#include "ComboController.h"

CombatMove::CombatMove()
{
}

CombatMove::~CombatMove()
{
}

void CombatMove::OnCreate()
{
	m_resetNormalise = Period::Normalise(m_resetPeriod, GetFrameCount());
	m_hitboxActivatePeriodNormal = Period::Normalise(m_hitboxActivatePeriod, GetFrameCount());
}

void CombatMove::OnEnter(const ComboController* controller)
{
	size_t length = m_moveModifiers.size();

	for (int i = 0; i < length; i++)
	{
		m_moveModifiers[i]->OnEnter();
	}
}

void CombatMove::OnExit(const ComboController* controller)
{
	size_t length = m_moveModifiers.size();

	for (int i = 0; i < length; i++)
	{
		m_moveModifiers[i]->OnExit();
	}
}

void CombatMove::OnUpdate(float normalTime, const ComboController* controller)
{
	size_t length = m_moveModifiers.size();

	for (int i = 0; i < length; i++)
	{
		m_moveModifiers[i]->Update(normalTime);
	}
}

void CombatMove::SetBaseDamage(float baseDamage)
{
	m_baseDamage = baseDamage;
}

float CombatMove::GetBaseDamage()
{
	return m_baseDamage;
}

void CombatMove::SetDamageInterval(float interval)
{
	m_damageInterval = interval;
}

float CombatMove::GetDamageInterval()
{
	return m_damageInterval;
}

void CombatMove::SetResetPeriod(Period period)
{
	m_resetPeriod = period;
	
	m_resetNormalise = Period::Normalise(period, GetFrameCount());
}

Period CombatMove::GetResetPeriod()
{
	return m_resetPeriod;
}

void CombatMove::SetActivatePeriod(Period period)
{
	m_hitboxActivatePeriod = period;
	
	m_hitboxActivatePeriodNormal = Period::Normalise(period, GetFrameCount());	
}

Period CombatMove::GetActivatePeriod()
{
	return m_hitboxActivatePeriod;
}

void CombatMove::SetContinuous(bool continuous)
{
	b_continuousDamage = continuous;
}

bool CombatMove::GetContinuousDamage()
{
	return b_continuousDamage;
}

bool CombatMove::ShouldReset(float normal)
{
	//Debugger::PrintMessage("Reset Move period: " + std::to_string(m_resetPeriod.startTime) + ", " + std::to_string(m_resetPeriod.endTime));
	return normal >= m_resetNormalise.startTime;
}

bool CombatMove::ActivateHitboxes(float normal)
{
	return m_hitboxActivatePeriodNormal.InRange(normal);
}